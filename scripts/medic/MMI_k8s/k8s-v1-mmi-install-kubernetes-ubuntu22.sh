#!/bin/bash
#set -x

############################################################################################################################
############################################################################################################################
############################################################################################################################
#Based on https://computingforgeeks.com/deploy-kubernetes-cluster-on-ubuntu-with-kubeadm/
#How to call it.
#cd /usr/local/bin/MMI_k8s/ && chmod +x k8s-v1-mmi-install-kubernetes-new.sh && ./k8s-v1-mmi-install-kubernetes-new.sh
#
# Understand that you will have to closely monitor this while running. It would be a good idea to perform a restart on the 
# Physicla machine before starting this script.
# if something goes wrong use "set -x"
############################################################################################################################
############################################################################################################################
############################################################################################################################

source '/mnt/nas_backup/scripts/shared_functions.sh'

_FLOATING_IP="172.16.0.174"
_AUTH_PASS="marlb0r0"

read -p "Are you sure you want to run this script? " -n 1 -r
echo    # (optional) move to a new line
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1 # handle exits from shell or function but don't exit interactive shell
fi



#Make sure that this machine can ping all machines.
_PING_ERROR=false
#for i in accl-ffm-vm-lb1 accl-ffm-vm-lb2 accl-ffm-vm-lb3 accl-ffm-vm-etcd1 accl-ffm-vm-etcd2 accl-ffm-vm-etcd3 accl-ffm-vm-cp1 accl-ffm-vm-cp2 accl-ffm-vm-cp3 ACCL-FFM-SRV-008 ACCL-FFM-SRV-009 ACCL-FFM-SRV-010;
for i in accl-ffm-vm-cp1 accl-ffm-srv-008 accl-ffm-srv-009 accl-ffm-srv-010 accl-ffm-vm-mcp2;
do    
    if ! checkPing "${i}"; then
        echo "Error pinging: ${i}" 
        _PING_ERROR=true
        break
    fi
done
#if [[ ${_PING_ERROR} = true ]] ; then
#  echo "_PING_ERROR IS TRUE."
#  exit 1
#fi



if [[ -f "/tmp/tempGC.log" ]]; then
    echo "DELETING /tmp/tempGC.log."
    rm /tmp/tempGC.log 
fi
if [[ -f "/tmp/ip.conf" ]]; then
    echo "DELETING /tmp/ip.conf."
    echo "DELETING /tmp/ip.conf." >> /tmp/tempGC.log
    rm /tmp/ip.conf    
fi
if [[ -f "/tmp/if.conf" ]]; then
    echo "DELETING /tmp/if.conf."
    echo "DELETING /tmp/if.conf." >> /tmp/tempGC.log
    rm /tmp/if.conf    
fi




#https://www.baeldung.com/linux/check-variable-exists-in-list
function exists_in_list() {
    LIST=$1
    DELIMITER=$2
    VALUE=$3
    [[ "$LIST" =~ ($DELIMITER|^)$VALUE($DELIMITER|$) ]]
}





#Get the External IP Address
_ROUTE_IP=$( ip route get 8.8.8.8  | head -n 1 | tr -s ' ' | cut -d ' ' -f 7 | grep -oE "\b([0-9]{1,3}\.){3}([0-9]{1,3})\b" )
_HOST_IP=$( hostname -I | grep -P -o "${_ROUTE_IP}(?=)" | grep -oE "\b([0-9]{1,3}\.){3}([0-9]{1,3})\b" )
if [[ "$_ROUTE_IP" == "$_HOST_IP" ]]; then
  echo "_HOST_IP: $_HOST_IP"
  echo $_HOST_IP | tee /tmp/ip.conf
else
    #ALERT HERE
    echo "_ROUTE_IP: $_ROUTE_IP **DOES NOT MATCH** _HOST_IP: $_HOST_IP     WILL EXIT NOW."
    exit 1
fi

#At this point 2 different functions confirmed the IP Address move on to get the INTERFACE NAME

#GET THE INTERFACE NAME WITH THE IP KNOWN.
_INTERFACE_NAME=$( ip addr | sed -z 's/\n/@@/g' | grep -P -o "@@[\d]{1,3}.*(?=${_HOST_IP})" | cut -d':' -f2 | sed -z 's/\s//g' )

#GETTING THE INTERFACE NAME WITH THE EXTERNAL IP.
_INTERFACE_NAME_ROUTE=$( ip route get 8.8.8.8 | head -n 1 | tr -s ' ' | cut -d ' ' -f 5 )


list_of_k8s_PHYSICAL_MACHINE="accl-ffm-srv-008 accl-ffm-srv-009 accl-ffm-srv-010 ACCL-FFM-SRV-008 ACCL-FFM-SRV-009 ACCL-FFM-SRV-010"
_HOSTNAME_CONTAINS_K8S_PHYSICAL_MACHINE=$( hostname )
if exists_in_list "$list_of_k8s_PHYSICAL_MACHINE" " " $_HOSTNAME_CONTAINS_K8S_PHYSICAL_MACHINE; then
    echo "_INTERFACE_NAME: $_INTERFACE_NAME"
    echo $_INTERFACE_NAME | tee /tmp/if.conf
else

  echo "_HOSTNAME_CONTAINS_K8S: $_HOSTNAME_CONTAINS_K8S" 

  if [[ "$_INTERFACE_NAME" == "$_INTERFACE_NAME_ROUTE" ]]; then
    echo "_INTERFACE_NAME: $_INTERFACE_NAME"
    echo $_INTERFACE_NAME | tee /tmp/if.conf
  else
      #ALERT HERE
      echo "_INTERFACE_NAME: $_INTERFACE_NAME **DOES NOT MATCH** _INTERFACE_NAME_RROUTE: $_INTERFACE_NAME_RROUTE     WILL EXIT NOW."
      exit 1
  fi

fi




echo "Sleeping 10"
sleep 10

##########################################################################


if [[ ! -f "/tmp/ip.conf" ]]; then
    echo "/tmp/ip.conf DOES NOT EXIST."
    echo "/tmp/ip.conf DOES NOT EXIST." >> /tmp/tempGC.log
    exit 1
fi
if [[ ! -f "/tmp/if.conf" ]]; then
    echo "/tmp/if.conf DOES NOT EXIST."
    echo "/tmp/if.conf DOES NOT EXIST." >> /tmp/tempGC.log
    exit 1
fi


THIS_IP="$( cat /tmp/ip.conf )"
echo "THIS_IP : ${THIS_IP}" >> /tmp/tempGC.log
INTERFACE_NAME="$( cat /tmp/if.conf )"
echo "INTERFACE_NAME : ${INTERFACE_NAME}" >> /tmp/tempGC.log



# Set external DNS
sed -i -e 's/#DNS=/DNS=8.8.8.8/' /etc/systemd/resolved.conf
service systemd-resolved restart

set -e
echo "ADDRESS into etc hosts: ${THIS_IP}" >> /tmp/tempGC.log
sed -e "s/^.*${HOSTNAME}.*/${THIS_IP} ${HOSTNAME} ${HOSTNAME}.local/" -i /etc/hosts

echo 'EDITOR="/usr/bin/nano"' >> /etc/environment
echo 'K8S_YAML_PATH="/mnt/nas_backup/scripts/kubernetes/yamls"' >> /etc/environment

sudo apt update
sleep 5
sudo apt -y full-upgrade
sleep 5

[ -f /var/run/reboot-required ] && sudo reboot -f

sleep 5



####################################################################################################################
############### Install in all K8S machine #########################################################################
####################################################################################################################
list_of_k8s="accl-ffm-vm-cp1 accl-ffm-vm-cp2 accl-ffm-vm-cp3 accl-ffm-srv-008 accl-ffm-srv-009 accl-ffm-srv-010 ACCL-FFM-SRV-008 ACCL-FFM-SRV-009 ACCL-FFM-SRV-010 accl-ffm-vm-mcp2"
_HOSTNAME_CONTAINS_K8S=$( hostname )
if exists_in_list "$list_of_k8s" " " $_HOSTNAME_CONTAINS_K8S; then

  echo "_HOSTNAME_CONTAINS_K8S: $_HOSTNAME_CONTAINS_K8S" 
  
cat >> ~/.bashrc <<EOL
export EDITOR='/usr/bin/nano'
export K8S_YAML_PATH='/mnt/nas_backup/scripts/kubernetes/yamls'

#GCF personal aliases
alias gan='echo -n "Enter namespace: " && read NS && kubectl get all -o wide -n $NS'
alias gam='watch kubectl get all -o wide -n mmi-medic'
alias gamm=' kubectl get all -o wide -n mmi-medic'
alias cdk='cd /usr/local/bin/MMI_k8s/'
alias kx='f() { [ "$1" ] && kubectl config use-context $1 || kubectl config current-context ; } ; f'
alias kn='f() { [ "$1" ] && kubectl config set-context --current --namespace $1 || kubectl config view --minify | grep namespace | cut -d" " -f6 ; } ; f'
EOL

  source ~/.bashrc
  
  printf "overlay\nbr_netfilter\n" >> /etc/modules-load.d/containerd.conf
  
  printf "net.bridge.bridge-nf-call-iptables = 1\nnet.ipv4.ip_forward = 1\nnet.bridge.bridge-nf-call-ip6tables = 1\n" >> /etc/sysctl.d/99-kubernetes-cri.conf

  sysctl --system
 
  sudo apt -y install vim git mysql-client wget net-tools mc jq openjdk-8-jre-headless curl apt-transport-https ca-certificates gnupg2 software-properties-common 
      
  wget https://github.com/containerd/containerd/releases/download/v1.6.16/containerd-1.6.16-linux-amd64.tar.gz -P /tmp/
  tar Cxzvf /usr/local /tmp/containerd-1.6.16-linux-amd64.tar.gz
  wget https://raw.githubusercontent.com/containerd/containerd/main/containerd.service -P /etc/systemd/system/
  systemctl daemon-reload
  systemctl enable --now containerd

  wget https://github.com/opencontainers/runc/releases/download/v1.1.4/runc.amd64 -P /tmp/
  install -m 755 /tmp/runc.amd64 /usr/local/sbin/runc

  wget https://github.com/containernetworking/plugins/releases/download/v1.2.0/cni-plugins-linux-amd64-v1.2.0.tgz -P /tmp/
  mkdir -p /opt/cni/bin
  tar Cxzvf /opt/cni/bin /tmp/cni-plugins-linux-amd64-v1.2.0.tgz

  mkdir -p /etc/containerd
  containerd config default | tee /etc/containerd/config.toml
  sed -i -e 's/SystemdCgroup = false/SystemdCgroup = true/' /etc/containerd/config.toml
  systemctl restart containerd
    
  sleep 5
  curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
  sleep 5
  echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
  sleep 5
  
  if grep -q swap "/etc/fstab"; then
    sudo sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab
    #TODO
    # /swap.img       none    swap    sw      0       0
    sudo swapoff -a
    sleep 5
    sudo mount -a
    sleep 5
    free -h
    sleep 5
  else 
    echo "NO SWAP in the fstab file."
  fi   

  sudo apt update
  sleep 5
  
  [ -f /var/run/reboot-required ] && sudo reboot -f  
  
  #sudo apt -y install vim git mysql-client wget net-tools mc jq nfs-common kubeadm=1.24.7-00 kubelet=1.24.7-00 kubectl=1.24.7-00
  sudo apt -y install nfs-common kubeadm=1.26.3-00 kubelet=1.26.3-00 kubectl=1.26.3-00
  sleep 5
  sudo apt-mark hold kubelet kubeadm kubectl
  sleep 5

  #kubectl version --client && kubeadm version
  #  Client Version: version.Info{Major:"1", Minor:"23", GitVersion:"v1.23.5", GitCommit:"c285e781331a3785a7f436042c65c5641ce8a9e9", GitTreeState:"clean", BuildDate:"2022-03-16T15:58:47Z", GoVersion:"go1.17.8", Compiler:"gc", Platform:"linux/amd64"}
  #  kubeadm version: &version.Info{Major:"1", Minor:"23", GitVersion:"v1.23.5", GitCommit:"c285e781331a3785a7f436042c65c5641ce8a9e9", GitTreeState:"clean", BuildDate:"2022-03-16T15:57:37Z", GoVersion:"go1.17.8", Compiler:"gc", Platform:"linux/amd64"}

  #LONGHORN issue fix for device name (sdb|sdc)
  #https://longhorn.io/kb/troubleshooting-volume-with-multipath/
  #https://www.esgeroth.org/log/entry/2005
  if ! grep -qE 'devnode\s\"\^sd\[a-z0-9\]\+\"' "/etc/multipath.conf"; then
cat >> /etc/multipath.conf <<EOL
blacklist {
    devnode "^sd[a-z0-9]+"
}
EOL
    sudo systemctl restart multipathd.service
    sleep 3
    echo "LONGHORN multipath issue patched."
    
    if multipath -t | grep -E 'devnode\s\"\^sd\[a-z0-9\]\+\"'; then
      echo "LONGHORN multipath returned correct code."
    else
      echo "ERROR: LONGHORN multipath returned incorrect code: EXITING NOW."
      exit 1
    fi  
    
  else
    echo "LONGHORN multipath issue already patched."
  fi
  
  # Enable kernel modules
  sudo modprobe overlay
  sleep 5
  sudo modprobe br_netfilter
  sleep 5


  # Add some settings to sysctl
sudo tee /etc/sysctl.d/kubernetes.conf<<EOF
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
EOF

  sleep 5

  # Reload sysctl
  sudo sysctl --system  
 


  sleep 5

  #Step 5: Initialize master node
  #Login to the server to be used as master and make sure that the br_netfilter module is loaded:
  #$ lsmod | grep br_netfilter
  #    br_netfilter           22256  0 
  #    bridge                151336  2 br_netfilter,ebtable_broute
      
            
  sudo systemctl enable kubelet
  sleep 5

  sudo kubeadm config images pull
  sleep 5

  # Containerd
  sudo kubeadm config images pull --cri-socket unix:///run/containerd/containerd.sock
  sleep 5

  sudo mkdir -p /root/.kube
  sudo touch /root/.kube/config
  sudo chown -R root:root /root/.kube
  
fi #if exists_in_list "$list_of_k8s" " " $_HOSTNAME_CONTAINS_K8S; then

  


####################################################################################################################
############### Install in a K8S CONTROL PLANE MASTER machine ######################################################
############### Install in a K8S CONTROL PLANE MASTER machine ######################################################
####################################################################################################################
list_of_cpms="accl-ffm-vm-cp1"
_HOSTNAME_CONTAINS_CP=$( hostname )
if exists_in_list "$list_of_cpms" " " $_HOSTNAME_CONTAINS_CP; then 
 
  echo "_HOSTNAME_CONTAINS_CP: $_HOSTNAME_CONTAINS_CP"

  THIS_IP="$( cat /tmp/ip.conf )"

  K8S_ADMIN_CONF_OUTPUT_FILE=./from_master_admin.conf
  OUTPUT_FILE=./join.sh
  KEY_FILE=./id_rsa.pub

  rm -rf $K8S_ADMIN_CONF_OUTPUT_FILE
  rm -rf $OUTPUT_FILE

  if [[ -f "/root/.ssh/id_rsa" ]]; then
    echo "DELETING /root/.ssh/id_rsa."
    rm /root/.ssh/id_rsa
  fi
  # Create key
  ssh-keygen -q -t rsa -b 4096 -N '' -f /root/.ssh/id_rsa
  cat /root/.ssh/id_rsa.pub >> /root/.ssh/authorized_keys
  cat /root/.ssh/id_rsa.pub > ${KEY_FILE}
  
  sudo apt install -y etcd-client

  echo "BEFORE KUBEADM"
  sudo kubeadm init --control-plane-endpoint="${THIS_IP}:6443" --upload-certs --apiserver-advertise-address=${THIS_IP} --pod-network-cidr=10.244.0.0/16 > ${OUTPUT_FILE}

  ##As soon as load balancers are ready
  #sudo kubeadm init --control-plane-endpoint="${_FLOATING_IP}:6443" --upload-certs --apiserver-advertise-address=${THIS_IP} --pod-network-cidr=10.244.0.0/16 > ${OUTPUT_FILE}


  echo "AFTER KUBEADM"
  ##CANT REMEMBER
  #sudo kubeadm init --apiserver-advertise-address=${THIS_IP} --pod-network-cidr=10.233.0.0/24 | grep -Ei "kubeadm join|discovery-token-ca-cert-hash" > ${OUTPUT_FILE}
  #chmod +x $OUTPUT_FILE

  #GC get the admin.conf file to be able to login to the cluster with k get all -0 wide
  sudo cat /etc/kubernetes/admin.conf > ${K8S_ADMIN_CONF_OUTPUT_FILE}

  # Configure kubectl for vagrant and root users
  #mkdir -p $HOME/.kube
  sudo cp /etc/kubernetes/admin.conf $HOME/.kube/config
  #sudo chown $(id -u):$(id -g) $HOME/.kube/config
  #sudo mkdir -p /root/.kube
  sudo cp /etc/kubernetes/admin.conf /root/.kube/config
  #sudo chown -R root:root /root/.kube
  
  echo "BEFORE KUBECTL WEAVE"

  #The images must be there first!  --pod-network-cidr=10.244.0.0/16 must be there above
  #kubectl apply -f https://github.com/coreos/flannel/raw/master/Documentation/kube-flannel.yml
  #kubectl apply -f ${K8S_YAML_PATH}/k8s-v1-mmi-install-kubernetes-kube-flannel.yml
  kubectl apply -f ${K8S_YAML_PATH}/weave-daemonset-k8s.yaml
  
  echo "AFTER KUBECTL WEAVE"


fi



####################################################################################################################
############### Install in a LOAD BALANCER machine #################################################################
####################################################################################################################
list_of_lbs="accl-ffm-vm-lb1 accl-ffm-vm-lb2 accl-ffm-vm-lb3"
_HOSTNAME_CONTAINS_LB=$( hostname )
if exists_in_list "$list_of_lbs" " " $_HOSTNAME_CONTAINS_LB; then

  _STATE="BACKUP"
  if [[ "${_HOSTNAME_CONTAINS_LB}" = "accl-ffm-vm-lb1" ]]; then
    _STATE="MASTER"  
  fi
  if [[ "${_HOSTNAME_CONTAINS_LB}" = "accl-ffm-vm-lb1" ]]; then
    _PRIORITY=100  
  fi
  if [[ "${_HOSTNAME_CONTAINS_LB}" = "accl-ffm-vm-lb2" ]]; then
    _PRIORITY=95  
  fi
  if [[ "${_HOSTNAME_CONTAINS_LB}" = "accl-ffm-vm-lb3" ]]; then
    _PRIORITY=90  
  fi

 
  echo "_HOSTNAME_CONTAINS_LB: $_HOSTNAME_CONTAINS_LB"
  
  sudo apt update && apt install -y keepalived haproxy
  
  sleep 15
  
cat > /etc/keepalived/check_apiserver.sh <<EOF
#!/bin/sh

errorExit() {
  echo "*** $@" 1>&2
  exit 1
}

curl --silent --max-time 2 --insecure https://localhost:6443/ -o /dev/null || errorExit "Error GET https://localhost:6443/"
if ip addr | grep -q ${_FLOATING_IP}; then
  curl --silent --max-time 2 --insecure https://${_FLOATING_IP}:6443/ -o /dev/null || errorExit "Error GET https://${_FLOATING_IP}:6443/"
fi
EOF

  sudo chmod +x /etc/keepalived/check_apiserver.sh
  
cat > /etc/keepalived/keepalived.conf <<EOF
vrrp_script check_apiserver {
  script "/etc/keepalived/check_apiserver.sh"
  interval 3
  timeout 10
  fall 5
  rise 2
  weight -2
}

vrrp_instance VI_1 {
    state ${_STATE}
    interface ${_INTERFACE_NAME_ROUTE}
    virtual_router_id 1
    priority ${_PRIORITY}
    advert_int 5
    authentication {
        auth_type PASS
        auth_pass ${_AUTH_PASS}
    }
    virtual_ipaddress {
        ${_FLOATING_IP}
    }
    track_script {
        check_apiserver
    }
}
EOF

  sudo systemctl enable --now keepalived
  
  sleep 15
  
cat >> /etc/haproxy/haproxy.cfg <<EOF

frontend kubernetes-frontend
  bind *:6443
  mode tcp
  option tcplog
  default_backend kubernetes-backend

backend kubernetes-backend
  option httpchk GET /healthz
  http-check expect status 200
  mode tcp
  option ssl-hello-chk
  balance roundrobin
    server ACCL-FFM-SRV-008 172.16.0.8:6443 check fall 3 rise 2
    server ACCL-FFM-SRV-009 172.16.0.9:6443 check fall 3 rise 2
    server ACCL-FFM-SRV-010 172.16.0.10:6443 check fall 3 rise 2

EOF

  sudo systemctl enable haproxy 
  sleep 15
  sudo systemctl restart haproxy


fi 
#End LOAD BALANCER INSTALL
##some useful commnands when implementing
#find out what state it is in.
#journalctl -flu keepalived
#Find out is the floating Ip is assigned here.
#ip a s
#ip a s ens160
#sudo apt-get purge -y --auto-remove haproxy && sudo apt-get purge -y --auto-remove keepalived && rm -rf /etc/keepalived/check_apiserver.sh /etc/keepalived/keepalived.conf /etc/haproxy/haproxy.cfg && reboot




echo " "
echo " "
echo "All commands finished."

exit 0
