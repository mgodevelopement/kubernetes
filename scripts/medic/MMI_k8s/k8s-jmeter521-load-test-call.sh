#!/bin/bash
#set -x


COLOR_NC='\e[0m' # No Color
COLOR_BLACK='\e[0;30m'
COLOR_RED='\e[0;31m'
COLOR_GREEN='\e[0;32m'
COLOR_YELLOW='\e[1;33m'
COLOR_BLUE='\e[0;34m'
COLOR_LIGHT_BLUE='\e[1;34m'
COLOR_WHITE='\e[1;37m'


_PARTNER_ID="DevRE59bUdEP6qdgSbK"
_URL="http://192.168.2.181:8080"
_USERNAME="g.cruz@mmi.de"
_USER_PASSWORD="medic123"
_MPI_DB_USERNAME="root"
_USE_DB=


remove-warning () {
    grep -v 'mysql: [Warning] Using a password on the command line interface can be insecure.'
}


#curl -X GET 'http://192.168.2.181:8080/medic/1/mpipro/getMetadata/%7B%22partnerid%22%3A%22rFeS9doh8aX2ZYegWS4v3Dg%22%2C%22sessionid%22%3A%20%221a4b53ae-ad65-402d-b931-1895c46e0c0f%22%2C%22moduleparameters%22%3A%7B%7D%7D'


_UUID=$(uuidgen | sed 's/-//g')
echo "_UUID: ${_UUID}"

_DATE=$(date -d '+6 hour' '+%F %T')
echo "_DATE: ${_DATE}"

#We need a sessionID so we can curl to login
_LOGIN_SESSION_ID=$( curl --location --request PUT "${_URL}/medic/1/um/login" \
  --header 'Content-Type: text/plain' \
  --data-raw "{\"partnerid\":\"${_PARTNER_ID}\",\"moduleparameters\":{\"username\":\"${_USERNAME}\",\"password\":\"${_USER_PASSWORD}\"}}" |  jq -r  '.RESULTOBJECT' )

if [[ ${_LOGIN_SESSION_ID} =~ ^\{?[A-F0-9a-f]{8}-[A-F0-9a-f]{4}-[A-F0-9a-f]{4}-[A-F0-9a-f]{4}-[A-F0-9a-f]{12}\}?$ ]]; then
    echo "_LOGIN_SESSION_ID: ${_LOGIN_SESSION_ID}"
else
    echo "_LOGIN_SESSION_ID IS NOT IN UUID format. exiting now."
    exit 1
fi



#We need to place a Temp URL to a Video in the DB
_SELECT_DOCUMENTID_STMT="select documentid from medic_dms.documentversion where documenttypeid IN( select ID from medic_dms.documenttype where FORMATCODE = 'VIDEO' AND SERVICE_STATUS = 'A'  ) AND SERVICE_STATUS = 'A' ORDER BY SERVICE_ENTRYTIMESTAMP DESC LIMIT 1;"
while IFS=$'\r' read _field1
do
   _DOCUMENTID=$_field1
   echo "_field1: ${_field1}"
done < <(kubectl exec -it -c mysql mysql-0 -- mysql -u $_MPI_DB_USERNAME -NB -e "$_SELECT_DOCUMENTID_STMT" 2>&1 | remove-warning )

if [[ ! -z "${_DOCUMENTID}" ]]; then
    if [[ ${_DOCUMENTID} =~ ^\{?[A-F0-9a-f]{8}-[A-F0-9a-f]{4}-[A-F0-9a-f]{4}-[A-F0-9a-f]{4}-[A-F0-9a-f]{12}\}?$ ]]; then
        echo "_DOCUMENTID: ${_DOCUMENTID}"
    else
        echo "_DOCUMENTID IS NOT IN UUID format. exiting now."
        exit 1
    fi
else
    echo "_DOCUMENTID IS NULL. exiting now."
    exit 1
fi





#INSERT VIDEO RECORD FOR tempUrl
kubectl exec -it -c mysql mysql-0 -- mysql -u $_MPI_DB_USERNAME -e "INSERT INTO indexdb.tempurl (randomkey, expiration_timestamp, partnerid, sessionid, documentid) VALUES('${_UUID}', '${_DATE}', '${_PARTNER_ID}', '${_LOGIN_SESSION_ID}', '${_DOCUMENTID}')"




exit 0

















############################################################################################################################
############################################################################################################################
############################################################################################################################
#Based on https://computingforgeeks.com/deploy-kubernetes-cluster-on-ubuntu-with-kubeadm/
#How to call it.
#cd /usr/local/bin/MMI_k8s/ && chmod +x k8s-v1-mmi-install-kubernetes-new.sh && ./k8s-v1-mmi-install-kubernetes-new.sh
#
# Understand that you will have to closely monitor this while running. It would be a good idea to performa restart on the 
# Physicla machine before starting this script.
# if something goes wrong use "set -x"
############################################################################################################################
############################################################################################################################
############################################################################################################################


read -p "Are you sure you want to run this script? " -n 1 -r
echo    # (optional) move to a new line
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1 # handle exits from shell or function but don't exit interactive shell
fi




if [[ -f "/tmp/tempGC.log" ]]; then
    echo "DELETING /tmp/tempGC.log."
    rm /tmp/tempGC.log 
fi
if [[ -f "/tmp/ip.conf" ]]; then
    echo "DELETING /tmp/ip.conf."
    echo "DELETING /tmp/ip.conf." >> /tmp/tempGC.log
    rm /tmp/ip.conf    
fi
if [[ -f "/tmp/if.conf" ]]; then
    echo "DELETING /tmp/if.conf."
    echo "DELETING /tmp/if.conf." >> /tmp/tempGC.log
    rm /tmp/if.conf    
fi




#https://www.baeldung.com/linux/check-variable-exists-in-list
function exists_in_list() {
    LIST=$1
    DELIMITER=$2
    VALUE=$3
    [[ "$LIST" =~ ($DELIMITER|^)$VALUE($DELIMITER|$) ]]
}





#Get the External IP Address
_ROUTE_IP=$( ip route get 8.8.8.8  | head -n 1 | tr -s ' ' | cut -d ' ' -f 7 | grep -oE "\b([0-9]{1,3}\.){3}([0-9]{1,3})\b" )
_HOST_IP=$( hostname -I | grep -P -o "${_ROUTE_IP}(?=)" | grep -oE "\b([0-9]{1,3}\.){3}([0-9]{1,3})\b" )
if [[ "$_ROUTE_IP" == "$_HOST_IP" ]]; then
  echo "_HOST_IP: $_HOST_IP"
  echo $_HOST_IP | tee /tmp/ip.conf
else
    #ALERT HERE
    echo "_ROUTE_IP: $_ROUTE_IP **DOES NOT MATCH** _HOST_IP: $_HOST_IP     WILL EXIT NOW."
    exit 1
fi

#At this point 2 different functions confirmed the IP Address move on to get the INTERFACE NAME

#GET THE INTERFACE NAME WITH THE IP KNOWN.
_INTERFACE_NAME=$( ip addr | sed -z 's/\n/@@/g' | grep -P -o "@@[\d]{1,3}.*(?=${_HOST_IP})" | cut -d':' -f2 | sed -z 's/\s//g' )

#GETTING THE INTERFACE NAME WITH THE EXTERNAL IP.
_INTERFACE_NAME_ROUTE=$( ip route get 8.8.8.8 | head -n 1 | tr -s ' ' | cut -d ' ' -f 5 )


list_of_k8s_PHYSICAL_MACHINE="accl-ffm-srv-008 accl-ffm-srv-009 accl-ffm-srv-010 ACCL-FFM-SRV-008 ACCL-FFM-SRV-009 ACCL-FFM-SRV-010"
_HOSTNAME_CONTAINS_K8S_PHYSICAL_MACHINE=$( hostname )
if exists_in_list "$list_of_k8s_PHYSICAL_MACHINE" " " $_HOSTNAME_CONTAINS_K8S_PHYSICAL_MACHINE; then
    echo "_INTERFACE_NAME: $_INTERFACE_NAME"
    echo $_INTERFACE_NAME | tee /tmp/if.conf
else

  echo "_HOSTNAME_CONTAINS_K8S: $_HOSTNAME_CONTAINS_K8S" 

  if [[ "$_INTERFACE_NAME" == "$_INTERFACE_NAME_ROUTE" ]]; then
    echo "_INTERFACE_NAME: $_INTERFACE_NAME"
    echo $_INTERFACE_NAME | tee /tmp/if.conf
  else
      #ALERT HERE
      echo "_INTERFACE_NAME: $_INTERFACE_NAME **DOES NOT MATCH** _INTERFACE_NAME_RROUTE: $_INTERFACE_NAME_RROUTE     WILL EXIT NOW."
      exit 1
  fi

fi




echo "Sleeping 10"
sleep 10

##########################################################################


if [[ ! -f "/tmp/ip.conf" ]]; then
    echo "/tmp/ip.conf DOES NOT EXIST."
    echo "/tmp/ip.conf DOES NOT EXIST." >> /tmp/tempGC.log
    exit 1
fi
if [[ ! -f "/tmp/if.conf" ]]; then
    echo "/tmp/if.conf DOES NOT EXIST."
    echo "/tmp/if.conf DOES NOT EXIST." >> /tmp/tempGC.log
    exit 1
fi


THIS_IP="$( cat /tmp/ip.conf )"
echo "THIS_IP : ${THIS_IP}" >> /tmp/tempGC.log
INTERFACE_NAME="$( cat /tmp/if.conf )"
echo "INTERFACE_NAME : ${INTERFACE_NAME}" >> /tmp/tempGC.log



# Set external DNS
sed -i -e 's/#DNS=/DNS=8.8.8.8/' /etc/systemd/resolved.conf
service systemd-resolved restart

set -e
echo "ADDRESS into etc hosts: ${THIS_IP}" >> /tmp/tempGC.log
sed -e "s/^.*${HOSTNAME}.*/${THIS_IP} ${HOSTNAME} ${HOSTNAME}.local/" -i /etc/hosts

# remove ubuntu-bionic entry
sed -e '/^.*ubuntu-bionic.*/d' -i /etc/hosts



sudo apt update
sleep 5
sudo apt -y full-upgrade
sleep 5
[ -f /var/run/reboot-required ] && sudo reboot -f
sleep 5




####################################################################################################################
############### Install in a K8S machine ###########################################################################
####################################################################################################################
list_of_k8s="accl-ffm-vm-cp1 accl-ffm-vm-cp2 accl-ffm-vm-cp3 accl-ffm-srv-008 accl-ffm-srv-009 accl-ffm-srv-010 ACCL-FFM-SRV-008 ACCL-FFM-SRV-009 ACCL-FFM-SRV-010"
_HOSTNAME_CONTAINS_K8S=$( hostname )
if exists_in_list "$list_of_k8s" " " $_HOSTNAME_CONTAINS_K8S; then

  echo "_HOSTNAME_CONTAINS_K8S: $_HOSTNAME_CONTAINS_K8S" 


  sudo apt -y install curl apt-transport-https
  sleep 5
  curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
  sleep 5
  echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
  sleep 5

  sudo apt update
  sleep 5
  sudo apt -y install vim git mysql-client wget net-tools mc jq nfs-common kubeadm=1.24.7-00 kubelet=1.24.7-00 kubectl=1.24.7-00
  sleep 5
  sudo apt-mark hold kubelet kubeadm kubectl
  sleep 5

  #kubectl version --client && kubeadm version
  #  Client Version: version.Info{Major:"1", Minor:"23", GitVersion:"v1.23.5", GitCommit:"c285e781331a3785a7f436042c65c5641ce8a9e9", GitTreeState:"clean", BuildDate:"2022-03-16T15:58:47Z", GoVersion:"go1.17.8", Compiler:"gc", Platform:"linux/amd64"}
  #  kubeadm version: &version.Info{Major:"1", Minor:"23", GitVersion:"v1.23.5", GitCommit:"c285e781331a3785a7f436042c65c5641ce8a9e9", GitTreeState:"clean", BuildDate:"2022-03-16T15:57:37Z", GoVersion:"go1.17.8", Compiler:"gc", Platform:"linux/amd64"}

  if grep -q swap "/etc/fstab"; then
    sudo sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab
    sudo swapoff -a
    sleep 5
    sudo mount -a
    sleep 5
    free -h
    sleep 5
  else 
    echo "NO SWAP in the fstab file."
  fi 

  #LONGHORN issue fix for device name (sdb|sdc)
  #https://longhorn.io/kb/troubleshooting-volume-with-multipath/
  #https://www.esgeroth.org/log/entry/2005
  if ! grep -qE 'devnode\s\"\^sd\[a-z0-9\]\+\"' "/etc/multipath.conf"; then
cat >> /etc/multipath.conf <<EOL
blacklist {
    devnode "^sd[a-z0-9]+"
}
EOL
    sudo systemctl restart multipathd.service
    sleep 3
    echo "LONGHORN multipath issue patched."
    
    if multipath -t | grep -E 'devnode\s\"\^sd\[a-z0-9\]\+\"'; then
      echo "LONGHORN multipath returned correct code."
    else
      echo "ERROR: LONGHORN multipath returned incorrect code: EXITING NOW."
      exit 1
    fi  
    
  else
    echo "LONGHORN multipath issue already patched."
  fi
  

  # Enable kernel modules
  sudo modprobe overlay
  sleep 5
  sudo modprobe br_netfilter
  sleep 5


  # Add some settings to sysctl
sudo tee /etc/sysctl.d/kubernetes.conf<<EOF
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
EOF

  sleep 5

  # Reload sysctl
  sudo sysctl --system

  sleep 5




  ####################################################################################################################
  ############### Install containerd #################################################################################
  ####################################################################################################################
  _INSTALL_CONTAINERD=true
  SYSTEMCTL_CONTAINERD="$( systemctl list-units --all -t service --full --no-legend "containerd.service" | sed 's/^\s*//g' | cut -f1 -d' ' )"  
  if [[ ! -z ${SYSTEMCTL_CONTAINERD} ]]; then    
    
    STATUS_CONTAINERD="$(systemctl is-active containerd.service)"
    RUNNING_CONTAINERD="$(systemctl status containerd | grep running)"
    echo "CONTAINERD Status: ${STATUS_CONTAINERD}"
    echo "CONTAINERD Running: ${RUNNING_CONTAINERD}"
    #If it finds Containerd and it is running then it will NOT install
    if [[ "${STATUS_CONTAINERD}" = "active" ]] && [[ ! -z ${RUNNING_CONTAINERD} ]]; then
        _INSTALL_CONTAINERD=false
        VERSION_CONTAINERD="$(containerd -version)"
        echo "CONTANINERD will NOT be added it is already running version: ${VERSION_CONTAINERD}"
    fi
    
  else   
    echo "ContainerD not found in command: systemctl list-unit-files --type service --no-pager"
  fi  #if [[ ! -z ${SYSTEMCTL_CONTAINERD} ]]; then  
   
  echo "CONTAINERD Install boolean: ${_INSTALL_CONTAINERD}"  
  
  if ${_INSTALL_CONTAINERD} ; then 
  
    # Configure persistent loading of modules
sudo tee /etc/modules-load.d/containerd.conf <<EOF
overlay
br_netfilter
EOF

    sleep 5


    # Load at runtime
    sudo modprobe overlay
    sleep 5
    sudo modprobe br_netfilter
    sleep 5

    # Ensure sysctl params are set
sudo tee /etc/sysctl.d/kubernetes.conf<<EOF
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
EOF

    sleep 5

    # Reload configs
    sudo sysctl --system
    sleep 5


    # Install required packages
    sudo apt install -y curl gnupg2 software-properties-common apt-transport-https ca-certificates
    sleep 5


    # Add Docker repo
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    sleep 5
    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
    sleep 5

    # Install containerd
    sudo apt update
    sleep 5
    sudo apt install -y containerd.io
    sleep 5

    # Configure containerd and start service
    #sudo su -
    mkdir -p /etc/containerd
    containerd config default>/etc/containerd/config.toml
    sleep 5

    # restart containerd
    sudo systemctl restart containerd
    sleep 5
    sudo systemctl enable containerd
    sleep 5
    #systemctl status  containerd

  fi #if ${_INSTALL_CONTAINERD} ; then 



  #Step 5: Initialize master node
  #Login to the server to be used as master and make sure that the br_netfilter module is loaded:
  #$ lsmod | grep br_netfilter
  #    br_netfilter           22256  0 
  #    bridge                151336  2 br_netfilter,ebtable_broute
      
      
      
  sudo systemctl enable kubelet
  sleep 5

  sudo kubeadm config images pull
  sleep 5

  # Containerd
  sudo kubeadm config images pull --cri-socket unix:///run/containerd/containerd.sock
  sleep 5

  sudo mkdir -p /root/.kube
  sudo touch /root/.kube/config
  sudo chown -R root:root /root/.kube
  
fi #if exists_in_list "$list_of_k8s" " " $_HOSTNAME_CONTAINS_K8S; then

  


####################################################################################################################
############### Install in a K8S CONTROL PLANE MASTER machine ######################################################
############### Install in a K8S CONTROL PLANE MASTER machine ######################################################
####################################################################################################################
list_of_cpms="accl-ffm-vm-cp1"
_HOSTNAME_CONTAINS_CP=$( hostname )
if exists_in_list "$list_of_cpms" " " $_HOSTNAME_CONTAINS_CP; then

  apt install openjdk-8-jre-headless -y
 
  echo "_HOSTNAME_CONTAINS_CP: $_HOSTNAME_CONTAINS_CP"

  THIS_IP="$( cat /tmp/ip.conf )"

  K8S_ADMIN_CONF_OUTPUT_FILE=./from_master_admin.conf
  OUTPUT_FILE=./join.sh
  KEY_FILE=./id_rsa.pub

  rm -rf $K8S_ADMIN_CONF_OUTPUT_FILE
  rm -rf $OUTPUT_FILE

  if [[ -f "/root/.ssh/id_rsa" ]]; then
    echo "DELETING /root/.ssh/id_rsa."
    rm /root/.ssh/id_rsa
  fi
  # Create key
  ssh-keygen -q -t rsa -b 4096 -N '' -f /root/.ssh/id_rsa
  cat /root/.ssh/id_rsa.pub >> /root/.ssh/authorized_keys
  cat /root/.ssh/id_rsa.pub > ${KEY_FILE}

  echo "BEFORE KUBEADM"
  sudo kubeadm init --control-plane-endpoint="${THIS_IP}:6443" --upload-certs --apiserver-advertise-address=${THIS_IP} --pod-network-cidr=10.244.0.0/16 > ${OUTPUT_FILE}

  echo "AFTER KUBEADM"
  ##CANT REMEMBER
  #sudo kubeadm init --apiserver-advertise-address=${THIS_IP} --pod-network-cidr=10.233.0.0/24 | grep -Ei "kubeadm join|discovery-token-ca-cert-hash" > ${OUTPUT_FILE}
  #chmod +x $OUTPUT_FILE

  #GC get the admin.conf file to be able to login to the cluster with k get all -0 wide
  sudo cat /etc/kubernetes/admin.conf > ${K8S_ADMIN_CONF_OUTPUT_FILE}

  # Configure kubectl for vagrant and root users
  #mkdir -p $HOME/.kube
  sudo cp /etc/kubernetes/admin.conf $HOME/.kube/config
  #sudo chown $(id -u):$(id -g) $HOME/.kube/config
  #sudo mkdir -p /root/.kube
  sudo cp /etc/kubernetes/admin.conf /root/.kube/config
  #sudo chown -R root:root /root/.kube
  
  echo "BEFORE KUBECTL FLANNEL"

  #The images must be there first!  --pod-network-cidr=10.244.0.0/16 must be there above
  #kubectl apply -f https://github.com/coreos/flannel/raw/master/Documentation/kube-flannel.yml
  kubectl apply -f k8s-v1-mmi-install-kubernetes-kube-flannel.yml
  
  echo "AFTER KUBECTL FLANNEL"


fi





echo " "
echo " "
echo "All commands finished."

exit 0
