#!/bin/bash
#set -x



read -p "Are you sure you want to run this script? " -n 1 -r
echo    # (optional) move to a new line
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1 # handle exits from shell or function but don't exit interactive shell
fi




if [[ -f "/tmp/tempGC.log" ]]; then
    echo "DELETING /tmp/tempGC.log."
    rm /tmp/tempGC.log 
fi
if [[ -f "/tmp/ip.conf" ]]; then
    echo "DELETING /tmp/ip.conf."
    echo "DELETING /tmp/ip.conf." >> /tmp/tempGC.log
    rm /tmp/ip.conf    
fi
if [[ -f "/tmp/if.conf" ]]; then
    echo "DELETING /tmp/if.conf."
    echo "DELETING /tmp/if.conf." >> /tmp/tempGC.log
    rm /tmp/if.conf    
fi

_LB_FLOATING_IP="143.244.206.194"

# Create local host entries




#https://www.baeldung.com/linux/check-variable-exists-in-list
function exists_in_list() {
    LIST=$1
    DELIMITER=$2
    VALUE=$3
    [[ "$LIST" =~ ($DELIMITER|^)$VALUE($DELIMITER|$) ]]
}

apt-get update -y
apt-get install -y net-tools
apt-get install -y mc
apt-get install -y jq
apt-get install nfs-common -y


#Get the External IP Address
_ROUTE_IP=$( ip route get 8.8.8.8  | head -n 1 | tr -s ' ' | cut -d ' ' -f 7 | grep -oE "\b([0-9]{1,3}\.){3}([0-9]{1,3})\b" )
_HOST_IP=$( hostname -I | grep -P -o "${_ROUTE_IP}(?=)" | grep -oE "\b([0-9]{1,3}\.){3}([0-9]{1,3})\b" )
if [[ "$_ROUTE_IP" == "$_HOST_IP" ]]; then
  echo "_HOST_IP: $_HOST_IP"
  echo $_HOST_IP | tee /tmp/ip.conf
else
    #ALERT HERE
    echo "_ROUTE_IP: $_ROUTE_IP **DOES NOT MATCH** _HOST_IP: $_HOST_IP     WILL EXIT NOW."
    exit 1
fi

#At this point 2 different functions confirmed the IP Address move on to get the INTERFACE NAME

#GET THE INTERFACE NAME WITH THE IP KNOWN.
_INTERFACE_NAME=$( ip addr | sed -z 's/\n/@@/g' | grep -P -o "@@[\d]{1,3}.*(?=${_HOST_IP})" | cut -d':' -f2 | sed -z 's/\s//g' )

#GETTING THE INTERFACE NAME WITH THE EXTERNAL IP.
_INTERFACE_NAME_ROUTE=$( ip route get 8.8.8.8 | head -n 1 | tr -s ' ' | cut -d ' ' -f 5 )


list_of_k8s_PHYSICAL_MACHINE="accl-ffm-srv-008 accl-ffm-srv-009 accl-ffm-srv-010 ACCL-FFM-SRV-008 ACCL-FFM-SRV-009 ACCL-FFM-SRV-010"
_HOSTNAME_CONTAINS_K8S_PHYSICAL_MACHINE=$( hostname )
if exists_in_list "$list_of_k8s_PHYSICAL_MACHINE" " " $_HOSTNAME_CONTAINS_K8S_PHYSICAL_MACHINE; then
    echo "_INTERFACE_NAME: $_INTERFACE_NAME"
    echo $_INTERFACE_NAME | tee /tmp/if.conf
else

  echo "_HOSTNAME_CONTAINS_K8S: $_HOSTNAME_CONTAINS_K8S" 

  if [[ "$_INTERFACE_NAME" == "$_INTERFACE_NAME_ROUTE" ]]; then
    echo "_INTERFACE_NAME: $_INTERFACE_NAME"
    echo $_INTERFACE_NAME | tee /tmp/if.conf
  else
      #ALERT HERE
      echo "_INTERFACE_NAME: $_INTERFACE_NAME **DOES NOT MATCH** _INTERFACE_NAME_RROUTE: $_INTERFACE_NAME_RROUTE     WILL EXIT NOW."
      exit 1
  fi

fi




echo "Sleeping 10"
sleep 10

##########################################################################




if [[ ! -f "/tmp/ip.conf" ]]; then
    echo "/tmp/ip.conf DOES NOT EXIST."
    echo "/tmp/ip.conf DOES NOT EXIST." >> /tmp/tempGC.log
    exit 1
fi
if [[ ! -f "/tmp/if.conf" ]]; then
    echo "/tmp/if.conf DOES NOT EXIST."
    echo "/tmp/if.conf DOES NOT EXIST." >> /tmp/tempGC.log
    exit 1
fi


THIS_IP="$( cat /tmp/ip.conf )"
echo "THIS_IP : ${THIS_IP}" >> /tmp/tempGC.log
INTERFACE_NAME="$( cat /tmp/if.conf )"
echo "INTERFACE_NAME : ${INTERFACE_NAME}" >> /tmp/tempGC.log

# bridged traffic to iptables is enabled for kube-router.
cat >> /etc/ufw/sysctl.conf <<EOF
net/bridge/bridge-nf-call-ip6tables = 1
net/bridge/bridge-nf-call-iptables = 1
net/bridge/bridge-nf-call-arptables = 1
EOF

# Set external DNS
sed -i -e 's/#DNS=/DNS=8.8.8.8/' /etc/systemd/resolved.conf
service systemd-resolved restart

set -e
echo "ADDRESS into etc hosts: ${THIS_IP}" >> /tmp/tempGC.log
sed -e "s/^.*${HOSTNAME}.*/${THIS_IP} ${HOSTNAME} ${HOSTNAME}.local/" -i /etc/hosts

# remove ubuntu-bionic entry
sed -e '/^.*ubuntu-bionic.*/d' -i /etc/hosts



####################################################################################################################
############### Install in a K8S machine ###########################################################################
####################################################################################################################
list_of_k8s="accl-ffm-vm-cp1 accl-ffm-vm-cp2 accl-ffm-vm-cp3 accl-ffm-srv-008 accl-ffm-srv-009 accl-ffm-srv-010 ACCL-FFM-SRV-008 ACCL-FFM-SRV-009 ACCL-FFM-SRV-010"
_HOSTNAME_CONTAINS_K8S=$( hostname )
if exists_in_list "$list_of_k8s" " " $_HOSTNAME_CONTAINS_K8S; then

  echo "_HOSTNAME_CONTAINS_K8S: $_HOSTNAME_CONTAINS_K8S" 

  # disable swap
  swapoff -a
  sed -i '/swap/d' /etc/fstab

  #disable firewall
  UFW_STATUS="$(ufw status)"
  echo "UFW_STATUS: [${UFW_STATUS}]"
  UFW_STATUS_GREP=$(echo ${UFW_STATUS} | sed 's/ //g' | sed 's/://g')
  echo "UFW_STATUS_GREP: [${UFW_STATUS_GREP}]"
  if [ "${UFW_STATUS_GREP}" = "Statusactive" ]; then
    echo "Will disable UFW"
    sudo systemctl disable --now ufw
  else
    echo "UFW not Active"  
  fi


  # Install kubeadm, kubectl and kubelet
  export DEBIAN_FRONTEND=noninteractive
  apt-get -qq install ebtables ethtool
  apt-get -qq update
  sleep 8

  ####################################################################################################################
  ############### Install containerd #################################################################################
  ####################################################################################################################
  _INSTALL_CONTAINERD=true 
  #SYSTEMCTL_CONTAINERD_PAGER="$(systemctl list-unit-files --type service --no-pager)"
  SYSTEMCTL_CONTAINERD="$( systemctl list-units --all -t service --full --no-legend "containerd.service" | sed 's/^\s*//g' | cut -f1 -d' ' )"  
  if [[ ! -z ${SYSTEMCTL_CONTAINERD} ]]; then    
    
    STATUS_CONTAINERD="$(systemctl is-active containerd.service)"
    RUNNING_CONTAINERD="$(systemctl status containerd | grep running)"
    echo "CONTAINERD Status: ${STATUS_CONTAINERD}"
    echo "CONTAINERD Running: ${RUNNING_CONTAINERD}"
    #If it finds Containerd and it is running then it will NOT install
    if [[ "${STATUS_CONTAINERD}" = "active" ]] && [[ ! -z ${RUNNING_CONTAINERD} ]]; then
        _INSTALL_CONTAINERD=false
        VERSION_CONTAINERD="$(containerd -version)"
        echo "CONTANINERD will NOT be added it is already running version: ${VERSION_CONTAINERD}"
    fi
    
  else   
    echo "ContainerD not found in command: systemctl list-unit-files --type service --no-pager"
  fi  #if [[ ! -z ${SYSTEMCTL_CONTAINERD} ]]; then
  
  
  
  
  echo "CONTAINERD Install boolean: ${_INSTALL_CONTAINERD}"     
  if ${_INSTALL_CONTAINERD} ; then 
  
cat <<EOF | sudo tee /etc/modules-load.d/containerd.conf
overlay
br_netfilter
EOF

    sudo modprobe overlay
    sudo modprobe br_netfilter

  # Setup required sysctl params, these persist across reboots.
cat <<EOF | sudo tee /etc/sysctl.d/99-kubernetes-cri.conf
net.bridge.bridge-nf-call-iptables  = 1
net.ipv4.ip_forward                 = 1
net.bridge.bridge-nf-call-ip6tables = 1
EOF

    # Apply sysctl params without reboot
    sudo sysctl --system
    sudo apt-get install -y containerd
    sudo mkdir -p /etc/containerd
    containerd config default | sudo tee /etc/containerd/config.toml
    sudo systemctl restart containerd
    #service containerd status
    
  fi #if ${_INSTALL_CONTAINERD} ; then  


  ####################################################################################################################
  ############### Install kubelet kubeadm kubectl ####################################################################
  ####################################################################################################################
  #curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -

  echo "BEFORE CURL CMD apt-key.gpg" 
  curl -O https://packages.cloud.google.com/apt/doc/apt-key.gpg 
  echo "AFTER CURL CMD apt-key.gpg" 
  apt-key add apt-key.gpg
  echo "AFTER CURL CMD apt-key add apt-key.gpg" 

  
cat <<EOF >/etc/apt/sources.list.d/kubernetes.list
deb http://apt.kubernetes.io/ kubernetes-xenial main
EOF


  #apt-mark unhold kubelet kubectl kubeadm
  apt-get -qq update
  sleep 8
  #apt-get -qq install -y kubelet kubeadm kubectl
  apt-get -qq install -y kubeadm=1.24.7-00 kubelet=1.24.7-00 kubectl=1.24.7-00
  #apt-get -qq install -y kubeadm=1.23.8-00 kubelet=1.23.8-00 kubectl=1.23.8-00
  #apt-get -qq install -y kubeadm=1.22.8-00 kubelet=1.22.8-00 kubectl=1.22.8-00
  #apt-mark hold kubelet kubectl kubeadm

  sleep 15


fi  #if exists_in_list "$list_of_k8s" " " $_HOSTNAME_CONTAINS_K8S; then



####################################################################################################################
############### Install in a K8S CONTROL PLANE MASTER machine ######################################################
############### Install in a K8S CONTROL PLANE MASTER machine ######################################################
####################################################################################################################
list_of_cpms="accl-ffm-vm-cp1 accl-ffm-vm-cp2 accl-ffm-vm-cp3"
_HOSTNAME_CONTAINS_CP=$( hostname )
if exists_in_list "$list_of_cpms" " " $_HOSTNAME_CONTAINS_CP; then

  apt install openjdk-8-jre-headless -y
 
  echo "_HOSTNAME_CONTAINS_CP: $_HOSTNAME_CONTAINS_CP"

  THIS_IP="$( cat /tmp/ip.conf )"

  K8S_ADMIN_CONF_OUTPUT_FILE=./from_master_admin.conf
  OUTPUT_FILE=./join.sh
  KEY_FILE=./id_rsa.pub

  rm -rf $K8S_ADMIN_CONF_OUTPUT_FILE
  rm -rf $OUTPUT_FILE

  if [[ -f "/root/.ssh/id_rsa" ]]; then
    echo "DELETING /root/.ssh/id_rsa."
    rm /root/.ssh/id_rsa
  fi
  # Create key
  ssh-keygen -q -t rsa -b 4096 -N '' -f /root/.ssh/id_rsa
  cat /root/.ssh/id_rsa.pub >> /root/.ssh/authorized_keys
  cat /root/.ssh/id_rsa.pub > ${KEY_FILE}

  # Start cluster
  #To be used with Load Balancers
  #sudo kubeadm init --control-plane-endpoint="${_LB_FLOATING_IP}:6443" --upload-certs --apiserver-advertise-address=${THIS_IP} --pod-network-cidr=10.233.0.0/16 > ${OUTPUT_FILE}
  echo "BEFORE KUBEADM"
  #USING REGULARLY WITHOUT LBs
  sudo kubeadm init --control-plane-endpoint="${THIS_IP}:6443" --upload-certs --apiserver-advertise-address=${THIS_IP} --pod-network-cidr=10.244.0.0/16 > ${OUTPUT_FILE}
  echo "AFTER KUBEADM"
  ##CANT REMEMBER
  #sudo kubeadm init --apiserver-advertise-address=${THIS_IP} --pod-network-cidr=10.233.0.0/24 | grep -Ei "kubeadm join|discovery-token-ca-cert-hash" > ${OUTPUT_FILE}
  chmod +x $OUTPUT_FILE

  #GC get the admin.conf file to be able to login to the cluster with k get all -0 wide
  sudo cat /etc/kubernetes/admin.conf > ${K8S_ADMIN_CONF_OUTPUT_FILE}

  # Configure kubectl for vagrant and root users
  mkdir -p $HOME/.kube
  sudo cp /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config
  sudo mkdir -p /root/.kube
  sudo cp /etc/kubernetes/admin.conf /root/.kube/config
  sudo chown -R root:root /root/.kube
  echo "BEFORE KUBECTL WEAVE/CALICO"
 
  #DOES NOT WORK for some reason the  UIRL host does not comeback. 10/26/2022
  #kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')&env.IPALLOC_RANGE=10.233.0.0/16"
  
  #The images must be there first!  --pod-network-cidr=10.244.0.0/16 must be there above
  #kubectl apply -f https://github.com/coreos/flannel/raw/master/Documentation/kube-flannel.yml
  kubectl apply -f k8s-v1-mmi-install-kubernetes-kube-flannel.yml
  
  echo "AFTER KUBECTL GIT/WEAVE/CALICO"
  apt-get -qq install -y avahi-daemon libnss-mdns

else
    #ALERT HERE
    echo "list_of_cpms: $list_of_cpms **DOES NOT MATCH** _HOSTNAME_CONTAINS_CP: $_HOSTNAME_CONTAINS_CP     WILL EXIT NOW."
fi   #if exists_in_list "$list_of_cpms" " " $_HOSTNAME_CONTAINS_CP; then




####################################################################################################################
############### Install in a K8S LB machine ########################################################################
####################################################################################################################
list_of_k8s_lbs="k8s-lb-1 k8s-lb-2"
_HOSTNAME_CONTAINS_K8S_LB=$( hostname )
if exists_in_list "$list_of_k8s_lbs" " " $_HOSTNAME_CONTAINS_K8S_LB; then

  echo "_HOSTNAME_CONTAINS_K8S_LB: $_HOSTNAME_CONTAINS_K8S_LB" 
  
  sudo apt-get -qq install -y keepalived haproxy 

cat >> /etc/keepalived/check_apiserver.sh <<EOF
#!/bin/sh

errorExit() {
  echo "*** $@" 1>&2
  exit 1
}

curl --silent --max-time 2 --insecure https://localhost:6443/ -o /dev/null || errorExit "Error GET https://localhost:6443/"
if ip addr | grep -q ${_LB_FLOATING_IP}; then
  curl --silent --max-time 2 --insecure https://${_LB_FLOATING_IP}:6443/ -o /dev/null || errorExit "Error GET https://${_LB_FLOATING_IP}:6443/"
fi
EOF

  chmod +x /etc/keepalived/check_apiserver.sh

cat >> /etc/keepalived/keepalived.conf <<EOF
vrrp_script check_apiserver {
  script "/etc/keepalived/check_apiserver.sh"
  interval 3
  timeout 10
  fall 5
  rise 2
  weight -2
}

vrrp_instance VI_1 {
    state BACKUP
    #"ip a s" will get you the value for the interface to use based on ip address 10.0.0.?   "sudo journalctl -flu keepalived"
    interface ${INTERFACE_NAME}
    virtual_router_id 1
    priority 100
    advert_int 5
    authentication {
        auth_type PASS
        auth_pass mysecret
    }
    virtual_ipaddress {
        ${_LB_FLOATING_IP}
    }
    track_script {
        check_apiserver
    }
}
EOF

  sudo systemctl enable --now keepalived

cat >> /etc/haproxy/haproxy.cfg <<EOF

frontend kubernetes-frontend
  bind *:6443
  mode tcp
  option tcplog
  default_backend kubernetes-backend

backend kubernetes-backend
  option httpchk GET /healthz
  http-check expect status 200
  mode tcp
  option ssl-hello-chk
  balance roundrobin
    server  k8s-cp-1 165.227.137.241:6443 check fall 3 rise 2
    #server master2 10.0.0.12:6443 check fall 3 rise 2
    #server master3 10.0.0.13:6443 check fall 3 rise 2

EOF

  sudo systemctl enable haproxy 
  sudo systemctl restart haproxy
  
  
  
fi #if exists_in_list "$list_of_k8s_lbs" " " $_HOSTNAME_CONTAINS_K8S_LB; then






####################################################################################################################
############### ALL HAS HAPPENED SEND FINISH NOTE ##################################################################
####################################################################################################################
echo "All steps performed exiting code 0."
exit 0