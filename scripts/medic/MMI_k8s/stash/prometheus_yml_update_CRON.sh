#!/usr/bin/env bash
#set -x

_GyrexPrometheusPort=8055
_finalIpsPortArray=()
_prometheusyamlFile=prometheus.yml

cd /root/docker/images/prometheus


#######################################################################################################################################################
#One container on the K8s System is weave and it holds the subnet we need to validate against.
#weave-net-lwzrk                            2/2     Running   8 (7d3h ago)   9d    192.168.2.16   accl-ffm-srv-006   <none>           <none>
_weavenet=$(kubectl get pod -n kube-system -o wide | grep weave | cut -d ' ' -f1 )
echo "_weavenet: $_weavenet"

#The default subnet is the one that lets us know the conntainer is part of kubernetes network.
#          Range: 10.32.0.0/12
#  DefaultSubnet: 10.32.0.0/12
_subnet=$( kubectl exec -n kube-system $_weavenet -c weave -- /home/weave/weave --local status | sed -En "s/^(.*)(DefaultSubnet:\s)(.*)?/\3/p" )
echo "_subnet: $_subnet"
_cidr2=$( echo "$_subnet" | cut -d '/' -f2 )
echo "_cidr2: /$_cidr2"



#######################################################################################################################################################
#This is an array of the currently monitored containers that prometheus was sstarted with.
#We will remove any containers form the array that fit the K8s Weavenet subnet with the gyrex prometheus port.
_targetLineFound_array=($( egrep '^\s{1,20}-\s{0,5}targets\s{0,5}:\s{0,5}\[.*\]' $_prometheusyamlFile | sed -En "s/(.*-\stargets:\s\[)(.*)(\]).*/\2/p"  | tr "," "\n"))
for index in "${_targetLineFound_array[@]}"
do
    _ip="${index//\'/$''}"
    _ipTocheck=$( echo $_ip | cut -d ':' -f1 )
    _portTocheck=$( echo $_ip | cut -d ':' -f2 )
    #We need to check if the IP is within the subnet mask attained from K8s.
    #The port must also be the prometheus port in case some other port is used also for Prometheus.
    #This means the IP should be removed since we will put the list of IPs from
    #K8s currently in production by Deployment/AutoScale rules.
    #Network:   10.32.0.0/12
    _isIpWithinSubnet=$( ipcalc $_ipTocheck/$_cidr2 | sed -En "s/^(.*)(Network:\s+)([0-9]{1}[0-9]?[0-9]?\.[0-9]{1}[0-9]?[0-9]?\.[0-9]{1}[0-9]?[0-9]?\.[0-9]{1}[0-9]?[0-9]?)(\/[0-9]{1}[0-9]{1}.*)?/\3/p" )
    if [[ "$_isIpWithinSubnet/$_cidr2" == "$_subnet" && "$_portTocheck" == "$_GyrexPrometheusPort" ]]; then
        echo "IP managed by K8s will be deleted: _isIpWithinSubnet: ($_ip) $_isIpWithinSubnet"
    else
        _finalIpsPortArray+=("$_ip")
    fi
done



#######################################################################################################################################################
#This is an array of the current running gyrex App containers with a prometheus port that is available.
#From this list we will add them to the prometheus file to be available for monitoring.
readarray -t _currentK8sIpsArr < <( kubectl get pods --all-namespaces --chunk-size=0 -o json | jq '.items[] | select(.spec.containers[].ports != null) | select(.spec.containers[].ports[].containerPort == '$_GyrexPrometheusPort' ) | .status.podIP' )
for index in "${!_currentK8sIpsArr[@]}"
do
    _addIPToMonitoring=${_currentK8sIpsArr[index]//\"/$''}
    echo "IP Managed by K8s as gyrex app with prometheus currently running will be added to monitoring: $_addIPToMonitoring"
    _finalIpsPortArray+=("$_addIPToMonitoring:$_GyrexPrometheusPort")
done



######################################################################################################################################################
#we need to recreate this string and sed it into the file
#- targets: ['192.168.2.13:3201', '192.168.2.13:3202', '10.32.0.7:8055', '10.32.0.8:8055']

_finalPrometheusTargetString="- targets: ["
i=0

# Iterate the loop to read and print each array element
for index in "${!_finalIpsPortArray[@]}"
do
    ((i=i+1))
    _finalPrometheusTargetString="$_finalPrometheusTargetString '${_finalIpsPortArray[index]}'"
    if [[ $i != ${#_finalIpsPortArray[@]}  ]]; then
         _finalPrometheusTargetString="$_finalPrometheusTargetString,"
    fi
done

_finalPrometheusTargetString="$_finalPrometheusTargetString]"

echo "$_finalPrometheusTargetString"

#sed "-i.`date +%F`" -E "s/(.*)-\stargets:\s\[.*\]/\1$_finalPrometheusTargetString/" ./$_prometheusyamlFile


echo "All changes were made. Exiting"

exit 0
