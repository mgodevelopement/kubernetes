#!/bin/bash
#set -x

#example call "/${K8S_YAML_PATH}/k8s-v1-mmi-clean-containerd-images.sh"

_LOG_FILE="/tmp/tempGC_containerd_images.log"

read -p "Are you sure you want to run this script? " -n 1 -r
echo    # (optional) move to a new line
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1 # handle exits from shell or function but don't exit interactive shell
fi



if [[ -f "/tmp/containerd_images.log" ]]; then
    echo "DELETING /tmp/containerd_images.log."
    rm /tmp/containerd_images.log
fi
if [[ -f "/tmp/containerd_known_running_images.log" ]]; then
    echo "DELETING /tmp/containerd_known_running_images.log."
    echo "DELETING /tmp/containerd_known_running_images.log." >> /tmp/containerd_images.log
    rm /tmp/containerd_known_running_images.log
fi
if [[ -f "/tmp/containerd_delete_these_images.log" ]]; then
    echo "DELETING /tmp/containerd_delete_these_images.log."
    echo "DELETING /tmp/containerd_delete_these_images.log." >> /tmp/containerd_images.log
    rm /tmp/containerd_delete_these_images.log
fi
if [[ -f "/tmp/containerd_this_nodes_images.log" ]]; then
    echo "DELETING /tmp/containerd_this_nodes_images.log."
    echo "DELETING /tmp/containerd_this_nodes_images.log." >> /tmp/containerd_images.log
    rm /tmp/containerd_this_nodes_images.log
fi


#https://www.baeldung.com/linux/reading-output-into-array
IFS=$'\n' read -r -d '' -a containerd_known_running_images_array < <( kubectl get pods --all-namespaces -o jsonpath="{.items[*].spec.containers[*].image}" | tr -s '[[:space:]]' '\n' | awk -F/ '{print $NF}' | awk '!/sha25|REF/' | sort | uniq )
#declare -p containerd_known_running_images_array

IFS=$'\n' read -r -d '' -a containerd_this_nodes_images_array < <( ctr -n=k8s.io image ls | awk '{print $1}' | awk -F/ '{print $NF}' | awk '!/sha25|REF/' | sort | uniq )
#declare -p containerd_this_nodes_images_array


for _THIS_NODE_IMAGE in "${containerd_this_nodes_images_array[@]}"
do
    _FOUND_IMAGE=false 
    for _KNOWN_CONTIANERD_IMAGE in "${containerd_known_running_images_array[@]}"
    do
        if [[ "$_THIS_NODE_IMAGE" = "$_KNOWN_CONTIANERD_IMAGE" ]]
        then            
            _FOUND_IMAGE=true
        fi
    done
    if ! $_FOUND_IMAGE; then
      echo "&& ctr -n=k8s.io images rm $( ctr -n=k8s.io image ls | grep .*${_THIS_NODE_IMAGE} | awk {'print $1'}) \\"
    fi
done



echo " "
echo "All commands finished."
exit 0
