#!/usr/bin/env bash


_FLOATING_IP="172.16.0.174"
_AUTH_PASS="marlb0r0"
_K8S_VERSION="1.26.3-00"
_K8S_VERSION_KUBECTL="v1.26.3"
_K8s_DIRECTORY="/tmp/k8s"
_CP_STRING_HAPROXY_FILE="_CP_STRING_HAPROXY.txt"


_HOST_NAMES_CP="k8s-master-a k8s-master-b k8s-master-c"
_HOST_NAMES_K8S_ALL="k8s-master-a k8s-master-b k8s-master-c k8s-node-a k8s-node-b k8s-node-c"
_HOST_NAMES_LBS="k8s-haproxy"
_HOST_NAMES_CLIENTS="k8s-haproxy"

####################################################################################################################
########################## Change the Hosts file to add all servers ################################################
########################## Extremely IMPORTANT that this list is correct ###########################################
####################################################################################################################
_HOST_SERVERS_ARRAY=()
_HOST_SERVERS_ARRAY=("${_HOST_SERVERS_ARRAY[@]}" "157.230.120.67  k8s-haproxy")
_HOST_SERVERS_ARRAY=("${_HOST_SERVERS_ARRAY[@]}" "139.59.136.234  k8s-master-a")
_HOST_SERVERS_ARRAY=("${_HOST_SERVERS_ARRAY[@]}" "139.59.135.133  k8s-master-b")
_HOST_SERVERS_ARRAY=("${_HOST_SERVERS_ARRAY[@]}" "157.230.117.49  k8s-master-c")
_HOST_SERVERS_ARRAY=("${_HOST_SERVERS_ARRAY[@]}" "157.230.113.249 k8s-node-a")
_HOST_SERVERS_ARRAY=("${_HOST_SERVERS_ARRAY[@]}" "64.226.81.209   k8s-node-b")
_HOST_SERVERS_ARRAY=("${_HOST_SERVERS_ARRAY[@]}" "64.226.89.6     k8s-node-c")





# _HOST_NAMES_CP="accl-ffm-vm-cp1 accl-ffm-vm-cp2 accl-ffm-vm-cp3"
# _HOST_NAMES_K8S_ALL="accl-ffm-vm-cp1 accl-ffm-vm-cp2 accl-ffm-vm-cp3 accl-ffm-srv-008 accl-ffm-srv-009 accl-ffm-srv-010 ACCL-FFM-SRV-008 ACCL-FFM-SRV-009 ACCL-FFM-SRV-010"
# _HOST_NAMES_LBS="accl-ffm-vm-lb1 accl-ffm-vm-lb2 accl-ffm-vm-lb3"
# _HOST_NAMES_CLIENTS="accl-ffm-vm-lb1"

####################################################################################################################
########################## Change the Hosts file to add all servers ################################################
########################## Extremely IMPORTANT that this list is correct ###########################################
####################################################################################################################
# _HOST_SERVERS_ARRAY=()
# _HOST_SERVERS_ARRAY=("${_HOST_SERVERS_ARRAY[@]}" "172.16.0.1    ACCL-FFM-VM-LB1")
# _HOST_SERVERS_ARRAY=("${_HOST_SERVERS_ARRAY[@]}" "172.16.0.2    ACCL-FFM-VM-LB2")
# _HOST_SERVERS_ARRAY=("${_HOST_SERVERS_ARRAY[@]}" "172.16.0.3    ACCL-FFM-VM-LB3")
# _HOST_SERVERS_ARRAY=("${_HOST_SERVERS_ARRAY[@]}" "172.16.0.4    ACCL-FFM-VM-ETCD1")
# _HOST_SERVERS_ARRAY=("${_HOST_SERVERS_ARRAY[@]}" "172.16.0.5    ACCL-FFM-VM-ETCD2")
# _HOST_SERVERS_ARRAY=("${_HOST_SERVERS_ARRAY[@]}" "172.16.0.6    ACCL-FFM-VM-ETCD3")
# _HOST_SERVERS_ARRAY=("${_HOST_SERVERS_ARRAY[@]}" "172.16.0.7    ACCL-FFM-VM-CP1")
# _HOST_SERVERS_ARRAY=("${_HOST_SERVERS_ARRAY[@]}" "172.16.0.20   ACCL-FFM-VM-CP2")
# _HOST_SERVERS_ARRAY=("${_HOST_SERVERS_ARRAY[@]}" "172.16.0.21   ACCL-FFM-VM-CP3")
# _HOST_SERVERS_ARRAY=("${_HOST_SERVERS_ARRAY[@]}" "172.16.0.8    ACCL-FFM-SRV-008")
# _HOST_SERVERS_ARRAY=("${_HOST_SERVERS_ARRAY[@]}" "172.16.0.9    ACCL-FFM-SRV-009")
# _HOST_SERVERS_ARRAY=("${_HOST_SERVERS_ARRAY[@]}" "172.16.0.10   ACCL-FFM-SRV-010")
# _HOST_SERVERS_ARRAY=("${_HOST_SERVERS_ARRAY[@]}" "172.16.0.11   ACCL-FFM-SRV-011")
# _HOST_SERVERS_ARRAY=("${_HOST_SERVERS_ARRAY[@]}" "172.16.0.12   ACCL-FFM-SRV-012")






read -p "Are you sure you want to run this script? " -n 1 -r
echo    # (optional) move to a new line
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1 # handle exits from shell or function but don't exit interactive shell
fi



#https://www.baeldung.com/linux/check-variable-exists-in-list
function exists_in_list() {
    LIST=$1
    DELIMITER=$2
    VALUE=$3
    [[ "$LIST" =~ ($DELIMITER|^)$VALUE($DELIMITER|$) ]]
}




if [[ -f "/tmp/tempGC.log" ]]; then
    echo "DELETING /tmp/tempGC.log."
    rm /tmp/tempGC.log
fi
if [[ -f "/tmp/ip.conf" ]]; then
    echo "DELETING /tmp/ip.conf."
    echo "DELETING /tmp/ip.conf." >> /tmp/tempGC.log
    rm /tmp/ip.conf
fi
if [[ -f "/tmp/if.conf" ]]; then
    echo "DELETING /tmp/if.conf."
    echo "DELETING /tmp/if.conf." >> /tmp/tempGC.log
    rm /tmp/if.conf
fi






#Get the External IP Address
_ROUTE_IP=$( ip route get 8.8.8.8  | head -n 1 | tr -s ' ' | cut -d ' ' -f 7 | grep -oE "\b([0-9]{1,3}\.){3}([0-9]{1,3})\b" )
_HOST_IP=$( hostname -I | grep -P -o "${_ROUTE_IP}(?=)" | grep -oE "\b([0-9]{1,3}\.){3}([0-9]{1,3})\b" )
if [[ "$_ROUTE_IP" == "$_HOST_IP" ]]; then
  echo "_HOST_IP: $_HOST_IP"
  echo $_HOST_IP | tee /tmp/ip.conf
else
    #ALERT HERE
    echo "_ROUTE_IP: $_ROUTE_IP **DOES NOT MATCH** _HOST_IP: $_HOST_IP     WILL EXIT NOW."
    exit 1
fi

#At this point 2 different functions confirmed the IP Address move on to get the INTERFACE NAME

#GET THE INTERFACE NAME WITH THE IP KNOWN.
_INTERFACE_NAME=$( ip addr | sed -z 's/\n/@@/g' | grep -P -o "@@[\d]{1,3}.*(?=${_HOST_IP})" | cut -d':' -f2 | sed -z 's/\s//g' )

#GETTING THE INTERFACE NAME WITH THE EXTERNAL IP.
_INTERFACE_NAME_ROUTE=$( ip route get 8.8.8.8 | head -n 1 | tr -s ' ' | cut -d ' ' -f 5 )


_HOSTNAME_CONTAINS_K8S_PHYSICAL_MACHINE=$( hostname )
if exists_in_list "${_HOST_NAMES_K8S_ALL}" " " $_HOSTNAME_CONTAINS_K8S_PHYSICAL_MACHINE; then
    echo "_INTERFACE_NAME: $_INTERFACE_NAME"
    echo $_INTERFACE_NAME | tee /tmp/if.conf
else

  echo "_HOSTNAME_CONTAINS_K8S: $_HOSTNAME_CONTAINS_K8S"

  if [[ "$_INTERFACE_NAME" == "$_INTERFACE_NAME_ROUTE" ]]; then
    echo "_INTERFACE_NAME: $_INTERFACE_NAME"
    echo $_INTERFACE_NAME | tee /tmp/if.conf
  else
      #ALERT HERE
      echo "_INTERFACE_NAME: $_INTERFACE_NAME **DOES NOT MATCH** _INTERFACE_NAME_RROUTE: $_INTERFACE_NAME_RROUTE     WILL EXIT NOW."
      exit 1
  fi

fi

#Cannot continue with missing variables.
if [[ -z $_HOST_IP || -z ${_FLOATING_IP}  || -z ${_INTERFACE_NAME_ROUTE} || -z ${_AUTH_PASS} ]]; then
  echo "Variables are missing. exiting script. exit 1"
  exit 1
else
  echo "variables used: [ $_HOST_IP || ${_FLOATING_IP} || ${_INTERFACE_NAME_ROUTE} || ${_AUTH_PASS} ]"
fi





echo "Sleeping 5"
sleep 5




if [[ ! -f "/tmp/ip.conf" ]]; then
    echo "/tmp/ip.conf DOES NOT EXIST."
    echo "/tmp/ip.conf DOES NOT EXIST." >> /tmp/tempGC.log
    exit 1
fi
if [[ ! -f "/tmp/if.conf" ]]; then
    echo "/tmp/if.conf DOES NOT EXIST."
    echo "/tmp/if.conf DOES NOT EXIST." >> /tmp/tempGC.log
    exit 1
fi





THIS_IP="$( cat /tmp/ip.conf )"
echo "THIS_IP : ${THIS_IP}" >> /tmp/tempGC.log
INTERFACE_NAME="$( cat /tmp/if.conf )"
echo "INTERFACE_NAME : ${INTERFACE_NAME}" >> /tmp/tempGC.log



# Set external DNS
sed -i -e 's/#DNS=/DNS=8.8.8.8/' /etc/systemd/resolved.conf
service systemd-resolved restart

set -e
echo "ADDRESS into etc hosts: ${THIS_IP}" >> /tmp/tempGC.log
sed -e "s/^.*${HOSTNAME}.*/${THIS_IP} ${HOSTNAME} ${HOSTNAME}.local/" -i /etc/hosts

# remove ubuntu-bionic entry
sed -e '/^.*ubuntu-bionic.*/d' -i /etc/hosts

echo 'EDITOR="/usr/bin/nano"' >> /etc/environment
echo 'K8S_YAML_PATH="/mnt/nas_backup/scripts/kubernetes/yamls"' >> /etc/environment

sudo apt update
sleep 5
sudo apt -y full-upgrade
sleep 5

[ -f /var/run/reboot-required ] && sudo reboot -f

sleep 5





####################################################################################################################
########################## Change the Hosts file to add all servers ################################################
########################## Extremely IMPORTANT that this list is correct ###########################################
####################################################################################################################
for _HOST_SERVER in "${_HOST_SERVERS_ARRAY[@]}"; do
   _IP_ADDRESS_HOST="$(echo "$_HOST_SERVER" | awk '{print $1}')"; echo "$_IP_ADDRESS_HOST" #If the IP address is already in the hosts file dont add it.
   if ! grep -q "${_IP_ADDRESS_HOST}" /etc/hosts; then
     echo "Appended to /etc/hosts: ${_HOST_SERVER}"
cat >> /etc/hosts <<EOF
${_HOST_SERVER}
EOF
   fi
done





#This list is needed for HAProxy later
_LB_CP_NODES_IP_LIST=""
for lb_node in ${_HOST_NAMES_LBS};
do
  _IP_ADDRESS_LB=$( getent hosts ${lb_node} | awk '{ print $1 }' )
  if [[ -z ${_IP_ADDRESS_LB} ]]; then
    echo "load balancer machine (${lb_node}) does not have an IP    WILL EXIT NOW."
    exit 1
  else
    _LB_CP_NODES_IP_LIST+="${_IP_ADDRESS_LB},"
  fi
done
for ctrl_plane_node in ${_HOST_NAMES_CP};
do
  _IP_ADDRESS_CP=$( getent hosts ${ctrl_plane_node} | awk '{ print $1 }' )
  if [[ -z ${_IP_ADDRESS_CP} ]]; then
    echo "Control Plane machine (${ctrl_plane_node}) does not have an IP    WILL EXIT NOW."
    exit 1
  else
    _LB_CP_NODES_IP_LIST+="${_IP_ADDRESS_CP},"
  fi
done

echo "_LB_CP_NODES_IP_LIST: ${_LB_CP_NODES_IP_LIST}"






cd /tmp

if [[ -d "${_K8s_DIRECTORY}" ]]; then
  rm -rf ${_K8s_DIRECTORY}
fi

mkdir -p ${_K8s_DIRECTORY}

cd ${_K8s_DIRECTORY}


apt update -y && apt upgrade -y
apt install -y jq




####################################################################################################################
############### Install in all K8S machine #########################################################################
####################################################################################################################
_HOSTNAME_CONTAINS_K8S=$( hostname )
if exists_in_list "${_HOST_NAMES_K8S_ALL}" " " ${_HOSTNAME_CONTAINS_K8S}; then

  echo "_HOSTNAME_CONTAINS_K8S: $_HOSTNAME_CONTAINS_K8S"

cat >> ~/.bashrc <<EOL
export EDITOR='/usr/bin/nano'
export K8S_YAML_PATH='/mnt/nas_backup/scripts/kubernetes/yamls'

#GCF personal aliases
alias gan='echo -n "Enter namespace: " && read NS && kubectl get all -o wide -n $NS'
alias gam='watch kubectl get all -o wide -n mmi-medic'
alias gamm=' kubectl get all -o wide -n mmi-medic'
alias cdk='cd /usr/local/bin/MMI_k8s/'
alias kx='f() { [ "$1" ] && kubectl config use-context $1 || kubectl config current-context ; } ; f'
alias kn='f() { [ "$1" ] && kubectl config set-context --current --namespace $1 || kubectl config view --minify | grep namespace | cut -d" " -f6 ; } ; f'
EOL

  source ~/.bashrc

  sudo apt install openjdk-8-jre-headless -y

  sudo apt -y install curl apt-transport-https
  sleep 5
  curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
  sleep 5
  echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
  sleep 5

  sudo apt update
  sleep 5
  sudo apt -y install vim git wget net-tools mc jq nfs-common kubeadm=${_K8S_VERSION} kubelet=${_K8S_VERSION} kubectl=${_K8S_VERSION}
  sleep 5
  sudo apt-mark hold kubelet kubeadm kubectl
  sleep 5

  #kubectl version --client && kubeadm version
  #  Client Version: version.Info{Major:"1", Minor:"23", GitVersion:"v1.23.5", GitCommit:"c285e781331a3785a7f436042c65c5641ce8a9e9", GitTreeState:"clean", BuildDate:"2022-03-16T15:58:47Z", GoVersion:"go1.17.8", Compiler:"gc", Platform:"linux/amd64"}
  #  kubeadm version: &version.Info{Major:"1", Minor:"23", GitVersion:"v1.23.5", GitCommit:"c285e781331a3785a7f436042c65c5641ce8a9e9", GitTreeState:"clean", BuildDate:"2022-03-16T15:57:37Z", GoVersion:"go1.17.8", Compiler:"gc", Platform:"linux/amd64"}

  if grep -q swap "/etc/fstab"; then
    sudo sed -i '/ swap / s/^\(.*\)$/#\1/g' /etc/fstab
    #TODO
    # /swap.img       none    swap    sw      0       0
    sudo swapoff -a
    sleep 5
    sudo mount -a
    sleep 5
    free -h
    sleep 5
  else
    echo "NO SWAP in the fstab file."
  fi



  #LONGHORN issue fix for device name (sdb|sdc)
  #https://longhorn.io/kb/troubleshooting-volume-with-multipath/
  #https://www.esgeroth.org/log/entry/2005
if [[ -f "/etc/multipath.conf" ]]; then
  if ! grep -qE 'devnode\s\"\^sd\[a-z0-9\]\+\"' "/etc/multipath.conf"; then
cat >> /etc/multipath.conf <<EOL
blacklist {
    devnode "^sd[a-z0-9]+"
}
EOL
    sudo systemctl restart multipathd.service
    sleep 3
    echo "LONGHORN multipath issue patched."

    if multipath -t | grep -E 'devnode\s\"\^sd\[a-z0-9\]\+\"'; then
      echo "LONGHORN multipath returned correct code."
    else
      echo "ERROR: LONGHORN multipath returned incorrect code: EXITING NOW."
      exit 1
    fi

  else
    echo "LONGHORN multipath issue already patched."
  fi

else
  echo "Multipath is not installed."
fi


  # Enable kernel modules
  sudo modprobe overlay
  sleep 5
  sudo modprobe br_netfilter
  sleep 5


  # Add some settings to sysctl
sudo tee /etc/sysctl.d/kubernetes.conf<<EOF
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
EOF

  sleep 5

  # Reload sysctl
  sudo sysctl --system

  sleep 5




  ####################################################################################################################
  ############### Install containerd #################################################################################
  ####################################################################################################################
  _INSTALL_CONTAINERD=true
  SYSTEMCTL_CONTAINERD="$( systemctl list-units --all -t service --full --no-legend "containerd.service" | sed 's/^\s*//g' | cut -f1 -d' ' )"
  if [[ ! -z ${SYSTEMCTL_CONTAINERD} ]]; then

    STATUS_CONTAINERD="$(systemctl is-active containerd.service)"
    echo "CONTAINERD Status: ${STATUS_CONTAINERD}"
    RUNNING_CONTAINERD="$(systemctl status containerd | grep running)"
    echo "CONTAINERD Running: ${RUNNING_CONTAINERD}"
    #If it finds Containerd and it is running then it will NOT install
    if [[ "${STATUS_CONTAINERD}" = "active" ]] && [[ ! -z ${RUNNING_CONTAINERD} ]]; then
        _INSTALL_CONTAINERD=false
        VERSION_CONTAINERD="$(containerd -version)"
        echo "CONTANINERD will NOT be added it is already running version: ${VERSION_CONTAINERD}"
    fi

  else
    echo "ContainerD not found in command: systemctl list-unit-files --type service --no-pager"
  fi  #if [[ ! -z ${SYSTEMCTL_CONTAINERD} ]]; then

  echo "CONTAINERD Install boolean: ${_INSTALL_CONTAINERD}"

  if ${_INSTALL_CONTAINERD} ; then

    # Configure persistent loading of modules
sudo tee /etc/modules-load.d/containerd.conf <<EOF
overlay
br_netfilter
EOF

    sleep 5


    # Load at runtime
    sudo modprobe overlay
    sleep 5
    sudo modprobe br_netfilter
    sleep 5

    # Ensure sysctl params are set
sudo tee /etc/sysctl.d/kubernetes.conf<<EOF
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
EOF

    sleep 5

    # Reload configs
    sudo sysctl --system
    sleep 5


    # Install required packages
    sudo apt install -y curl gnupg2 software-properties-common apt-transport-https ca-certificates
    sleep 5


    # Add Docker repo
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
    sleep 5
    sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
    sleep 5

    # Install containerd
    sudo apt update
    sleep 5
    sudo apt install -y containerd.io
    sleep 5

    # Configure containerd and start service
    #sudo su -
    mkdir -p /etc/containerd
    containerd config default>/etc/containerd/config.toml
    sleep 5

    # restart containerd
    sudo systemctl restart containerd
    sleep 5
    sudo systemctl enable containerd
    sleep 5
    #systemctl status  containerd

  fi #if ${_INSTALL_CONTAINERD} ; then



  #Step 5: Initialize master node
  #Login to the server to be used as master and make sure that the br_netfilter module is loaded:
  #$ lsmod | grep br_netfilter
  #    br_netfilter           22256  0
  #    bridge                151336  2 br_netfilter,ebtable_broute



  sudo systemctl enable kubelet
  sleep 5

  sudo kubeadm config images pull
  sleep 5

  # Containerd
  sudo kubeadm config images pull --cri-socket unix:///run/containerd/containerd.sock
  sleep 5

  sudo mkdir -p /root/.kube
  sudo touch /root/.kube/config
  sudo chown -R root:root /root/.kube






fi








####################################################################################################################
######################################## Install cssl ##############################################################
####################################################################################################################
_HOSTNAME_CONTAINS_CM=$( hostname )
if exists_in_list "${_HOST_NAMES_CLIENTS}" " " $_HOSTNAME_CONTAINS_CM; then

  wget https://pkg.cfssl.org/R1.2/cfssl_linux-amd64
  wget https://pkg.cfssl.org/R1.2/cfssljson_linux-amd64
  chmod +x cfssl*

  #Step 3 - Move the binaries to /usr/local/bin
  mv cfssl_linux-amd64 /usr/local/bin/cfssl
  mv cfssljson_linux-amd64 /usr/local/bin/cfssljson

  #step 4 - Verify the installation
  _CSSL_VERSION=$( cfssl version | sed -n 1p )

  if [[ -z ${_CSSL_VERSION} ]]; then
    echo "Could not get a cssl version. quiting script. exit 1"
    exit 1
  else
    echo "CSSL version: ${_CSSL_VERSION}"
  fi
fi



####################################################################################################################
######################### Lets install kubectl on the mashine that will run it #####################################
####################################################################################################################
_HOSTNAME_CONTAINS_CM=$( hostname )
if exists_in_list "${_HOST_NAMES_CLIENTS}" " " $_HOSTNAME_CONTAINS_CM; then

  curl -LO https://storage.googleapis.com/kubernetes-release/release/${_K8S_VERSION_KUBECTL}/bin/linux/amd64/kubectl
  #Step 2 - Add the execution permission to the binary
  chmod +x kubectl
  #Step 3 - Move the binary to /usr/local/bin
  mv kubectl /usr/local/bin
  #Step 4 - Verify the installation
  _KUBECTL_VERSION=$( kubectl version --output=json | jq '.clientVersion.gitVersion' --raw-output )

  if [[ -z ${_KUBECTL_VERSION} ]]; then
    echo "Could not get a kubectl version. exiting script. exit 1"
    exit 1
  else
    echo "KUBECTL version: ${_KUBECTL_VERSION}"
  fi
fi



####################################################################################################################
############### Install in a Client Computer machine create certificates. ##########################################
####################################################################################################################
_HOSTNAME_CONTAINS_CM=$( hostname )
if exists_in_list "${_HOST_NAMES_CLIENTS}" " " $_HOSTNAME_CONTAINS_CM; then


cat > ca-config.json <<EOF
{
  "signing": {
    "default": {
      "expiry": "8760h"
    },
    "profiles": {
      "kubernetes": {
        "usages": ["signing", "key encipherment", "server auth", "client auth"],
        "expiry": "8760h"
      }
    }
  }
}
EOF

cat > ca-csr.json <<EOF
{
  "CN": "Kubernetes",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
  {
    "C": "DE",
    "L": "Germany",
    "O": "mmi",
    "OU": "CA",
    "ST": "gmbh"
  }
 ]
}
EOF

  cfssl gencert -initca ca-csr.json | cfssljson -bare ca

  if [[ ! -f "${_K8s_DIRECTORY}/ca-key.pem" || ! -f "${_K8s_DIRECTORY}/ca.pem" ]]; then
    echo "Could not find ca-key.pem or ca.pem in ${_K8s_DIRECTORY}. exiting script. exit 1"
    exit 1
  else
    echo "Found files: ca-key.pem, ca.pem in ${_K8s_DIRECTORY}."
  fi

echo "line: 614"

#Creating the certificate for the Etcd cluster
cat > kubernetes-csr.json <<EOF
{
  "CN": "Kubernetes",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
  {
    "C": "DE",
    "L": "Germany",
    "O": "mmi",
    "OU": "CA",
    "ST": "gmbh"
  }
 ]
}
EOF
echo "line: 635 @@${_LB_CP_NODES_IP_LIST}@@"
  #Generate the certificate and private key
  cfssl gencert \
  -ca=ca.pem \
  -ca-key=ca-key.pem \
  -config=ca-config.json \
  -hostname=${_LB_CP_NODES_IP_LIST}127.0.0.1,kubernetes.default \
  -profile=kubernetes kubernetes-csr.json | \
  cfssljson -bare kubernetes
echo "line: 644"
  if [[ ! -f "${_K8s_DIRECTORY}/kubernetes-key.pem" || ! -f "${_K8s_DIRECTORY}/kubernetes.pem" ]]; then
    echo "Could not find kubernetes-key.pem or kubernetes.pem in ${_K8s_DIRECTORY}. exiting script. exit 1"
    exit 1
  else
    echo "Found files: kubernetes-key.pem, kubernetes.pem in ${_K8s_DIRECTORY}."
  fi


  #What is in the directory at end
  echo $PWD
  ls -la



fi #END Client Machine create certificates






#tutorial only HAProxy
####################################################################################################################
############### Install in a LOAD BALANCER machine #################################################################
####################################################################################################################
_HOSTNAME_CONTAINS_LB=$( hostname )
if exists_in_list "${_HOST_NAMES_LBS}" " " $_HOSTNAME_CONTAINS_LB; then

  apt-get install -y haproxy

  if ! grep -q "kubernetes" /etc/haproxy/haproxy.cfg ; then


    _CP_STRING_HAPROXY_FILE="_CP_STRING_HAPROXY.txt"
    if [[ -f ${_CP_STRING_HAPROXY_FILE} ]]; then
      truncate -s 0 ${_CP_STRING_HAPROXY_FILE}
    else
      touch ${_CP_STRING_HAPROXY_FILE}
    fi
    for ctrl_plane_node in ${_HOST_NAMES_CP};
    do
      _IP_ADDRESS_CP=$( getent hosts ${ctrl_plane_node} | awk '{ print $1 }' )
      if [[ -z ${_IP_ADDRESS_CP} ]]; then
        echo "Control Plane machine (${ctrl_plane_node}) does not have an IP    WILL EXIT NOW."
        exit 1
      fi

cat >> ${_CP_STRING_HAPROXY_FILE} <<EOF
    server ${ctrl_plane_node} ${_IP_ADDRESS_CP}:6443 check fall 3 rise 2
EOF

    done

  _CP_STRING_HAPROXY_FILE_CAT=$( cat ${_CP_STRING_HAPROXY_FILE} )


cat >> /etc/haproxy/haproxy.cfg <<EOF

frontend kubernetes
bind *:6443
option tcplog
mode tcp
default_backend kubernetes-master-nodes

backend kubernetes-master-nodes
mode tcp
balance roundrobin
option tcp-check
${_CP_STRING_HAPROXY_FILE_CAT}
EOF

  fi #END word kubernetes not in .cfg file

  _HAPROXY_VERSION=$( haproxy --version )

  if [[ -z ${_HAPROXY_VERSION} ]]; then
    echo "Could not get a haproxy version. exiting script. exit 1"
    exit 1
  else
     echo "HAPROXY version: ${_HAPROXY_VERSION}"
  fi

  systemctl restart haproxy


fi #END Load Balancers









echo " "
echo " "
echo "All commands finished."














exit 0

































#Control Plane masters
_CP_STRING_HAPROXY_FILE="_CP_STRING_HAPROXY.txt"
if [[ -f ${_CP_STRING_HAPROXY_FILE} ]]; then
  truncate -s 0 ${_CP_STRING_HAPROXY_FILE}
else
  touch ${_CP_STRING_HAPROXY_FILE}
fi
for ctrl_plane_node in ${_HOST_NAMES_CP};
do
  _IP_ADDRESS_CP=$( getent hosts ${ctrl_plane_node} | awk '{ print $1 }' )
  if [[ -z ${_IP_ADDRESS_CP} ]]; then
    echo "Control Plane machine (${_IP_ADDRESS_CP}) does not have an IP    WILL EXIT NOW."
    exit 1
  fi

cat >> ${_CP_STRING_HAPROXY_FILE} <<EOF
    server ${ctrl_plane_node} ${_IP_ADDRESS}:6443 check fall 3 rise 2
EOF

done

cat ${_CP_STRING_HAPROXY_FILE}























####################################################################################################################
############### Install in a LOAD BALANCER machine #################################################################
####################################################################################################################
_HOSTNAME_CONTAINS_LB=$( hostname )
if exists_in_list "${_HOST_NAMES_LBS}" " " $_HOSTNAME_CONTAINS_LB; then

  _STATE="BACKUP"
  if [[ "${_HOSTNAME_CONTAINS_LB}" = "ubuntugc1604" || "${_HOSTNAME_CONTAINS_LB}" = "accl-ffm-vm-lb1" ]]; then
    _STATE="MASTER"
    _PRIORITY=100
  fi
  if [[ "${_HOSTNAME_CONTAINS_LB}" = "accl-ffm-vm-lb2" ]]; then
    _PRIORITY=95
  fi
  if [[ "${_HOSTNAME_CONTAINS_LB}" = "accl-ffm-vm-lb3" ]]; then
    _PRIORITY=90
  fi

  #Cannot continue with missing varaibles.
  if [[ -z ${_HOSTNAME_CONTAINS_LB} || -z ${_STATE} || -z ${_PRIORITY} || -z ${_FLOATING_IP}  || -z ${_INTERFACE_NAME_ROUTE} || -z ${_AUTH_PASS} ]]; then
    echo "Variables are missing. exiting script. exit 1"
    exit 1
  else
    echo "variables used: [ ${_HOSTNAME_CONTAINS_LB} || ${_STATE} || ${_PRIORITY} || ${_FLOATING_IP} || ${_INTERFACE_NAME_ROUTE} || ${_AUTH_PASS} ]"
  fi

  apt install -y keepalived haproxy

  echo "sleeping 5 - after install keepalived haproxy"
  sleep 5


cat > /etc/keepalived/check_apiserver.sh <<EOF
#!/bin/sh

errorExit() {
  echo "*** $@" 1>&2
  exit 1
}

curl --silent --max-time 2 --insecure https://localhost:6443/ -o /dev/null || errorExit "Error GET https://localhost:6443/"
if ip addr | grep -q ${_FLOATING_IP}; then
  curl --silent --max-time 2 --insecure https://${_FLOATING_IP}:6443/ -o /dev/null || errorExit "Error GET https://${_FLOATING_IP}:6443/"
fi
EOF

  sudo chmod +x /etc/keepalived/check_apiserver.sh

cat > /etc/keepalived/keepalived.conf <<EOF
vrrp_script check_apiserver {
  script "/etc/keepalived/check_apiserver.sh"
  interval 3
  timeout 10
  fall 5
  rise 2
  weight -2
}

vrrp_instance VI_1 {
    state ${_STATE}
    interface ${_INTERFACE_NAME_ROUTE}
    virtual_router_id 1
    priority ${_PRIORITY}
    advert_int 5
    authentication {
        auth_type PASS
        auth_pass ${_AUTH_PASS}
    }
    virtual_ipaddress {
        ${_FLOATING_IP}
    }
    track_script {
        check_apiserver
    }
}
EOF

  sudo systemctl enable --now keepalived

  sleep 5

  if ! grep -q "kubernetes" /etc/haproxy/haproxy.cfg ; then


    _CP_STRING_HAPROXY_FILE="_CP_STRING_HAPROXY.txt"
    if [[ -f ${_CP_STRING_HAPROXY_FILE} ]]; then
      truncate -s 0 ${_CP_STRING_HAPROXY_FILE}
    else
      touch ${_CP_STRING_HAPROXY_FILE}
    fi
    for ctrl_plane_node in ${_HOST_NAMES_CP};
    do
      _IP_ADDRESS_CP=$( getent hosts ${ctrl_plane_node} | awk '{ print $1 }' )
      if [[ -z ${_IP_ADDRESS_CP} ]]; then
        echo "Control Plane machine (${ctrl_plane_node}) does not have an IP    WILL EXIT NOW."
        exit 1
      fi

cat >> ${_CP_STRING_HAPROXY_FILE} <<EOF
    server ${ctrl_plane_node} ${_IP_ADDRESS_CP}:6443 check fall 3 rise 2
EOF

    done

  _CP_STRING_HAPROXY_FILE_CAT=$( cat ${_CP_STRING_HAPROXY_FILE} )


cat >> /etc/haproxy/haproxy.cfg <<EOF

frontend kubernetes-frontend
  bind *:6443
  mode tcp
  option tcplog
  default_backend kubernetes-backend

backend kubernetes-backend
  option httpchk GET /healthz
  http-check expect status 200
  mode tcp
  option ssl-hello-chk
  balance roundrobin
${_CP_STRING_HAPROXY_FILE_CAT}
EOF

  fi #END word kubernetes not in .cfg file

  sudo systemctl enable haproxy
  sleep 5
  sudo systemctl restart haproxy


fi
#End LOAD BALANCER INSTALL
##some useful commnands when implementing
#find out what state it is in.
#journalctl -flu keepalived
#Find out is the floating Ip is assigned here.
#ip a s
#ip a s ens160
#sudo apt-get purge -y --auto-remove haproxy && sudo apt-get purge -y --auto-remove keepalived && rm -rf /etc/keepalived/check_apiserver.sh /etc/keepalived/keepalived.conf /etc/haproxy/haproxy.cfg && reboot







#tutorial only HAProxy
####################################################################################################################
############### Install in a LOAD BALANCER machine #################################################################
####################################################################################################################
_HOSTNAME_CONTAINS_LB=$( hostname )
if exists_in_list "$list_of_lbs" " " $_HOSTNAME_CONTAINS_LB; then

  apt-get install -y haproxy

  if ! grep -q "kubernetes" /etc/haproxy/haproxy.cfg ; then

cat >> /etc/haproxy/haproxy.cfg <<EOF
frontend kubernetes
bind 192.168.150.124:6443
option tcplog
mode tcp
default_backend kubernetes-master-nodes

backend kubernetes-master-nodes
mode tcp
balance roundrobin
option tcp-check
server k8s-master-a 192.168.1.113:6443 check fall 3 rise 2
server k8s-master-b 192.168.1.114:6443 check fall 3 rise 2
server k8s-master-c 192.168.1.115:6443 check fall 3 rise 2
EOF

  fi

  _HAPROXY_VERSION=$( haproxy --version )

  if [[ -z ${_HAPROXY_VERSION} ]]; then
    echo "Could not get a haproxy version. exiting script. exit 1"
    exit 1
  else
     echo "HAPROXY version: ${_HAPROXY_VERSION}"
  fi

  systemctl restart haproxy


fi #END Load Balancers
