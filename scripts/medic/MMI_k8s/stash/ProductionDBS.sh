#!/bin/bash

sed -i 's/ENGINE=ndbcluster/ENGINE=InnoDB/g' /cygdrive/c/Temp/MMI_Production_DBs/Backups_20220314/mysql_medic_um.sql
sed -i "6i DROP DATABASE IF EXISTS medic_um;" /cygdrive/c/Temp/MMI_Production_DBs/Backups_20220314/mysql_medic_um.sql
sed -i "7i CREATE DATABASE medic_um \/*\!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_german2_ci *\/;" /cygdrive/c/Temp/MMI_Production_DBs/Backups_20220314/mysql_medic_um.sql
sed -i "8i USE medic_um;" /cygdrive/c/Temp/MMI_Production_DBs/Backups_20220314/mysql_medic_um.sql
head -c 1000 /cygdrive/c/Temp/MMI_Production_DBs/Backups_20220314/mysql_medic_um.sql >> tempGC.log

sed -i 's/ENGINE=ndbcluster/ENGINE=InnoDB/g' /cygdrive/c/Temp/MMI_Production_DBs/Backups_20220314/mysql_medic_dms.sql
sed -i "6i DROP DATABASE IF EXISTS medic_dms;" /cygdrive/c/Temp/MMI_Production_DBs/Backups_20220314/mysql_medic_dms.sql
sed -i "7i CREATE DATABASE medic_dms \/*\!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_german2_ci *\/;" /cygdrive/c/Temp/MMI_Production_DBs/Backups_20220314/mysql_medic_dms.sql
sed -i "8i USE medic_dms;" /cygdrive/c/Temp/MMI_Production_DBs/Backups_20220314/mysql_medic_dms.sql
head -c 1000 /cygdrive/c/Temp/MMI_Production_DBs/Backups_20220314/mysql_medic_dms.sql >> tempGC.log

sed -i 's/ENGINE=ndbcluster/ENGINE=InnoDB/g' /cygdrive/c/Temp/MMI_Production_DBs/Backups_20220314/mysql_medic_om.sql
sed -i "6i DROP DATABASE IF EXISTS medic_om;" /cygdrive/c/Temp/MMI_Production_DBs/Backups_20220314/mysql_medic_om.sql
sed -i "7i CREATE DATABASE medic_om \/*\!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_german2_ci *\/;" /cygdrive/c/Temp/MMI_Production_DBs/Backups_20220314/mysql_medic_om.sql
sed -i "8i USE medic_om;" /cygdrive/c/Temp/MMI_Production_DBs/Backups_20220314/mysql_medic_om.sql
head -c 1000 /cygdrive/c/Temp/MMI_Production_DBs/Backups_20220314/mysql_medic_om.sql >> tempGC.log
