CREATE STREAM vigie_chart_usage_stream_1 (
    user_id VARCHAR,
    function_parameters STRUCT< type VARCHAR, code VARCHAR>,
    entrytimestamp STRUCT< `$date` VARCHAR> )
WITH (KAFKA_TOPIC='medic_vigie_chart_usage', PARTITIONS=5, VALUE_FORMAT='JSON', key='user_id');


CREATE STREAM vigie_chart_usage_stream_2 AS
    SELECT
        user_id AS userid,
        FUNCTION_PARAMETERS->type AS entitytype,
        FUNCTION_PARAMETERS->code AS entityid,
        SUBSTRING(ENTRYTIMESTAMP->`$date`, 1, 10) AS datesearched
    FROM vigie_chart_usage_stream_1;


CREATE TABLE vigie_chart_usage_table AS
    SELECT userid, entitytype, entityid, datesearched, count(*) AS count
    FROM vigie_chart_usage_stream_2
    GROUP BY userid, entitytype, entityid, datesearched
    EMIT CHANGES;

