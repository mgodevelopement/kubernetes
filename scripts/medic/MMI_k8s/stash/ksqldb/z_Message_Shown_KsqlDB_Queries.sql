CREATE STREAM vigie_message_shown_stream_1 (
    user_id VARCHAR,
    object_id VARCHAR,
    entrytimestamp VARCHAR )
WITH (KAFKA_TOPIC='medic_vigie_message_shown', PARTITIONS=5, VALUE_FORMAT='JSON', key='user_id');


CREATE STREAM vigie_message_shown_stream_2 AS
    SELECT
        user_id AS userid,
        object_id AS messageid,
        SUBSTRING(entrytimestamp, 1, 10) AS entrytimestamp
    FROM vigie_message_shown_stream_1;


CREATE TABLE vigie_message_shown_table AS
    SELECT userid, messageid, entrytimestamp, count(*) AS count
    FROM vigie_message_shown_stream_2
    GROUP BY userid, messageid, entrytimestamp
    EMIT CHANGES;

