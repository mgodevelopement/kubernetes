CREATE STREAM vigie_message_shown_stream_1_V1 (
    user_id VARCHAR,
    object_id VARCHAR,
    entrytimestamp VARCHAR )
WITH (KEY_FORMAT='NONE', KAFKA_TOPIC='medic_vigie_message_shown', PARTITIONS=3, VALUE_FORMAT='JSON');


CREATE STREAM vigie_message_shown_stream_2_V1 AS
    SELECT
        user_id AS userid,
        object_id AS messageid,
        SUBSTRING(entrytimestamp, 1, 10) AS entrytimestamp
    FROM vigie_message_shown_stream_1_V1;


CREATE TABLE vigie_message_shown_table WITH (KEY_FORMAT='DELIMITED') AS
    SELECT userid, messageid, entrytimestamp, count(*) AS count
    FROM vigie_message_shown_stream_2_V1
    GROUP BY userid, messageid, entrytimestamp
    EMIT CHANGES;

