CREATE STREAM vigie_chart_usage_stream_1_V1 (
    user_id VARCHAR,
    function_parameters STRUCT< type VARCHAR, code VARCHAR>,
    entrytimestamp STRUCT< `$date` VARCHAR> )
WITH (KEY_FORMAT='NONE', KAFKA_TOPIC='medic_vigie_chart_usage', PARTITIONS=3, VALUE_FORMAT='JSON');


CREATE STREAM vigie_chart_usage_stream_2_V1 AS
    SELECT
        user_id AS userid,
        FUNCTION_PARAMETERS->type AS entitytype,
        FUNCTION_PARAMETERS->code AS entityid,
        SUBSTRING(ENTRYTIMESTAMP->`$date`, 1, 10) AS datesearched
    FROM vigie_chart_usage_stream_1_V1;


CREATE TABLE vigie_chart_usage_table WITH (KEY_FORMAT='DELIMITED') AS
    SELECT userid, entitytype, entityid, datesearched, count(*) AS count
    FROM vigie_chart_usage_stream_2_V1
    GROUP BY userid, entitytype, entityid, datesearched
    EMIT CHANGES;



