
CREATE STREAM vigie_chart_usage_stream_1_V1 (
    user_id VARCHAR,
    function_parameters STRUCT< type VARCHAR, code VARCHAR>,
    entrytimestamp STRUCT< `$date` VARCHAR> )
WITH (KEY_FORMAT='NONE', KAFKA_TOPIC='medic_vigie_chart_usage', PARTITIONS=3, VALUE_FORMAT='JSON');


CREATE STREAM vigie_chart_usage_stream_2_V1 AS
    SELECT
        user_id AS userid,
        FUNCTION_PARAMETERS->type AS entitytype,
        FUNCTION_PARAMETERS->code AS entityid,
        SUBSTRING(ENTRYTIMESTAMP->`$date`, 1, 10) AS datesearched
    FROM vigie_chart_usage_stream_1_V1;


CREATE TABLE vigie_chart_usage_table WITH (KEY_FORMAT='DELIMITED') AS
    SELECT userid, entitytype, entityid, datesearched, count(*) AS count
    FROM vigie_chart_usage_stream_2_V1
    GROUP BY userid, entitytype, entityid, datesearched
    EMIT CHANGES;






CREATE STREAM vigie_message_shown_stream_1_V1 (
    user_id VARCHAR,
    object_id VARCHAR,
    entrytimestamp VARCHAR )
WITH (KEY_FORMAT='NONE', KAFKA_TOPIC='medic_vigie_message_shown', PARTITIONS=3, VALUE_FORMAT='JSON');


CREATE STREAM vigie_message_shown_stream_2_V1 AS
    SELECT
        user_id AS userid,
        object_id AS messageid,
        SUBSTRING(entrytimestamp, 1, 10) AS entrytimestamp
    FROM vigie_message_shown_stream_1_V1;


CREATE TABLE vigie_message_shown_table WITH (KEY_FORMAT='DELIMITED') AS
    SELECT userid, messageid, entrytimestamp, count(*) AS count
    FROM vigie_message_shown_stream_2_V1
    GROUP BY userid, messageid, entrytimestamp
    EMIT CHANGES;





CREATE STREAM VIGIE_DOCUMENTS_01_V1 (FUNCTION_RETURNSTATUS VARCHAR,
                        FUNCTION_MODULENAME VARCHAR,
                        FUNCTION_PARAMETERS VARCHAR,
                        FUNCTION_NAME VARCHAR,
                        SERVICE_SOURCE VARCHAR,
                        PARTNER_ID VARCHAR,
                        SESSIONID VARCHAR,
                        ENTRYTIMESTAMP VARCHAR,
                        USER STRUCT< USER_ID VARCHAR>,
                        FUNCTION_RETURNVALUE ARRAY<STRUCT<"DOCUMENTID" VARCHAR, "DOCUMENTTYPEID" VARCHAR>>)
WITH (KEY_FORMAT='NONE', kafka_topic='medic_logs', value_format='JSON');

CREATE STREAM VIGIE_DOCUMENTS_02_V1 AS SELECT
                        PARTNER_ID,
                        SESSIONID,
                        ENTRYTIMESTAMP,
                        USER->USER_ID AS USERID,
                        EXPLODE(FUNCTION_RETURNVALUE) AS DOCUMENTS_SINGLE
FROM VIGIE_DOCUMENTS_01_V1 WHERE FUNCTION_NAME = 'VIGIEBOX' AND SERVICE_SOURCE = 'VIGIE' EMIT CHANGES;

CREATE STREAM VIGIE_DOCUMENTS_03_V1 AS SELECT
                        PARTNER_ID,
                        SESSIONID,
                        ENTRYTIMESTAMP,
                        USERID,
                        DOCUMENTS_SINGLE->DOCUMENTID AS DOCUMENTID
FROM VIGIE_DOCUMENTS_02_V1 EMIT CHANGES;

