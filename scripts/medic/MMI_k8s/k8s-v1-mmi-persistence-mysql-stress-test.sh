#!/bin/bash


read -p "Are you sure you want to run this script? " -n 1 -r
echo    # (optional) move to a new line
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1 # handle exits from shell or function but don't exit interactive shell
fi




if [[ ! -f "/tmp/mysqlslap/employees_db-full-1.0.6.tar.bz2" && ! -f "/tmp/mysqlslap/employees_db-full-1.0.6.tar" ]]; then
  echo "Will create dir and download file."  
  mkdir /tmp/mysqlslap
  cd /tmp/mysqlslap
  wget https://launchpad.net/test-db/employees-db-1/1.0.6/+download/employees_db-full-1.0.6.tar.bz2
  apt-get update
  apt-get install -y bzip2
  bzip2 -dfv employees_db-full-1.0.6.tar.bz2
  tar -xf employees_db-full-1.0.6.tar
  cd employees_db
  ls -l
else 
  echo "Moving to /tmp/mysqlslap/employees_db"
  cd /tmp/mysqlslap/employees_db
fi

sed -e "s/set storage_engine = InnoDB;/set default_storage_engine = InnoDB;/" -i /tmp/mysqlslap/employees_db/employees.sql
sed -e "s/^select CONCAT('storage engine: ', @@storage_engine) as INFO;/select CONCAT('default storage engine: ', @@default_storage_engine) as INFO;/" -i /tmp/mysqlslap/employees_db/employees.sql

kubectl cp /tmp/mysqlslap mysql-0:/tmp

echo "reached exit"
exit 0

#https://www.digitalocean.com/community/tutorials/how-to-measure-mysql-query-performance-with-mysqlslap
kubectl exec -it mysql-0 bash
cd /tmp/mysqlslap/employees_db
mysql -u root -t < employees.sql
exit

#Run these on Workbench to test and make all the tables were created successfully.
#root@accl-ffm-vm-cp1:/home/cruz# kubectl port-forward mysql-0 3306:3306
#use employees;
#show tables;
#describe titles;
#select count(*) from titles;

#Creates a MysqlCommunity server with MysqlSlap
#kubectl apply -f ubuntutestboxMysqlStress.yaml
#kubectl exec -it [mysql-deployment-POD ID] -- bash
#  mysqlslap --user=root --host=[IP ADDRESS FROM MYSQL POD] --auto-generate-sql --verbose
#  mysqlslap --user=root --host=[IP ADDRESS FROM MYSQL POD] --concurrency=50 --iterations=10 --auto-generate-sql --verbose 
#  mysqlslap --user=root --host=10.244.2.15 --concurrency=50 --iterations=100 --number-int-cols=5 --number-char-cols=20 --auto-generate-sql --verbose
#  mysqlslap --user=root --host=10.244.2.15 --concurrency=50 --iterations=10 --create-schema=employees --query="SELECT * FROM dept_emp;" --verbose







