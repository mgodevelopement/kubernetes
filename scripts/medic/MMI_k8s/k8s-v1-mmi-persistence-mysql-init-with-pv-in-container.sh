#!/bin/bash


COLOR_NC='\e[0m' # No Color
COLOR_BLACK='\e[0;30m'
COLOR_RED='\e[0;31m'
COLOR_GREEN='\e[0;32m'
COLOR_YELLOW='\e[1;33m'
COLOR_BLUE='\e[0;34m'
COLOR_LIGHT_BLUE='\e[1;34m'
COLOR_WHITE='\e[1;37m'


_BASH_START_TIME=$(date +%s)
_LOG_FILE="k8s-v1-mmi-persistence-mysql-init-with-pv-in-container.log"  #/mnt/nas_backup/scripts/kubernetes/mysql/pv/z_mysql-init-pv_in_container.log
_WORKING_DIR="/tmp/"
_K8S_MYSQL_CONTAINER_USERNAME="root"
_SQL_FILE_DIRECTORY="/mnt/nas_backup/scripts/kubernetes/mysql/pv"

echo "Watch progress by running: kubectl exec -it mysql-0 -- tail -f ${_WORKING_DIR}${_LOG_FILE} -n 9000"

read -p "Are you sure you want to run this script? " -n 1 -r
echo    # (optional) move to a new line
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1 # handle exits from shell or function but don't exit interactive shell
fi



#######################################################################################
########################## PROCESS THE PARAMETERS SENT IN #############################
#######################################################################################
# IFS=' ' read -r -a array <<< "$@"
# for index in "${!array[@]}"
# do
    # if [[ ${array[index]} == *"--mpiprodate"* ]]; then
        # IFS='=' read -r -a mpiprodate_array <<< "${array[index]}"
        # _MPIPRO_DATE=${mpiprodate_array[1]}
    # fi
# done

# if [[ -z "$_MPIPRO_DATE" ]]; then
  # echo "ERROR ABORTING SCRIPT; mpiprodate is null."
  # exit 1
# fi


cd ${_WORKING_DIR}


#Reset the log file
if [[ -f ${_LOG_FILE} ]]; then
  truncate -s 0 ${_LOG_FILE}
  echo "$(date +"%m-%d-%Y %H:%M:%S") log file (${_LOG_FILE}) truncated." >> ${_LOG_FILE}
  echo "Run the following command to see results: cat ${_LOG_FILE})"
fi




echo "$(date +"%m-%d-%Y %H:%M:%S") Starting PRIVILEGES CALLS." >> ${_LOG_FILE}
mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} -e "CREATE USER IF NOT EXISTS 'indexdb'@'%' IDENTIFIED BY 'indexdb'" && echo -e "$(date +"%m-%d-%Y %H:%M:%S") CREATE USER indexdb succeeded" >> ${_LOG_FILE} || echo -e "$(date +"%m-%d-%Y %H:%M:%S") CREATE USER indexdb FAILED" >> ${_LOG_FILE}
mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} -e "GRANT ALL PRIVILEGES ON *.* TO 'indexdb'@'%' WITH GRANT OPTION;" && echo -e "$(date +"%m-%d-%Y %H:%M:%S") GRANT indexdb succeeded" >> ${_LOG_FILE} || echo -e "$(date +"%m-%d-%Y %H:%M:%S") GRANT indexdb FAILED" >> ${_LOG_FILE}
mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} -e "CREATE USER IF NOT EXISTS 'medic'@'%' IDENTIFIED BY 'gw6R9ZKryj48aF6Ed5y7rZ8L'" && echo -e "$(date +"%m-%d-%Y %H:%M:%S") CREATE USER medic succeeded" >> ${_LOG_FILE} || echo -e "$(date +"%m-%d-%Y %H:%M:%S") CREATE USER medic FAILED" >> ${_LOG_FILE}
mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} -e "GRANT ALL PRIVILEGES ON *.* TO 'medic'@'%' WITH GRANT OPTION;" && echo -e "$(date +"%m-%d-%Y %H:%M:%S") GRANT medic succeeded" >> ${_LOG_FILE} || echo -e "$(date +"%m-%d-%Y %H:%M:%S") GRANT medic FAILED" >> ${_LOG_FILE}
mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} -e "FLUSH PRIVILEGES;" && echo -e "$(date +"%m-%d-%Y %H:%M:%S") FLUSH PRIVILEGES succeeded" >> ${_LOG_FILE} || echo -e "$(date +"%m-%d-%Y %H:%M:%S") FLUSH PRIVILEGES FAILED" >> ${_LOG_FILE}
echo "$(date +"%m-%d-%Y %H:%M:%S") Ending PRIVILEGES CALLS." >> ${_LOG_FILE}


echo "$(date +"%m-%d-%Y %H:%M:%S") Starting CREATE DATABASES CALLS." >> ${_LOG_FILE}

mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} -e "DROP DATABASE IF EXISTS medic_dms;";
#mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} -e "DROP DATABASE IF EXISTS medic_om;";
mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} -e "DROP DATABASE IF EXISTS medic_log;";
mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} -e "DROP DATABASE IF EXISTS medic_um;";
mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} -e "DROP DATABASE IF EXISTS indexdb;";
echo "$(date +"%m-%d-%Y %H:%M:%S") All databases dropped." >> ${_LOG_FILE}

mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} -e "CREATE DATABASE medic_dms DEFAULT CHARACTER SET utf8 COLLATE utf8_german2_ci";
#mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} -e "CREATE DATABASE medic_om DEFAULT CHARACTER SET utf8 COLLATE utf8_german2_ci";
mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} -e "CREATE DATABASE medic_log DEFAULT CHARACTER SET utf8 COLLATE utf8_german2_ci";
mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} -e "CREATE DATABASE medic_um DEFAULT CHARACTER SET utf8 COLLATE utf8_german2_ci";
mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} -e "CREATE DATABASE indexdb DEFAULT CHARACTER SET utf8 COLLATE utf8_german2_ci";
echo "$(date +"%m-%d-%Y %H:%M:%S") Ending CREATE DATABASES CALLS." >> ${_LOG_FILE}


#You want to run these commands once the files have been copied
yum install pv -y && echo "$(date +"%m-%d-%Y %H:%M:%S") pv insall LOG succeeded." >> ${_LOG_FILE} || echo "$(date +"%m-%d-%Y %H:%M:%S") pv install LOG failed." >> ${_LOG_FILE}

echo -e "$(date +"%m-%d-%Y %H:%M:%S") ${COLOR_GREEN}Starting INDEXDB import.${COLOR_NC}" >> ${_LOG_FILE}
echo -e "$(date +"%m-%d-%Y %H:%M:%S") ${COLOR_GREEN}Starting INDEXDB import.${COLOR_NC}"
pv ${_SQL_FILE_DIRECTORY}/DATE_mysql_medic_indexdb.sql | mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} indexdb && echo "$(date +"%m-%d-%Y %H:%M:%S") pv indexdb succeeded." >> ${_LOG_FILE} || echo "$(date +"%m-%d-%Y %H:%M:%S") pv indexdb failed." >> ${_LOG_FILE}
echo -e "$(date +"%m-%d-%Y %H:%M:%S") ${COLOR_GREEN}Starting DMS import.${COLOR_NC}" >> ${_LOG_FILE}
echo -e "$(date +"%m-%d-%Y %H:%M:%S") ${COLOR_GREEN}Starting DMS import.${COLOR_NC}"
pv ${_SQL_FILE_DIRECTORY}/DATE_mysql_medic_dms.sql | mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} medic_dms && echo "$(date +"%m-%d-%Y %H:%M:%S") pv dms succeeded." >> ${_LOG_FILE} || echo "$(date +"%m-%d-%Y %H:%M:%S") pv dms failed." >> ${_LOG_FILE}
echo -e "$(date +"%m-%d-%Y %H:%M:%S") ${COLOR_GREEN}Starting LOG import.${COLOR_NC}" >> ${_LOG_FILE}
echo -e "$(date +"%m-%d-%Y %H:%M:%S") ${COLOR_GREEN}Starting LOG import.${COLOR_NC}"
pv ${_SQL_FILE_DIRECTORY}/DATE_mysql_medic_log.sql | mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} medic_log && echo "$(date +"%m-%d-%Y %H:%M:%S") pv medic_log succeeded." >> ${_LOG_FILE} || echo "$(date +"%m-%d-%Y %H:%M:%S") pv medic_log failed." >> ${_LOG_FILE}
echo -e "$(date +"%m-%d-%Y %H:%M:%S") ${COLOR_GREEN}Starting MEDIC_UM import.${COLOR_NC}" >> ${_LOG_FILE}
echo -e "$(date +"%m-%d-%Y %H:%M:%S") ${COLOR_GREEN}Starting MEDIC_UM import.${COLOR_NC}"
pv ${_SQL_FILE_DIRECTORY}/DATE_mysql_medic_um.sql  | mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} medic_um && echo "$(date +"%m-%d-%Y %H:%M:%S") pv um succeeded." >> ${_LOG_FILE} || echo "$(date +"%m-%d-%Y %H:%M:%S") pv um failed." >> ${_LOG_FILE}
#echo -e "$(date +"%m-%d-%Y %H:%M:%S") ${COLOR_GREEN}Starting MEDIC_OM import.${COLOR_NC}" >> ${_LOG_FILE}
#echo -e "$(date +"%m-%d-%Y %H:%M:%S") ${COLOR_RED}BE READY FOR A LONG PAUSE HERE IN LOGGING FOR AT LEAST 15 hours. Take a screenshot the log file may be truncated here.${COLOR_NC}" >> ${_LOG_FILE}
#pv ${_SQL_FILE_DIRECTORY}/DATE_mysql_medic_om.sql | mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} medic_om && echo "$(date +"%m-%d-%Y %H:%M:%S") pv om succeeded." >> ${_LOG_FILE} || echo "$(date +"%m-%d-%Y %H:%M:%S") pv om failed." >> ${_LOG_FILE}

#Install Java if it is NOT already installed
rpm -qa | grep -qw java | yum install java-1.8.0-openjdk -y
#yum install java-1.8.0-openjdk -y && echo "$(date +"%m-%d-%Y %H:%M:%S") java insall LOG succeeded." >> ${_LOG_FILE} || echo "$(date +"%m-%d-%Y %H:%M:%S") java install LOG failed." >> ${_LOG_FILE}

echo -e "$(date +"%m-%d-%Y %H:%M:%S") ${COLOR_GREEN}Starting INDEXDB.SESSION import.${COLOR_NC}" >> ${_LOG_FILE}
echo -e "$(date +"%m-%d-%Y %H:%M:%S") ${COLOR_RED}BE READY FOR A LONG PAUSE HERE JAVA insert ignore into sessions will take a long time.${COLOR_NC}" >> ${_LOG_FILE}
echo -e "$(date +"%m-%d-%Y %H:%M:%S") ${COLOR_GREEN}Starting INDEXDB.SESSION import.${COLOR_NC}"
echo -e "$(date +"%m-%d-%Y %H:%M:%S") ${COLOR_RED}BE READY FOR A LONG PAUSE HERE JAVA insert ignore into sessions will take a long time.${COLOR_NC}"
java -jar /mnt/nas_backup/scripts/kubernetes/keep/MysqlInsertIgnore-1.0_full.jar \
  --persistence_user="root" --persistence_password="" --mysql_ip="localhost" \
  --mysql_port=3306 --db_to_use="indexdb" --fixed_thread_poolsize=350 \
  --filename_and_path="/mnt/nas_backup/scripts/kubernetes/mysql/ignore_insert_sessions.sql" \
  --pause_millis=5 2>&1 >> /tmp/ignore_insert_sessions.log
echo -e "$(date +"%m-%d-%Y %H:%M:%S") ${COLOR_GREEN}FINISHED INDEXDB.SESSION import.${COLOR_NC}" >> ${_LOG_FILE}
echo -e "$(date +"%m-%d-%Y %H:%M:%S") ${COLOR_GREEN}FINISHED INDEXDB.SESSION import.${COLOR_NC}"



_BASH_END_TIME=$(date +%s)
_BASH_ELAPSED_TIME=$(( _BASH_END_TIME - _BASH_START_TIME ))
_BASH_ELAPSED_TIME_VAR=$( eval "echo BASH elapsed time: $(date -ud "@$_BASH_ELAPSED_TIME" +'$((%s/3600/24)) days %H hr %M min %S sec')" )
echo -e "" >> ${_LOG_FILE}
echo -e "${COLOR_GREEN}${_BASH_ELAPSED_TIME_VAR}${COLOR_NC}" >> ${_LOG_FILE}
echo -e "" >> ${_LOG_FILE}


echo -e "$(date +"%m-%d-%Y %H:%M:%S") ${COLOR_YELLOW}All processes finished; exiting with code 0.${COLOR_NC}" >> ${_LOG_FILE}
echo -e "$(date +"%m-%d-%Y %H:%M:%S") ${COLOR_YELLOW}All processes finished; exiting with code 0.${COLOR_NC}"
exit 0



