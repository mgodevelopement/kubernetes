apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: clu-mpi-mysql-three-statefulset
  namespace: mmi-medic
spec:
  selector:
    matchLabels:
      app: clu-mpi-mysql-three-lbl-sel
  serviceName: clu-mpi-mysql-three-svc
  replicas: 1
  template:
    metadata:
      labels:
        app: clu-mpi-mysql-three-lbl-sel
        app.kubernetes.io/part-of: mpipro-mysql-cluster-member
    spec:    
      affinity:
        nodeAffinity:
          requiredDuringSchedulingIgnoredDuringExecution:
            nodeSelectorTerms:
            - matchExpressions:
              - key: kubernetes.io/hostname
                operator: In
                values:
                - accl-ffm-srv-010    
      terminationGracePeriodSeconds: 10
      containers:
      - name: clu-mpi-mysql
        image: mysql:5.7.42
        ports:
        - containerPort: 3306
          name: clumpimysqlport  #Cannot be more than 15 chars
        lifecycle:
          postStart:
            exec:
              command: ["/bin/sh", "-c", "/mnt/data/prep-work.sh"] 
        env:
        - name: MYSQL_ALLOW_EMPTY_PASSWORD
          value: "1"
        #- name: MYSQL_ROOT_PASSWORD
        #  value: "marlb0r0"
        volumeMounts:
        - name: conf-vol
          mountPath: /docker-entrypoint-initdb.d/b-init-mysql-cluster.sql
          subPath: b-init-mysql-cluster.sql
        - name: conf-vol
          mountPath: /docker-entrypoint-initdb.d/a-init-test-script.sql
          subPath: a-init-test-script.sql
        - name: conf-vol
          mountPath: /etc/my.cnf
          subPath: my.cnf
        - mountPath: /var/lib/mysql
          name: clu-mpi-mysql-three-volume
          subPath: mysql
        - name: conf-vol
          mountPath: /mnt/data
        - name: scripts-volume
          mountPath: /mnt/nas_backup/scripts # directory location within container
        resources:
          requests:
            cpu: 500m
            memory: 1Gi
        livenessProbe:
          exec:
            command: ["mysqladmin", "ping"]
          initialDelaySeconds: 30
          periodSeconds: 10
          timeoutSeconds: 5
        readinessProbe:
          exec:
            # Check we can execute queries over TCP (skip-networking is off).
            command: ["mysql", "-h", "127.0.0.1", "-e", "SELECT 1"]
          initialDelaySeconds: 5
          periodSeconds: 2
          timeoutSeconds: 1
      initContainers:
      - name: init-set
        image: alpine:latest
        command:
        - /mnt/scripts/run.sh
        volumeMounts:
        - name: conf-vol
          mountPath: /docker-entrypoint-initdb.d/b-init-mysql-cluster.sql
          subPath: b-init-mysql-cluster.sql
        - name: conf-vol
          mountPath: /docker-entrypoint-initdb.d/a-init-test-script.sql
          subPath: a-init-test-script.sql
        - name: scripts-vol
          mountPath: /mnt/scripts
        - mountPath: /var/lib/mysql
          name: clu-mpi-mysql-three-volume
          subPath: mysql  
        - name: conf-vol
          mountPath: /mnt/data
        - name: scripts-volume
          mountPath: /mnt/nas_backup/scripts # directory location within container        
      volumes:
      - name: scripts-vol
        configMap:
          name: clu-mpi-mysql-three-set-config
          defaultMode: 0555
      - name: conf-vol
        emptyDir: {}
      - name: scripts-volume
        hostPath:
          # directory location on host
          path: /mnt/nas_backup/scripts
          # this field is optional
          type: Directory      
  volumeClaimTemplates:
  - metadata:
      name: clu-mpi-mysql-three-volume
    spec:
      storageClassName: longhorn
      accessModes: [ "ReadWriteOnce" ]
      resources:
        requests:
          storage: 80Gi
         
         
---

apiVersion: v1
kind: ConfigMap
metadata:
  name: clu-mpi-mysql-three-set-config
  namespace: mmi-medic
data:
  run.sh: |
    #!/bin/sh
    Sleep 10 # wait for vars to set
    SET_INDEX=${HOSTNAME##*-}
    echo "Starting initializing for pod $SET_INDEX"
    if [ "$SET_INDEX" = "0" ]; then
      cp /mnt/scripts/my-0.cnf /mnt/data/my.cnf
      cp /mnt/scripts/init-mysql.sql /mnt/data/init-mysql.sql
      cp /mnt/scripts/prep-work.sh /mnt/data/prep-work.sh
      touch /mnt/data/auto.cnf       
    elif [ "$SET_INDEX" = "1" ]; then
      cp /mnt/scripts/my-1.cnf /mnt/data/my.cnf
      cp /mnt/scripts/init-mysql.sql /mnt/data/init-mysql.sql
      cp /mnt/scripts/prep-work.sh /mnt/data/prep-work.sh
      touch /mnt/data/auto.cnf
    elif [ "$SET_INDEX" = "2" ]; then
      cp /mnt/scripts/my-2.cnf /mnt/data/my.cnf
      cp /mnt/scripts/init-mysql.sql /mnt/data/init-mysql.sql
      cp /mnt/scripts/prep-work.sh /mnt/data/prep-work.sh
      touch /mnt/data/auto.cnf
    else
      echo "Invalid statefulset index"
      exit 1
    fi 
  my-0.cnf: |
    [mysqld]    
    log-bin
    max-connections = 750
    lower-case-table-names = 1
    max-allowed-packet = 128M
    expire_logs_days=4
    server_id = 101
    server-id = 101 
    general_log=0
  my-1.cnf: | 
    [mysqld]  
    log-bin
    max-connections = 751
    lower-case-table-names = 1
    max-allowed-packet = 128M
    expire_logs_days=4
    server_id = 102
    server-id = 102   
    general_log=0     
  my-2.cnf: |
    [mysqld]    
    log-bin
    max-connections = 752
    lower-case-table-names = 1
    max-allowed-packet = 128M
    expire_logs_days=4
    server_id = 103
    server-id = 103
    general_log=0
  prep-work.sh: |
    #!/bin/sh    
    ###### IMPORTANT REMOVE BELOW LINES BEFORE PROD DEPLOYMENTS. ####
    ##This is just to make sure init-mysql.sql are executed each time while test clustering stuff ###########    
    #rm -rf var/lib/mysql/auto.cnf
    #rm -rf /var/lib/mysql/
    echo "Prepwork done." >> /tmp/tempGC.log
  init-mysql.sql: |    
    GRANT ALL PRIVILEGES ON *.* TO 'root'@'%' WITH GRANT OPTION;
    CREATE USER IF NOT EXISTS 'indexdb'@'%' IDENTIFIED BY 'indexdb';
    GRANT ALL PRIVILEGES ON *.* TO 'indexdb'@'%' WITH GRANT OPTION;
    CREATE USER IF NOT EXISTS 'medic'@'%' IDENTIFIED BY 'gw6R9ZKryj48aF6Ed5y7rZ8L';
    GRANT ALL PRIVILEGES ON *.* TO 'medic'@'%' WITH GRANT OPTION;
    CREATE USER IF NOT EXISTS 'mpipro'@'%' IDENTIFIED BY 'mpipro';
    GRANT ALL PRIVILEGES ON *.* TO 'mpipro'@'%' WITH GRANT OPTION;
    FLUSH PRIVILEGES;
    
    
    
    
    
    
    
    
    
#root@ACCL-FFM-SRV-008:/home/cruz# bash ${K8S_YAML_PATH}/k8s-v1-mmi-TEST-persistence-mysql-mmi-mpipro-cluster-init-with-pv.sh --mpiprodate=20231001    
#After the container gets created then run the following in each container.
#kubectl -c clu-mpi-mysql exec -it pod/clu-mpi-mysql-three-0 -- bash
#kubectl -c clu-mpi-mysql exec -it pod/clu-mpi-mysql-three-statefulset-0 -- bash
#bash-4.2# mysql -u root -p < /mnt/data/init-mysql.sql  NOT NEEDED
#bash-4.2# mysql -uroot -p -e "select host, user from mysql.user;" 
#bash-4.2# bash /mnt/nas_backup/scripts/kubernetes/yamls/k8s-v1-mmi-TEST-persistence-mysql-mmi-mpipro-cluster.sh --mpiprodate=20231001
#cruz@ACCL-FFM-SRV-009:~$ sudo kubectl exec -it pod/clu-mpi-mysql-three-2 -- tail -f /tmp/k8s-v1-mmi-TEST-persistence-mysql-mmi-mpipro-cluster.log -n 9000
#cruz@ACCL-FFM-SRV-009:~$ sudo kubectl exec -it pod/clu-mpi-mysql-three-statefulset-0 -- tail -f /tmp/k8s-v1-mmi-TEST-persistence-mysql-mmi-mpipro-cluster.log -n 9000
#root@accl-ffm-vm-cp1:/home/cruz# kubectl exec -it pod/clu-mpi-mysql-three-statefulset-1 -- mysql -uroot -p -e "select User from mysql.user;"

#root@accl-ffm-vm-cp1:/home/cruz# kubectl exec -it pod/clu-mpi-mysql-three-statefulset-0 -- mysql -uroot -p -e "SELECT * FROM mpipro_20231001.versionmpi_spctext;"
