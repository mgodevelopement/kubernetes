#!/bin/bash


read -p "Are you sure you want to run this script? " -n 1 -r
echo    # (optional) move to a new line
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1 # handle exits from shell or function but don't exit interactive shell
fi


apt-mark unhold kubelet kubectl kubeadm

if ! [ -x "$(command -v kubeadm)" ]; then
  echo 'Kubeadm: is not installed.' >&2
else
  kubeadm reset -f
fi


sleep 10
apt purge kubectl kubeadm kubelet kubernetes-cni -y
sleep 10
apt autoremove -y
sleep 10
rm -rf /etc/cni /etc/kubernetes /var/lib/dockershim /var/lib/etcd /var/lib/kubelet /var/run/kubernetes ~/.kube/*
sleep 10
systemctl daemon-reload
sleep 10
apt-get update -y
sleep 10
apt-get upgrade -y


#Kubectl will still be there but does not seem to effect the re installation of Kubernetes.


####################################################################################################################
############### ALL HAS HAPPENED SEND FINISH NOTE ##################################################################
####################################################################################################################
echo "All steps performed exiting code 0."
exit 0