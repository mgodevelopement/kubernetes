#!/usr/bin/env bash
#set -x

COLOR_NC='\e[0m' # No Color
COLOR_BLACK='\e[0;30m'
COLOR_RED='\e[0;31m'
COLOR_GREEN='\e[0;32m'
COLOR_YELLOW='\e[1;33m'
COLOR_BLUE='\e[0;34m'
COLOR_LIGHT_BLUE='\e[1;34m'
COLOR_WHITE='\e[1;37m'


_EMAIL_TO="g.cruz@mmi.de, a.rajan@mmi.de, e.erbey@mmi.de, d.marcin@mmi.de"
_textbelt_key="827c8010a95c179280c340934ee7a0c0047d8076vnOaTkGJc0xQ68ASJS6dt5LW3"

#Place any phone numbers that will receive an SMS here.
#declare -a StringArray=("+4917634089040" "+4915156305054" )
declare -a StringArray=("+4917634089040" "+4915129076504" )


###############################################################################################################
#Since this will run by CRON on the 1st and the 15th we only want those days
#Be carefuull with this script it the echos can usurp your variables being set.
###############################################################################################################
function calc_use_date {

    local dateParam=$( date -d $1 )
    _todays_day=$(date -d "$dateParam" +"%d")
    _todays_date_formatted=$(date -d "$dateParam" +"%Y%m%d" )
    _current_month_first_day_formatted=$(date -d "$dateParam" +"%Y%m01" )
    _current_month_15_day_formatted=$(date -d "$dateParam" +"%Y%m15" )
    _current_month_last_day=$(date -d "$_current_month_first_day_formatted + 1 month - 1 day" )
    _current_month_last_day_formatted=$(date -d "$_current_month_last_day" +"%Y%m%d" )

    if [[ $_todays_day < 15 ]]; then

        _diff01_LEFT=$((($(date +%s -d $_todays_date_formatted)-$(date +%s -d $_current_month_first_day_formatted))/86400))
        _diff01_RIGHT=$((($(date +%s -d $_current_month_15_day_formatted)-$(date +%s -d $_todays_date_formatted))/86400))
        if [[ "$_diff01_LEFT" -gt "$_diff01_RIGHT" ]]; then
             _use_month_with_year=$(date -d $_todays_date_formatted +'%Y%m15' )
        else
             _use_month_with_year=$(date -d $_todays_date_formatted +'%Y%m01' )
        fi
        echo "$_use_month_with_year"

    else

        _diff01_LEFT=$((($(date +%s -d $_todays_date_formatted)-$(date +%s -d $_current_month_15_day_formatted))/86400))
        _diff01_RIGHT=$((($(date +%s -d $_current_month_last_day_formatted)-$(date +%s -d $_todays_date_formatted))/86400))
        if [[ "$_diff01_RIGHT" -gt "$_diff01_LEFT" ]]; then
            _use_month_with_year=$(date -d $_todays_date_formatted +'%Y%m15' )
        else
            _todays_month=$(date -d $_todays_date_formatted +"%m")
            if [ "$_todays_month" -eq "12" ]; then
                _next_month="01"
                _next_year="$(date --set='+1 year' +'%Y')"
            else
                monthArray=('01' '02' '03' '04' '05' '06' '07' '08' '09' '10' '11' '12')
                _index_at=$(declare -p monthArray | sed -n "s,.*\[\([^]]*\)\]=\"$_todays_month\".*,\1,p")
                ((_index_at=_index_at+1))
                _next_month=${monthArray[$_index_at]}
                _next_year="$(date -d $_todays_date_formatted +'%Y')"
            fi
            _c="${_next_year}${_next_month}01"
            _use_month_with_year=$(date -d "$_c" +"%Y%m%d")
        fi

        echo "$_use_month_with_year"

    fi

}



###############
function get_mmi_server_ip {

  _ipaddress=$(ifconfig | sed -En "s/.*inet\s(addr:|Adresse:)?(192.168.2.|172.16.0.)([0-9]{1,3}[0-9]?).*/\2\3/p")
  echo "$_ipaddress"

}


##############
function remove-warning () {

    grep -v 'mysql: \[Warning\] Using a password on the command line interface can be insecure.'

}


##############
function remove-docker-tty-warning () {

    grep -v 'failed to resize tty, using default size'

}



#############
function checkPing {

    ping -c 1 -W 5 $1 &> /dev/null
    rc=$?
    if [ $rc = 0 ];
    then
        true
        return
    else
        false
        return
    fi

}



