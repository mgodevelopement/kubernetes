#!/bin/bash

#####################################################################################
#####################################################################################
### EXAMPLE CALL:
###   cruz@NB-Cruz-3 /cygdrive/c/Temp
###     $ cd C:/Users/Cruz/Code/kubernetes/scripts/medic/MMI_k8s
###   cruz@NB-Cruz-3 /cygdrive/c/Users/Cruz/Code/kubernetes/scripts/medic/MMI_k8s
###     $ ./k8s_move_yaml_files.sh
###         moving weave-daemonset-k8s.yaml
###         moving k8s-jmeter521-load-test-call.sh
###         All Done
#####################################################################################
#####################################################################################

_FROM_DIR="C:/Users/Cruz/Code/kubernetes/scripts/medic/MMI_k8s/stash2/"
_TO_DIR="C:/Users/Cruz/Code/kubernetes/scripts/medic/MMI_k8s"


for _FILENAME in k8s-v1-mmi-application-gyrex-configmap.yaml \
k8s_move_yaml_files.sh \
weave-daemonset-k8s.yaml \
k8s-jmeter521-load-test-call.sh \
k8s-v1-mmi-metrics-server-components.yaml \
k8s-v1-mmi-metrics-dashboard-adminuser.yaml \
k8s-v1-mmi-metallb.yaml \
k8s-v1-mmi-metallb-ipaddresspool-advertisement.yaml \
k8s-v1-mmi-persistence-longhorn-values-150.yaml \
k8s-v1-mmi-persistence-longhorn-values.yaml \
k8s-v1-mmi-persistence-longhorn-values.yaml \
k8s-v1-mmi-general-namespace.yaml \
k8s-v1-mmi-persistence-mysql-configmap.yaml \
k8s-v1-mmi-persistence-mysql-service.yaml \
k8s-v1-mmi-persistence-mysql-statefulset.yaml \
k8s-v1-mmi-persistence-mysql-service-pf.yaml \
k8s-v1-mmi-longhorn-pvc-mongo-cluster-values.yaml \
k8s-v1-mmi-application-kafka-cluster-values.yaml \
k8s-v1-mmi-longhorn-pvc-redis-cluster-values.yaml \
k8s-v1-mmi-application-elastisearch-es-statefulset.yaml \
k8s-v1-mmi-application-elastisearch-svc-cluster.yaml \
k8s-v1-mmi-application-elastisearch-svc-loadbalancer.yaml \
k8s-v1-mmi-testing-ubuntutestbox.yaml \
k8s-v1-mmi-application-kafka-kv-store-deployment.yaml \
k8s-v1-mmi-application-kafka-kv-store-autoscale-rules.yaml \
k8s-v1-mmi-application-mpipro-configmap.yaml \
k8s-v1-mmi-application-mpipro-autoscale-rules.yaml \
k8s-v1-mmi-application-mpipro-svc.yaml \
k8s-v1-mmi-application-mpipro-deployment.yaml \
k8s-v1-mmi-application-amts-autoscale-rules.yaml \
k8s-v1-mmi-application-amts-configmap.yaml \
k8s-v1-mmi-application-amts-deployment.yaml \
k8s-v1-mmi-application-amts-svc.yaml \
k8s-v1-mmi-application-karaf-cum-deployment.yaml \
k8s-v1-mmi-application-karaf-cum-svc.yaml \
k8s-v1-mmi-application-karaf-cum-autoscale-rules.yaml \
k8s-v1-mmi-application-gyrex-ingress-ssl-wildcard-cumlb.yaml \
k8s-v1-mmi-application-gyrex-deployment.yaml \
k8s-v1-mmi-application-gyrex-autoscale-rules.yaml \
k8s-v1-mmi-application-gyrex-configmap.yaml \
k8s-v1-mmi-application-gyrex-deployment.yaml \
k8s-v1-mmi-application-gyrex-svc.yaml \
k8s-v1-mmi-application-gyrex-srv001-ingress-ssl-wildcard.yaml \
k8s-v1-mmi-application-gyrex-srv010-ingress-ssl-wildcard.yaml \
k8s-v1-mmi-application-gyrex-ingress-ssl-wildcard-cumlb.yaml \
k8s-v1-mmi-application-gyrex-dms-configmap.yaml \
k8s-v1-mmi-application-gyrex-dms-deployment.yaml \
k8s-v1-mmi-application-gyrex-dms-svc.yaml \
k8s-v1-mmi-application-gyrex-dms-ingress-ssl-wildcard.yaml \
k8s-v1-mmi-application-gyrex-test-configmap.yaml \
k8s-v1-mmi-application-gyrex-test-deployment.yaml \
k8s-v1-mmi-application-gyrex-test-svc.yaml \
k8s-v1-mmi-application-gyrex-test-ingress-ssl-wildcard.yaml \
k8s-v1-mmi-general-cron-job-session-table-cleanup.yaml \
k8s-v1-mmi-general-cron-job-search-indexer.yaml \
k8s-v1-mmi-general-cron-job-del-shutdown-pods.yaml \
k8s-v1-mmi-general-cron-job-mongo-daily-backup.yaml \
k8s-v1-mmi-application-zeppelin-spark-hadoop-pvc-longhorn.yaml \
k8s-v1-mmi-testing-ubuntutestboxZeppelin.yaml \
k8s-v1-mmi-testing-ubuntutestboxZeppelin.yaml \
k8s-v1-mmi-application-zeppelin-spark-hadoop.yaml \
k8s-v1-mmi-application-zeppelin-spark-hadoop-svc.yaml \
k8s-v1-mmi-install-kubernetes-reset_k8s.sh \
k8s-v1-mmi-install-kubernetes.sh \
k8s-mysql-init-script.sh \
k8s-v1-mmi-persistence-mysql-init-with-pv.sh \
k8s-v1-mmi-persistence-mysql-init-with-pv-in-container.sh \
redis_view_keys_values.sh \
Scripts_DynamicNFSStorageClass.sh \
k8s-v1-mmi-persistence-mysql-stress-test.sh \
k8s-v1-mmi-install-kubernetes-reset_k8s.sh; 
do 
    
  if [ -f ${_FROM_DIR}${_FILENAME} ]; then 
    echo "moving ${_FILENAME}" 
    mv ${_FROM_DIR}${_FILENAME} ${_TO_DIR}
  fi 
  
done;

echo "All Done"