#!/bin/bash
#set -x




################################## SHELL SCRIPT ###################################
# CALL THIS FILE ONLY ON A STRONG SERVER .29, .30, .31
#
#   ./k8s-mysql-init-script.sh
#      --mpiprodate=20230315
#      --startprocesssingstep=8
#      --cleanup=true
#
#      example call: bash /usr/local/bin/MMI_k8s/k8s-mysql-init-script.sh --mpiprodate=20230315 --startprocesssingstep=8 --cleanup=false
#
#
#
# CERTAIN FILES MUST ALREADY EXISTS!
#
###############################################################################



COLOR_NC='\e[0m' # No Color
COLOR_BLACK='\e[0;30m'
COLOR_RED='\e[0;31m'
COLOR_GREEN='\e[0;32m'
COLOR_YELLOW='\e[1;33m'
COLOR_BLUE='\e[0;34m'
COLOR_LIGHT_BLUE='\e[1;34m'
COLOR_WHITE='\e[1;37m'

_BASH_START_TIME=$(date +%s)
_CLEANUP="false"
_TOTAL_STEPS_IN_FILE=12  #adjust as you add steps.
_LOG_FILE="/usr/local/bin/MMI_k8s/k8s-mysql-init-script.log"
_WORKING_DIR="/mnt/nas_backup/scripts/kubernetes/mysql"
_WORKING_DIR_INNODBCLUSTER="${_WORKING_DIR}/innodbcluster"
_PROCES_AT_STEP_START=1
_PROCES_AT_STEP_END=${_TOTAL_STEPS_IN_FILE}

### this file gets created by secrtion "####UPDATE sessions sesion" by running it on .16 it then places the file in the _WORKING_DIR
_INDEXDB_SQL_FILE_IGNORE="/mnt/nas_backup/scripts/kubernetes/mysql/ignore_insert_sessions.sql"

### indexdb.session table is not being backed up so take this one. but take the file "DATE_" below and call it a day.
### cp /mnt/nas_backup/ACCL-FFM-SRV-002/20220131_021001_mysql_medic_indexdb.zip /mnt/nas_backup/scripts/kubernetes/mysql
_INDEXDB_SQL_FILE="/mnt/nas_backup/scripts/kubernetes/mysql/stash/DATE_mysql_medic_indexdb.sql"
_CUSTOMDB_SQL_FILE="/mnt/nas_backup/scripts/kubernetes/mysql/stash/DATE_mysql_medic_customdb.sql"
_DMS_ZIP_FILE="_mysql_medic_dms.zip"
_OM_ZIP_FILE="_mysql_medic_om.zip"
_LOG_ZIP_FILE="_mysql_medic_log.zip"
_UM_ZIP_FILE="_mysql_medic_um.zip"


#MySQL-Shell specific variables.
_IMPORT_TABLE_MYSQL_SHELL_THREADS=38
_IMPORT_TABLE_MYSQL_SHELL_BYTES_PER_CHUNK="50M"
_IMPORT_TABLE_MYSQL_SHELL_CONNECTION_STRING="mysql://root:@127.0.0.1:3306"
_IMPORT_TABLE_MYSQL_SHELL_CONTAINER="mysql-FAKE"
_IMPORT_TABLE_MYSQL_SHELL_CONTAINER_SUB="mysql"
_IMPORT_TABLE_MYSQL_SHELL_FIELDS_TERMINATED_BY=", "
_IMPORT_TABLE_MYSQL_SHELL_SHOW_PROGRESS="false"
_IMPORT_TABLE_MYSQL_SHELL_FIELDS_ENCLOSED_BY="'"
_IMPORT_TABLE_MYSQL_SHELL_DIALECT="csv-unix"
_IMPORT_TABLE_MYSQL_SHELL_SKIP_ROWS=0
_IMPORT_TABLE_MYSQL_SHELL_REPLACE_DUPLICATES="true"



#read -p "Are you sure you want to run this script? " -n 1 -r
#echo    # (optional) move to a new line
#if [[ ! $REPLY =~ ^[Yy]$ ]]
#then
#    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1 # handle exits from shell or function but don't exit interactive shell
#fi


cd ${_WORKING_DIR_INNODBCLUSTER}


if [[ ! -f ${_INDEXDB_SQL_FILE_IGNORE} ]]; then
  echo "IndexDB IGNORE file does not exist: ${_INDEXDB_SQL_FILE_IGNORE}"
  exit 1
fi
if [[ ! -f ${_INDEXDB_SQL_FILE} ]]; then
  echo "IndexDB file does not exist: ${_INDEXDB_SQL_FILE}"
  exit 1
fi
if [ ! $(ls ${_WORKING_DIR}/*${_DMS_ZIP_FILE} 2>/dev/null | wc -l) -gt 0 ]; then
  echo "DMS file does not exist: ${_DMS_ZIP_FILE}"
  exit 1
fi
if [ ! $(ls ${_WORKING_DIR}/*${_OM_ZIP_FILE} 2>/dev/null | wc -l) -gt 0 ]; then
  echo "OM file does not exist: ${_OM_ZIP_FILE}"
  exit 1
fi
if [ ! $(ls ${_WORKING_DIR}/*${_LOG_ZIP_FILE} 2>/dev/null | wc -l) -gt 0 ]; then
  echo "LOG file does not exist: ${_LOG_ZIP_FILE}"
  exit 1
fi
if [ ! $(ls ${_WORKING_DIR}/*${_UM_ZIP_FILE} 2>/dev/null | wc -l) -gt 0 ]; then
  echo "UM file does not exist: ${_UM_ZIP_FILE}"
  exit 1
fi
if [ ! $(ls ${_WORKING_DIR}/MPIPRO_*.sql.zip 2>/dev/null | wc -l) -gt 0 ]; then
  echo "MPIPRO file does not exist: MPIPRO_*.sql.zip"
  echo "On .13 or .14 run the following: cp /root/docker/images/mmi_mpipro/MPIPRO_*.sql.zip /mnt/nas_backup/scripts/kubernetes/mysql/"
  exit 1
fi
#if [ ! $(ls ${_WORKING_DIR}/DATE_mysql_medic_customdb.sql 2>/dev/null | wc -l) -gt 0 ]; then
if [[ ! -f ${_CUSTOMDB_SQL_FILE} ]]; then
  echo "CustomDB file does not exist: ${_CUSTOMDB_SQL_FILE}"
  echo "DATE_mysql_medic_customdb.sql file does not exist."
  echo "Run the following commands:"
  echo "  mysqldump --single-transaction --host localhost --port 3306 -uerbey -pmarlb0r0 customdb > /tmp/DATE_mysql_medic_customdb.sql"
  echo "  mv /tmp/DATE_mysql_medic_customdb.sql ${_WORKING_DIR}/stash"
  exit 1
fi





#######################################################################################
########################## PROCESS THE PARAMETERS SENT IN #############################
#######################################################################################
IFS=' ' read -r -a array <<< "$@"
for index in "${!array[@]}"
do
    if [[ ${array[index]} == *"--mpiprodate"* ]]; then
        IFS='=' read -r -a mpiprodate_array <<< "${array[index]}"
        _MPIPRO_DATE=${mpiprodate_array[1]}
    fi
    if [[ ${array[index]} == *"--processatstepstart"* ]]; then
        IFS='=' read -r -a processatstepstart_array <<< "${array[index]}"
        _PROCES_AT_STEP_START=${processatstepstart_array[1]}
    fi
    if [[ ${array[index]} == *"--processatstepend"* ]]; then
        IFS='=' read -r -a processatstepend_array <<< "${array[index]}"
        _PROCES_AT_STEP_END=${processatstepend_array[1]}
    fi
    if [[ ${array[index]} == *"--cleanup"* ]]; then
        IFS='=' read -r -a cleanup_array <<< "${array[index]}"
        _CLEANUP=${cleanup_array[1]}
    fi
done

if [[ -z "$_MPIPRO_DATE" ]]; then
  echo "ERROR ABORTING SCRIPT; mpiprodate is null"
  exit 1
fi


#Reset the log file
if [[ -f ${_LOG_FILE} ]]; then
  truncate -s 0 ${_LOG_FILE}
  echo "$(date +"%m-%d-%Y %H:%M:%S") log file (${_LOG_FILE}) truncated." >> ${_LOG_FILE}
  echo "Run the following command to see results: cat ${_LOG_FILE})"
fi


echo "Will run from step to step: ${_PROCES_AT_STEP_START} :: ${_PROCES_AT_STEP_END}" >> ${_LOG_FILE}









############# TESTING AREA #####################





#_BASH_END_TIME=$(date +%s)
#_BASH_ELAPSED_TIME=$(( _BASH_END_TIME - _BASH_START_TIME ))
#_BASH_ELAPSED_TIME_VAR=$( eval "echo BASH elapsed time: $(date -ud "@$_BASH_ELAPSED_TIME" +'$((%s/3600/24)) days %H hr %M min %S sec')" )
#echo -e "${COLOR_GREEN}${_BASH_ELAPSED_TIME_VAR}${COLOR_NC}" >> ${_LOG_FILE}
#echo -e "$(date +"%m-%d-%Y %H:%M:%S") FAKE TESTING AREA exit" 2>&1 >> ${_LOG_FILE}
#cat ${_LOG_FILE}
#exit 0
############# TESTING AREA #####################














if [[ 1 -ge ${_PROCES_AT_STEP_START} && 1 -le ${_PROCES_AT_STEP_END} ]]; then

  echo -e "${COLOR_RED}PROCESSING STEP 1 of ${_TOTAL_STEPS_IN_FILE} steps: copy sql's${COLOR_NC}" >> ${_LOG_FILE}

  cp ${_CUSTOMDB_SQL_FILE} ${_WORKING_DIR_INNODBCLUSTER}
  if [ $? -eq 0 ]; then
      echo -e "$(date +"%m-%d-%Y %H:%M:%S") Copy of DATE_mysql_medic_customdb.sql succeeded" >> ${_LOG_FILE}
  else
      echo -e "$(date +"%m-%d-%Y %H:%M:%S") Copy of DATE_mysql_medic_customdb.sql failed, exiting now." >> ${_LOG_FILE}
      exit 1
  fi
  #We do a check for this file at the top of k8s-mysql-init-script.sh
  ### The CustomDB file run:
  ###  root@ACCL-FFM-SRV-003: mysqldump --single-transaction --host localhost --port 3306 -uerbey -pmarlb0r0 customdb > /tmp/DATE_mysql_medic_customdb.sql
  ###  root@ACCL-FFM-SRV-003: mv /tmp/DATE_mysql_medic_customdb.sql /mnt/nas_backup/scripts/kubernetes/mysql/


  cp ${_INDEXDB_SQL_FILE} ${_WORKING_DIR_INNODBCLUSTER}
  if [ $? -eq 0 ]; then
      echo -e "$(date +"%m-%d-%Y %H:%M:%S") Copy of DATE_mysql_medic_indexdb.sql succeeded" >> ${_LOG_FILE}
  else
      echo -e "$(date +"%m-%d-%Y %H:%M:%S") Copy of DATE_mysql_medic_indexdb.sql failed, exiting now." >> ${_LOG_FILE}
      exit 1
  fi
  ### THIS CODE IS NOT NEEDED BECAUSE OF THE STASH FILE BEING COPIED ABOVE
  ### We need to omit the session table data as it will be old. we will get it from running "ignore_insert_sessions.sql"
  ### NOTE: grep -v, --invert-match       Invert the sense of matching, to select non-matching lines. To get rid of the session table inserts
  ### Remove the file that does not have "DATE" in the front
  #grep -vh "INSERT INTO \`session\`" *_mysql_medic_indexdb.sql > DATE_mysql_medic_indexdb_WO_SESSION.sql && echo "$(date +"%m-%d-%Y %H:%M:%S") grep INSERT SESSION succeeded." >> ${_LOG_FILE} || echo "$(date +"%m-%d-%Y %H:%M:%S") grep INSERT SESSION failed." >> ${_LOG_FILE}
  #ls | grep -P "^[^DATE].*_mysql_medic_indexdb.sql$" | xargs -d"\n" rm

  ### grep -vh "INSERT INTO \`session\`" *_mysql_medic_indexdb.sql > DATE_mysql_medic_indexdb.sql \
  ### && ls | grep -P "^[^DATE].*_mysql_medic_indexdb.sql$" | xargs -d"\n" rm

fi #step -ge 1



if [[ 2 -ge ${_PROCES_AT_STEP_START} && 2 -le ${_PROCES_AT_STEP_END} ]]; then

  echo -e "${COLOR_RED}PROCESSING STEP 2 of ${_TOTAL_STEPS_IN_FILE} steps: Unzip${COLOR_NC}" >> ${_LOG_FILE}

  unzip -o ${_WORKING_DIR}/*_mysql_medic_dms.zip && echo "$(date +"%m-%d-%Y %H:%M:%S") unzip DMS succeeded." >> ${_LOG_FILE} || echo "$(date +"%m-%d-%Y %H:%M:%S") unzip DMS failed." >> ${_LOG_FILE}

  unzip -o ${_WORKING_DIR}/*_mysql_medic_log.zip && echo "$(date +"%m-%d-%Y %H:%M:%S") unzip LOG succeeded." >> ${_LOG_FILE} || echo "$(date +"%m-%d-%Y %H:%M:%S") unzip LOG failed." >> ${_LOG_FILE}

  unzip -o ${_WORKING_DIR}/*_mysql_medic_um.zip && echo "$(date +"%m-%d-%Y %H:%M:%S") unzip UM succeeded." >> ${_LOG_FILE} || echo "$(date +"%m-%d-%Y %H:%M:%S") unzip UM failed." >> ${_LOG_FILE}

  unzip -o ${_WORKING_DIR}/*_mysql_medic_om.zip && echo "$(date +"%m-%d-%Y %H:%M:%S") unzip OM succeeded." >> ${_LOG_FILE} || echo "$(date +"%m-%d-%Y %H:%M:%S") unzip OM failed." >> ${_LOG_FILE}

  unzip -o ${_WORKING_DIR}/MPIPRO_*.sql.zip && echo "$(date +"%m-%d-%Y %H:%M:%S") unzip MPIPRO succeeded." >> ${_LOG_FILE} || echo "$(date +"%m-%d-%Y %H:%M:%S") unzip MPIPRO failed." >> ${_LOG_FILE}
  #We do a check for this file at the top of k8s-mysql-init-script.sh
  ### The MPIPRO DB comes from maybe /root/docker/images/mmi_mpipro
  ### Make sure you only get the last .zip file
  ###       .13: cp /root/docker/images/mmi_mpipro/MPIPRO_*.sql.zip /mnt/nas_backup/scripts/kubernetes/mysql/

  ####unzip -o *_mysql_medic_indexdb.zip && echo "$(date +"%m-%d-%Y %H:%M:%S") unzip IndexDB succeeded." >> ${_LOG_FILE} || echo "$(date +"%m-%d-%Y %H:%M:%S") unzip IndexDB failed." >> ${_LOG_FILE}
  #MOVED TO k8s-mysql-init-script.sh
  ### indexdb.session table is not being backed up so take this one. but take the file "DATE_" below and call it a day.
  ###  cp /mnt/nas_backup/ACCL-FFM-SRV-002/20220131_021001_mysql_medic_indexdb.zip /mnt/nas_backup/scripts/kubernetes/mysql
  ### cp /mnt/nas_backup/scripts/kubernetes/mysql/stash/DATE_mysql_medic_indexdb.sql /mnt/nas_backup/scripts/kubernetes/mysql

fi  #step -ge 2


if [[ 3 -ge ${_PROCES_AT_STEP_START} && 3 -le ${_PROCES_AT_STEP_END} ]]; then

  echo -e "${COLOR_RED}PROCESSING STEP 3 of ${_TOTAL_STEPS_IN_FILE} steps: Change file names${COLOR_NC}" >> ${_LOG_FILE}

  ###NOTE: NOT NEEDED AS WE ARE NOW DONG THIS WITH awk
  ### Rename the files so the .sh file can work them.
  #mv *_mysql_medic_log.sql DATE_mysql_medic_log.sql && echo "$(date +"%m-%d-%Y %H:%M:%S") mv LOG succeeded." >> ${_LOG_FILE} || echo "$(date +"%m-%d-%Y %H:%M:%S") mv LOG failed." >> ${_LOG_FILE}
  #mv *_mysql_medic_dms.sql DATE_mysql_medic_dms.sql && echo "$(date +"%m-%d-%Y %H:%M:%S") mv DMS succeeded." >> ${_LOG_FILE} || echo "$(date +"%m-%d-%Y %H:%M:%S") mv DMS failed." >> ${_LOG_FILE}
  #mv *_mysql_medic_um.sql DATE_mysql_medic_um.sql && echo "$(date +"%m-%d-%Y %H:%M:%S") mv UM succeeded." >> ${_LOG_FILE} || echo "$(date +"%m-%d-%Y %H:%M:%S") mv UM failed." >> ${_LOG_FILE}

fi #step -ge 3



if [[ 4 -ge ${_PROCES_AT_STEP_START} && 4 -le ${_PROCES_AT_STEP_END} ]]; then

  echo -e "${COLOR_RED}PROCESSING STEP 4 of ${_TOTAL_STEPS_IN_FILE} steps: awk MPIPRO, DMS, LOG, UM${COLOR_NC}" >> ${_LOG_FILE}
  echo "$(date +"%m-%d-%Y %H:%M:%S") Start awk MPIPRO." >> ${_LOG_FILE}
  awk '\
  !/^INSERT INTO `COMPANY_NAME_SIGNATURE`/ \
  && !/^INSERT INTO `MOLECULE_ATC`/ \
  && !/^INSERT INTO `ATC_WORDTOKENS`/ \
  && !/^INSERT INTO `COMPANY_WORDTOKENS`/ \
  && !/^INSERT INTO `REGULATIONPRODUCTGROUP_PRODUCT`/ \
  && !/^INSERT INTO `ICON`/ \
  && !/^INSERT INTO `HAEVG_CONTRACT_INSURANCE`/ \
  && !/^INSERT INTO `BENEFITASSESSMENTGBA`/ \
  && !/^INSERT INTO `ITEM_IDENTA_LOGO`/ \
  && !/^INSERT INTO `DOCUMENTCATEGORYDATA_ATC`/ \
  && !/^INSERT INTO `ARV_HINTLINK`/ \
  && !/^INSERT INTO `COMPANYADDRESSCONTACT`/ \
  && !/^INSERT INTO `DOCUMENT_THERAPYHINT`/ \
  && !/^INSERT INTO `PRODUCTCOUNT`/ \
  && !/^INSERT INTO `COMPANYADDRESS`/ \
  && !/^INSERT INTO `ARV_IWWINDMOLECULEHINT`/ \
  && !/^INSERT INTO `ARVAOK`/ \
  && !/^INSERT INTO `ARVAOK_HINT`/ \
  && !/^INSERT INTO `HMV_GROUP`/ \
  && !/^INSERT INTO `ENTITYSYNONYM`/ \
  && !/^INSERT INTO `PRODUCT_DOCUMENT_SPC`/ \
  && !/^INSERT INTO `HMV_KEYWORD`/ \
  && !/^INSERT INTO `ICD10_NAME_SIGNATURE`/ \
  && !/^INSERT INTO `COMPANY`/ \
  && !/^INSERT INTO `ARV_HINT`/ \
  && !/^INSERT INTO `DOCUMENT_SPC`/ \
  && !/^INSERT INTO `MOLECULE_NAME_SIGNATURE`/ \
  && !/^INSERT INTO `PRODUCT_PRODUCTROA`/ \
  && !/^INSERT INTO `MOLECULE_WORDTOKENS`/ \
  && !/^INSERT INTO `ITEM_IDENTA`/ \
  && !/^INSERT INTO `PRODUCT_ICD`/ \
  && !/^INSERT INTO `HMV_SUBGROUP`/ \
  && !/^INSERT INTO `ICD10_WORDTOKENS`/ \
  && !/^INSERT INTO `ALPHAID_NAME_SIGNATURE`/ \
  && !/^INSERT INTO `COMPOSITIONELEMENT`/ \
  && !/^INSERT INTO `ARV_IWWDOCUMENT`/ \
  && !/^INSERT INTO `HMV_TYPE`/ \
  && !/^INSERT INTO `PRODUCT_PRESCRIPTIONUNIT`/ \
  && !/^INSERT INTO `COMPOSITION`/ \
  && !/^INSERT INTO `ARCHIVE_PRODUCTMOLECULE`/ \
  && !/^INSERT INTO `ITEMCOMPARISONCRITERIA`/ \
  && !/^INSERT INTO `MOLECULE`/ \
  && !/^INSERT INTO `BENEFITASSESSMENTGBAPATGRP`/ \
  && !/^INSERT INTO `REGULATIONDOCUMENT`/ \
  && !/^INSERT INTO `PRODUCT_NAME_SIGNATURE`/ \
  && !/^INSERT INTO `PACKAGE_NAME_SIGNATURE`/ \
  && !/^INSERT INTO `ARV_IWWINDMOLECULE_PACKAGE`/ \
  && !/^INSERT INTO `ARV_IWWINDMOLECULE_PACKAGE`/ \
  && !/^INSERT INTO `ARV_DOCUMENT2`/ \
  && !/^INSERT INTO `ALPHAID_WORDTOKENS`/ \
  && !/^INSERT INTO `ITEM_ATC`/ \
  && !/^INSERT INTO `ALPHAID`/ \
  && !/^INSERT INTO `CATALOGENTRY`/ \
  && !/^INSERT INTO `ITEM_ROA`/ \
  && !/^INSERT INTO `PRODUCTCOMPARISONCRITERIA`/ \
  && !/^INSERT INTO `DOCUMENTCATEGORYDATA_BI`/ \
  && !/^INSERT INTO `PRODUCT_ACTIVE_MOLECULE`/ \
  && !/^INSERT INTO `PACKAGE_COMPARISION_INDEXES`/ \
  && !/^INSERT INTO `LUCENE_INDEX`/ \
  && !/^INSERT INTO `CONTAINER_ITEM`/ \
  && !/^INSERT INTO `ARV_IWWINDMOLECULELINK_PACKAGE`/ \
  && !/^INSERT INTO `PACKAGE_PRICE`/ \
  && !/^INSERT INTO `COMPOSITION_COMPOSITIONELEM`/ \
  && !/^INSERT INTO `CONTAINER`/ \
  && !/^INSERT INTO `PACKAGE_INSURANCE`/ \
  && !/^INSERT INTO `ARVAOK_PACKAGEGROUP`/ \
  && !/^INSERT INTO `PACKAGEEXTENDED2`/ \
  && !/^INSERT INTO `ARCHIVE_PRODUCT`/ \
  && !/^INSERT INTO `ARV_HINTLINK_PACKAGES`/ \
  && !/^INSERT INTO `ITEM`/ \
  && !/^INSERT INTO `ARCHIVE_PACKAGE`/ \
  && !/^INSERT INTO `HMV_PRODUCT`/ \
  && !/^INSERT INTO `PRODUCT_WORDTOKENS`/ \
  && !/^INSERT INTO `PACKAGE_PRICE_HISTORY`/ \
  && !/^INSERT INTO `PACKAGE_WORDTOKENS`/ \
  && !/^INSERT INTO `PRODUCT_MOLECULE`/ \
  && !/^INSERT INTO `MEDPLAN_PACKAGE`/ \
  && !/^INSERT INTO `ARV_PACKAGEGROUP`/ \
  && !/^INSERT INTO `PRODUCT_COMPANY`/ \
  && !/^INSERT INTO `PACKAGEEXTENDED`/ \
  && !/^INSERT INTO `ITEM_IDENTA_PICTURE`/ \
  && !/^INSERT INTO `PACKAGE_PRODUCT`/ \
  && !/^INSERT INTO `PRODUCT`/ \
  && !/^INSERT INTO `DOCUMENTCATEGORYDATA_SPC`/ {print > "DATE_MPIPRO.sql"; next } \
  /^INSERT INTO `COMPANY_NAME_SIGNATURE`/ {print > "ignore_insert_mpipro_company_name_signature.sql"; next } \
  /^INSERT INTO `MOLECULE_ATC`/ {print > "ignore_insert_mpipro_molecule_atc.sql"; next } \
  /^INSERT INTO `ATC_WORDTOKENS`/ {print > "ignore_insert_mpipro_atc_wordtokens.sql"; next } \
  /^INSERT INTO `COMPANY_WORDTOKENS`/ {print > "ignore_insert_mpipro_company_wordtokens.sql"; next } \
  /^INSERT INTO `REGULATIONPRODUCTGROUP_PRODUCT`/ {print > "ignore_insert_mpipro_regulationproductgroup_product.sql"; next } \
  /^INSERT INTO `ICON`/ {print > "ignore_insert_mpipro_icon.sql"; next } \
  /^INSERT INTO `HAEVG_CONTRACT_INSURANCE`/ {print > "ignore_insert_mpipro_haevg_contract_insurance.sql"; next } \
  /^INSERT INTO `BENEFITASSESSMENTGBA`/ {print > "ignore_insert_mpipro_benefitassessmentgba.sql"; next } \
  /^INSERT INTO `ITEM_IDENTA_LOGO`/ {print > "ignore_insert_mpipro_item_identa_logo.sql"; next } \
  /^INSERT INTO `DOCUMENTCATEGORYDATA_ATC`/ {print > "ignore_insert_mpipro_documentcategorydata_atc.sql"; next } \
  /^INSERT INTO `ARV_HINTLINK`/ {print > "ignore_insert_mpipro_arv_hintlink.sql"; next } \
  /^INSERT INTO `COMPANYADDRESSCONTACT`/ {print > "ignore_insert_mpipro_companyaddresscontact.sql"; next } \
  /^INSERT INTO `DOCUMENT_THERAPYHINT`/ {print > "ignore_insert_mpipro_document_therapyhint.sql"; next } \
  /^INSERT INTO `PRODUCTCOUNT`/ {print > "ignore_insert_mpipro_productcount.sql"; next } \
  /^INSERT INTO `COMPANYADDRESS`/ {print > "ignore_insert_mpipro_companyaddress.sql"; next } \
  /^INSERT INTO `ARV_IWWINDMOLECULEHINT`/ {print > "ignore_insert_mpipro_arv_iwwindmoleculehint.sql"; next } \
  /^INSERT INTO `ARVAOK`/ {print > "ignore_insert_mpipro_arvaok.sql"; next } \
  /^INSERT INTO `ARVAOK_HINT`/ {print > "ignore_insert_mpipro_arvaok_hint.sql"; next } \
  /^INSERT INTO `HMV_GROUP`/ {print > "ignore_insert_mpipro_hmv_group.sql"; next } \
  /^INSERT INTO `ENTITYSYNONYM`/ {print > "ignore_insert_mpipro_entitysynonym.sql"; next } \
  /^INSERT INTO `PRODUCT_DOCUMENT_SPC`/ {print > "ignore_insert_mpipro_product_document_spc.sql"; next } \
  /^INSERT INTO `HMV_KEYWORD`/ {print > "ignore_insert_mpipro_hmv_keyword.sql"; next } \
  /^INSERT INTO `ICD10_NAME_SIGNATURE`/ {print > "ignore_insert_mpipro_icd10_name_signature.sql"; next } \
  /^INSERT INTO `COMPANY`/ {print > "ignore_insert_mpipro_company.sql"; next } \
  /^INSERT INTO `ARV_HINT`/ {print > "ignore_insert_mpipro_arv_hint.sql"; next } \
  /^INSERT INTO `DOCUMENT_SPC`/ {print > "ignore_insert_mpipro_document_spc.sql"; next } \
  /^INSERT INTO `MOLECULE_NAME_SIGNATURE`/ {print > "ignore_insert_mpipro_molecule_name_signature.sql"; next } \
  /^INSERT INTO `PRODUCT_PRODUCTROA`/ {print > "ignore_insert_mpipro_product_productroa.sql"; next } \
  /^INSERT INTO `MOLECULE_WORDTOKENS`/ {print > "ignore_insert_mpipro_molecule_wordtokens.sql"; next } \
  /^INSERT INTO `ITEM_IDENTA`/ {print > "ignore_insert_mpipro_item_identa.sql"; next } \
  /^INSERT INTO `PRODUCT_ICD`/ {print > "ignore_insert_mpipro_product_icd.sql"; next } \
  /^INSERT INTO `HMV_SUBGROUP`/ {print > "ignore_insert_mpipro_hmv_subgroup.sql"; next } \
  /^INSERT INTO `ICD10_WORDTOKENS`/ {print > "ignore_insert_mpipro_icd10_wordtokens.sql"; next } \
  /^INSERT INTO `ALPHAID_NAME_SIGNATURE`/ {print > "ignore_insert_mpipro_alphaid_name_signature.sql"; next } \
  /^INSERT INTO `COMPOSITIONELEMENT`/ {print > "ignore_insert_mpipro_compositionelement.sql"; next } \
  /^INSERT INTO `ARV_IWWDOCUMENT`/ {print > "ignore_insert_mpipro_arv_iwwdocument.sql"; next } \
  /^INSERT INTO `HMV_TYPE`/ {print > "ignore_insert_mpipro_hmv_type.sql"; next } \
  /^INSERT INTO `PRODUCT_PRESCRIPTIONUNIT`/ {print > "ignore_insert_mpipro_product_prescriptionunit.sql"; next } \
  /^INSERT INTO `COMPOSITION`/ {print > "ignore_insert_mpipro_composition.sql"; next } \
  /^INSERT INTO `ARCHIVE_PRODUCTMOLECULE`/ {print > "ignore_insert_mpipro_archive_productmolecule.sql"; next } \
  /^INSERT INTO `ITEMCOMPARISONCRITERIA`/ {print > "ignore_insert_mpipro_itemcomparisoncriteria.sql"; next } \
  /^INSERT INTO `MOLECULE`/ {print > "ignore_insert_mpipro_molecule.sql"; next } \
  /^INSERT INTO `BENEFITASSESSMENTGBAPATGRP`/ {print > "ignore_insert_mpipro_benefitassessmentgbapatgrp.sql"; next } \
  /^INSERT INTO `REGULATIONDOCUMENT`/ {print > "ignore_insert_mpipro_regulationdocument.sql"; next } \
  /^INSERT INTO `PRODUCT_NAME_SIGNATURE`/ {print > "ignore_insert_mpipro_product_name_signature.sql"; next } \
  /^INSERT INTO `PACKAGE_NAME_SIGNATURE`/ {print > "ignore_insert_mpipro_package_name_signature.sql"; next } \
  /^INSERT INTO `ARV_IWWINDMOLECULE_PACKAGE`/ {print > "ignore_insert_mpipro_arv_iwwindmolecule_package.sql"; next } \
  /^INSERT INTO `ARV_IWWINDMOLECULE_PACKAGE`/ {print > "ignore_insert_mpipro_arv_iwwindmolecule_package.sql"; next } \
  /^INSERT INTO `ARV_DOCUMENT2`/ {print > "ignore_insert_mpipro_arv_document2.sql"; next } \
  /^INSERT INTO `ALPHAID_WORDTOKENS`/ {print > "ignore_insert_mpipro_alphaid_wordtokens.sql"; next } \
  /^INSERT INTO `ITEM_ATC`/ {print > "ignore_insert_mpipro_item_atc.sql"; next } \
  /^INSERT INTO `ALPHAID`/ {print > "ignore_insert_mpipro_alphaid.sql"; next } \
  /^INSERT INTO `CATALOGENTRY`/ {print > "ignore_insert_mpipro_catalogentry.sql"; next } \
  /^INSERT INTO `ITEM_ROA`/ {print > "ignore_insert_mpipro_item_roa.sql"; next } \
  /^INSERT INTO `PRODUCTCOMPARISONCRITERIA`/ {print > "ignore_insert_mpipro_productcomparisoncriteria.sql"; next } \
  /^INSERT INTO `DOCUMENTCATEGORYDATA_BI`/ {print > "ignore_insert_mpipro_documentcategorydata_bi.sql"; next } \
  /^INSERT INTO `PRODUCT_ACTIVE_MOLECULE`/ {print > "ignore_insert_mpipro_product_active_molecule.sql"; next } \
  /^INSERT INTO `PACKAGE_COMPARISION_INDEXES`/ {print > "ignore_insert_mpipro_package_comparision_indexes.sql"; next } \
  /^INSERT INTO `LUCENE_INDEX`/ {print > "ignore_insert_mpipro_lucene_index.sql"; next } \
  /^INSERT INTO `CONTAINER_ITEM`/ {print > "ignore_insert_mpipro_container_item.sql"; next } \
  /^INSERT INTO `ARV_IWWINDMOLECULELINK_PACKAGE`/ {print > "ignore_insert_mpipro_arv_iwwindmoleculelink_package.sql"; next } \
  /^INSERT INTO `PACKAGE_PRICE`/ {print > "ignore_insert_mpipro_package_price.sql"; next } \
  /^INSERT INTO `COMPOSITION_COMPOSITIONELEM`/ {print > "ignore_insert_mpipro_composition_compositionelem.sql"; next } \
  /^INSERT INTO `CONTAINER`/ {print > "ignore_insert_mpipro_container.sql"; next } \
  /^INSERT INTO `PACKAGE_INSURANCE`/ {print > "ignore_insert_mpipro_package_insurance.sql"; next } \
  /^INSERT INTO `ARVAOK_PACKAGEGROUP`/ {print > "ignore_insert_mpipro_arvaok_packagegroup.sql"; next } \
  /^INSERT INTO `PACKAGEEXTENDED2`/ {print > "ignore_insert_mpipro_packageextended2.sql"; next } \
  /^INSERT INTO `ARCHIVE_PRODUCT`/ {print > "ignore_insert_mpipro_archive_product.sql"; next } \
  /^INSERT INTO `ARV_HINTLINK_PACKAGES`/ {print > "ignore_insert_mpipro_arv_hintlink_packages.sql"; next } \
  /^INSERT INTO `ITEM`/ {print > "ignore_insert_mpipro_item.sql"; next } \
  /^INSERT INTO `ARCHIVE_PACKAGE`/ {print > "ignore_insert_mpipro_archive_package.sql"; next } \
  /^INSERT INTO `HMV_PRODUCT`/ {print > "ignore_insert_mpipro_hmv_product.sql"; next } \
  /^INSERT INTO `PRODUCT_WORDTOKENS`/ {print > "ignore_insert_mpipro_product_wordtokens.sql"; next } \
  /^INSERT INTO `PACKAGE_PRICE_HISTORY`/ {print > "ignore_insert_mpipro_package_price_history.sql"; next } \
  /^INSERT INTO `PACKAGE_WORDTOKENS`/ {print > "ignore_insert_mpipro_package_wordtokens.sql"; next } \
  /^INSERT INTO `PRODUCT_MOLECULE`/ {print > "ignore_insert_mpipro_product_molecule.sql"; next } \
  /^INSERT INTO `MEDPLAN_PACKAGE`/ {print > "ignore_insert_mpipro_medplan_package.sql"; next } \
  /^INSERT INTO `ARV_PACKAGEGROUP`/ {print > "ignore_insert_mpipro_arv_packagegroup.sql"; next } \
  /^INSERT INTO `PRODUCT_COMPANY`/ {print > "ignore_insert_mpipro_product_company.sql"; next } \
  /^INSERT INTO `PACKAGEEXTENDED`/ {print > "ignore_insert_mpipro_packageextended.sql"; next } \
  /^INSERT INTO `ITEM_IDENTA_PICTURE`/ {print > "ignore_insert_mpipro_item_identa_picture.sql"; next } \
  /^INSERT INTO `PACKAGE_PRODUCT`/ {print > "ignore_insert_mpipro_package_product.sql"; next } \
  /^INSERT INTO `PRODUCT`/ {print > "ignore_insert_mpipro_product.sql"; next } \
  /^INSERT INTO `DOCUMENTCATEGORYDATA_SPC`/ {print > "ignore_insert_mpipro_documentcategorydata_spc.sql";}' \
  ${_WORKING_DIR}/MPIPRO_*.sql && echo "$(date +"%m-%d-%Y %H:%M:%S") awk MPIPRO succeeded." >> ${_LOG_FILE} || echo "$(date +"%m-%d-%Y %H:%M:%S") awk MPIPRO failed." >> ${_LOG_FILE}
  cp DATE_MPIPRO.sql DATE_MPIPRO_QLIK.sql && echo "$(date +"%m-%d-%Y %H:%M:%S") cp to QLIK succeeded." >> ${_LOG_FILE} || echo "$(date +"%m-%d-%Y %H:%M:%S") cp to QLIK failed." >> ${_LOG_FILE}
  echo "$(date +"%m-%d-%Y %H:%M:%S") End awk MPIPRO." >> ${_LOG_FILE}

  echo "$(date +"%m-%d-%Y %H:%M:%S") Start awk DMS." >> ${_LOG_FILE}
  awk '\
  !/^INSERT INTO `documentsource`/ \
  && !/^INSERT INTO `documenttype`/ \
  && !/^INSERT INTO `seokeyword`/ \
  && !/^INSERT INTO `documentversion_seokeyword`/ \
  && !/^INSERT INTO `documentversionapproved`/ \
  && !/^INSERT INTO `document_language`/ \
  && !/^INSERT INTO `documentversion_searchindex`/ \
  && !/^INSERT INTO `documentfile`/ \
  && !/^INSERT INTO `documentversion_profession`/ \
  && !/^INSERT INTO `documentversion`/ {print > "DATE_mysql_medic_dms.sql"; next } \
  /^INSERT INTO `documentsource`/ {print > "ignore_insert_dms_documentsource.sql"; next } \
  /^INSERT INTO `documenttype`/ {print > "ignore_insert_dms_documenttype.sql"; next } \
  /^INSERT INTO `seokeyword`/ {print > "ignore_insert_dms_seokeyword.sql"; next } \
  /^INSERT INTO `documentversion_seokeyword`/ {print > "ignore_insert_dms_documentversion_seokeyword.sql"; next } \
  /^INSERT INTO `documentversionapproved`/ {print > "ignore_insert_dms_documentversionapproved.sql"; next } \
  /^INSERT INTO `document_language`/ {print > "ignore_insert_dms_document_language.sql"; next } \
  /^INSERT INTO `documentversion_searchindex`/ {print > "ignore_insert_dms_documentversion_searchindex.sql"; next } \
  /^INSERT INTO `documentfile`/ {print > "ignore_insert_dms_documentfile.sql"; next } \
  /^INSERT INTO `documentversion_profession`/ {print > "ignore_insert_dms_documentversion_profession.sql"; next } \
  /^INSERT INTO `documentversion`/ {print > "ignore_insert_dms_documentversion.sql";}' \
  ${_WORKING_DIR}/*_mysql_medic_dms.sql && echo "$(date +"%m-%d-%Y %H:%M:%S") awk DMS succeeded." >> ${_LOG_FILE} || echo "$(date +"%m-%d-%Y %H:%M:%S") awk DMS failed." >> ${_LOG_FILE}
  echo "$(date +"%m-%d-%Y %H:%M:%S") End awk DMS." >> ${_LOG_FILE}

  echo "$(date +"%m-%d-%Y %H:%M:%S") Start awk UM." >> ${_LOG_FILE}
  awk '\
  !/^INSERT INTO `newsletter_profession`/ \
  && !/^INSERT INTO `country`/ \
  && !/^INSERT INTO `partner_permission`/ \
  && !/^INSERT INTO `activationcode_user`/ \
  && !/^INSERT INTO `partner`/ \
  && !/^INSERT INTO `profession`/ \
  && !/^INSERT INTO `contact`/ \
  && !/^INSERT INTO `user_participation`/ \
  && !/^INSERT INTO `newsletter_user`/ \
  && !/^INSERT INTO `activationcode_productmodule`/ \
  && !/^INSERT INTO `activationcode`/ \
  && !/^INSERT INTO `address`/ \
  && !/^INSERT INTO `user_language`/ \
  && !/^INSERT INTO `userservicekey`/ \
  && !/^INSERT INTO `user_profession`/ \
  && !/^INSERT INTO `user`/ {print > "DATE_mysql_medic_um.sql"; next } \
  /^INSERT INTO `newsletter_profession`/ {print > "ignore_insert_um_newsletter_profession.sql"; next } \
  /^INSERT INTO `country`/ {print > "ignore_insert_um_country.sql"; next } \
  /^INSERT INTO `partner_permission`/ {print > "ignore_insert_um_partner_permission.sql"; next } \
  /^INSERT INTO `activationcode_user`/ {print > "ignore_insert_um_activationcode_user.sql"; next } \
  /^INSERT INTO `partner`/ {print > "ignore_insert_um_partner.sql"; next } \
  /^INSERT INTO `profession`/ {print > "ignore_insert_um_profession.sql"; next } \
  /^INSERT INTO `contact`/ {print > "ignore_insert_um_contact.sql"; next } \
  /^INSERT INTO `user_participation`/ {print > "ignore_insert_um_user_participation.sql"; next } \
  /^INSERT INTO `newsletter_user`/ {print > "ignore_insert_um_newsletter_user.sql"; next } \
  /^INSERT INTO `activationcode_productmodule`/ {print > "ignore_insert_um_activationcode_productmodule.sql"; next } \
  /^INSERT INTO `activationcode`/ {print > "ignore_insert_um_activationcode.sql"; next } \
  /^INSERT INTO `address`/ {print > "ignore_insert_um_address.sql"; next } \
  /^INSERT INTO `user_language`/ {print > "ignore_insert_um_user_language.sql"; next } \
  /^INSERT INTO `userservicekey`/ {print > "ignore_insert_um_userservicekey.sql"; next } \
  /^INSERT INTO `user_profession`/ {print > "ignore_insert_um_user_profession.sql"; next } \
  /^INSERT INTO `user`/ {print > "ignore_insert_um_user.sql";}' \
  ${_WORKING_DIR}/*_mysql_medic_um.sql && echo "$(date +"%m-%d-%Y %H:%M:%S") awk UM succeeded." >> ${_LOG_FILE} || echo "$(date +"%m-%d-%Y %H:%M:%S") awk UM failed." >> ${_LOG_FILE}
  echo "$(date +"%m-%d-%Y %H:%M:%S") End awk UM." >> ${_LOG_FILE}

  echo "$(date +"%m-%d-%Y %H:%M:%S") Start awk RULE." >> ${_LOG_FILE}
  awk '\
  !/^INSERT INTO `rulelog`/ {print > "DATE_mysql_medic_log.sql"; next } \
  /^INSERT INTO `rulelog`/ {print > "ignore_insert_log_rulelog.sql";}' \
  ${_WORKING_DIR}/*_mysql_medic_log.sql && echo "$(date +"%m-%d-%Y %H:%M:%S") awk LOG succeeded." >> ${_LOG_FILE} || echo "$(date +"%m-%d-%Y %H:%M:%S") awk LOG failed." >> ${_LOG_FILE}
  echo "$(date +"%m-%d-%Y %H:%M:%S") End awk RULE." >> ${_LOG_FILE}

fi #step -ge 4



if [[ 5 -ge ${_PROCES_AT_STEP_START} && 5 -le ${_PROCES_AT_STEP_END} ]]; then

  echo -e "${COLOR_RED}PROCESSING STEP 5 of ${_TOTAL_STEPS_IN_FILE} steps: awk OM${COLOR_NC}" >> ${_LOG_FILE}

  #We only want the create* statements not the INSERT INTO LINES for DATE_mysql_medic_om.sql. Then create the individual OM insert ignore files
  echo "$(date +"%m-%d-%Y %H:%M:%S") Start awk OM." >> ${_LOG_FILE}
  awk '\
  !/^INSERT INTO / {print > "DATE_mysql_medic_om.sql"; next } \
  /^INSERT INTO `objecttag`/ {print > "ignore_insert_om_objecttag.sql"; next } \
  /^INSERT INTO `objecttype`/ {print > "ignore_insert_om_objecttype.sql"; next } \
  /^INSERT INTO `object_objecttag`/ {print > "ignore_insert_om_object_objecttag.sql"; next } \
  /^INSERT INTO `object_object`/ {print > "ignore_insert_om_object_object.sql"; next } \
  /^INSERT INTO `object_searchindex`/ {print > "ignore_insert_om_object_searchindex.sql"; next } \
  /^INSERT INTO `object`/ {print > "ignore_insert_om_object.sql"; next } \
  /^INSERT INTO `object_searchindex_idx`/ {print > "ignore_insert_om_object_searchindex_idx.sql"; next } \
  /^INSERT INTO `indexlogger`/ {print > "ignore_insert_om_indexlogger.sql";}' \
  ${_WORKING_DIR}/*_mysql_medic_om.sql
  echo "$(date +"%m-%d-%Y %H:%M:%S") Done awk OM." >> ${_LOG_FILE}

fi #step -ge 5



if [[ 6 -ge ${_PROCES_AT_STEP_START} && 6 -le ${_PROCES_AT_STEP_END} ]]; then

  echo -e "${COLOR_RED}PROCESSING STEP 6 of ${_TOTAL_STEPS_IN_FILE} steps: ignore_insert_sessions awk${COLOR_NC}" >> ${_LOG_FILE}

  echo "$(date +"%m-%d-%Y %H:%M:%S") starting _INDEXDB_SQL_FILE_IGNORE loop." >> ${_LOG_FILE}
  awk '/^INSERT.*INTO `session`/ {print > "ignore_insert_sessions_final.sql";}' ${_INDEXDB_SQL_FILE_IGNORE}
  echo "$(date +"%m-%d-%Y %H:%M:%S") ending _INDEXDB_SQL_FILE_IGNORE loop." >> ${_LOG_FILE}

  echo "$(date +"%m-%d-%Y %H:%M:%S") starting sed NULL." >> ${_LOG_FILE}
  sed -E -i -e "s/, null/, NULL/g" ignore_insert_sessions_final.sql
#  sed -E -i -e "s/, null/, NULL/g" -e "s/^INSERT\sINTO\s\`.*\`\sVALUES\s*\((.*)\);$/\1/g" ignore_insert_sessions_final.sql
  echo "$(date +"%m-%d-%Y %H:%M:%S") done sed NULL." >> ${_LOG_FILE}

fi #step -ge 6



if [[ 7 -ge ${_PROCES_AT_STEP_START} && 7 -le ${_PROCES_AT_STEP_END} ]]; then

  echo -e "${COLOR_RED}PROCESSING STEP 7 of ${_TOTAL_STEPS_IN_FILE} steps: ProductionDBS.sh${COLOR_NC}" >> ${_LOG_FILE}

  #Need to be changes to add the "ENGINE", "DROP", "CREATE", "USE" statements .This will run about an hour (verify) you can tail productiondbs.log to see
  echo "$(date +"%m-%d-%Y %H:%M:%S") ProductionDBS.sh CALL STARTED." >> ${_LOG_FILE}
  /mnt/nas_backup/scripts/kubernetes/keep/ProductionDBS.sh --mpiprodate=${_MPIPRO_DATE} && echo "$(date +"%m-%d-%Y %H:%M:%S") ProductionDBS.sh call succeeded." >> ${_LOG_FILE} || echo "$(date +"%m-%d-%Y %H:%M:%S") ProductionDBS.sh call failed." >> ${_LOG_FILE}
  echo "$(date +"%m-%d-%Y %H:%M:%S") ProductionDBS.sh CALL ENDED." >> ${_LOG_FILE}

fi #step -ge 7







if [[ 8 -ge ${_PROCES_AT_STEP_START} && 8 -le ${_PROCES_AT_STEP_END} ]]; then

  echo -e "${COLOR_RED}PROCESSING STEP 8 of ${_TOTAL_STEPS_IN_FILE} steps: MySQL Privileges${COLOR_NC}" >> ${_LOG_FILE}

  echo "$(date +"%m-%d-%Y %H:%M:%S") Starting PRIVILEGES CALLS." >> ${_LOG_FILE}
  kubectl exec -it -c mysql ${_IMPORT_TABLE_MYSQL_SHELL_CONTAINER_SUB} -- mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} -e "CREATE USER IF NOT EXISTS 'indexdb'@'%' IDENTIFIED BY 'indexdb'" && echo -e "$(date +"%m-%d-%Y %H:%M:%S") CREATE USER indexdb succeeded" >> ${_LOG_FILE} || echo -e "$(date +"%m-%d-%Y %H:%M:%S") CREATE USER indexdb FAILED" >> ${_LOG_FILE}
  kubectl exec -it -c mysql ${_IMPORT_TABLE_MYSQL_SHELL_CONTAINER_SUB} -- mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} -e "GRANT ALL PRIVILEGES ON *.* TO 'indexdb'@'%' WITH GRANT OPTION;" && echo -e "$(date +"%m-%d-%Y %H:%M:%S") GRANT indexdb succeeded" >> ${_LOG_FILE} || echo -e "$(date +"%m-%d-%Y %H:%M:%S") GRANT indexdb FAILED" >> ${_LOG_FILE}
  kubectl exec -it -c mysql ${_IMPORT_TABLE_MYSQL_SHELL_CONTAINER_SUB} -- mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} -e "CREATE USER IF NOT EXISTS medic@'%' IDENTIFIED BY 'gw6R9ZKryj48aF6Ed5y7rZ8L'" && echo -e "$(date +"%m-%d-%Y %H:%M:%S") CREATE USER medic succeeded" >> ${_LOG_FILE} || echo -e "$(date +"%m-%d-%Y %H:%M:%S") CREATE USER medic FAILED" >> ${_LOG_FILE}
  kubectl exec -it -c mysql ${_IMPORT_TABLE_MYSQL_SHELL_CONTAINER_SUB} -- mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} -e "GRANT ALL PRIVILEGES ON *.* TO 'medic'@'%' WITH GRANT OPTION;" && echo -e "$(date +"%m-%d-%Y %H:%M:%S") GRANT medic succeeded" >> ${_LOG_FILE} || echo -e "$(date +"%m-%d-%Y %H:%M:%S") GRANT medic FAILED" >> ${_LOG_FILE}
  kubectl exec -it -c mysql ${_IMPORT_TABLE_MYSQL_SHELL_CONTAINER_SUB} -- mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} -e "CREATE USER IF NOT EXISTS mpipro@'%' IDENTIFIED BY 'mpipro'" && echo -e "$(date +"%m-%d-%Y %H:%M:%S") CREATE USER mpipro succeeded" >> ${_LOG_FILE} || echo -e "$(date +"%m-%d-%Y %H:%M:%S") CREATE USER mpipro FAILED" >> ${_LOG_FILE}
  kubectl exec -it -c mysql ${_IMPORT_TABLE_MYSQL_SHELL_CONTAINER_SUB} -- mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} -e "GRANT ALL PRIVILEGES ON *.* TO 'mpipro'@'%' WITH GRANT OPTION;" && echo -e "$(date +"%m-%d-%Y %H:%M:%S") GRANT mpipro succeeded" >> ${_LOG_FILE} || echo -e "$(date +"%m-%d-%Y %H:%M:%S") GRANT mpirpro FAILED" >> ${_LOG_FILE}
  kubectl exec -it -c mysql ${_IMPORT_TABLE_MYSQL_SHELL_CONTAINER_SUB} -- mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} -e "FLUSH PRIVILEGES;" && echo -e "$(date +"%m-%d-%Y %H:%M:%S") FLUSH PRIVILEGES succeeded" >> ${_LOG_FILE} || echo -e "$(date +"%m-%d-%Y %H:%M:%S") FLUSH PRIVILEGES FAILED" >> ${_LOG_FILE}
  echo "$(date +"%m-%d-%Y %H:%M:%S") Ending PRIVILEGES CALLS." >> ${_LOG_FILE}

fi #step -ge 8



if [[ 9 -ge ${_PROCES_AT_STEP_START} && 9 -le ${_PROCES_AT_STEP_END} ]]; then

  echo -e "${COLOR_RED}PROCESSING STEP 9 of ${_TOTAL_STEPS_IN_FILE} steps: CREATE DBs${COLOR_NC}" >> ${_LOG_FILE}

  echo "$(date +"%m-%d-%Y %H:%M:%S") Starting CREATE DATABASES CALLS." >> ${_LOG_FILE}
  kubectl exec -it -c mysql ${_IMPORT_TABLE_MYSQL_SHELL_CONTAINER_SUB} -- mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} -e "DROP DATABASE IF EXISTS mpipro_${_MPIPRO_DATE};";
  kubectl exec -it -c mysql ${_IMPORT_TABLE_MYSQL_SHELL_CONTAINER_SUB} -- mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} -e "DROP DATABASE IF EXISTS mpipro_qlik;";
  kubectl exec -it -c mysql ${_IMPORT_TABLE_MYSQL_SHELL_CONTAINER_SUB} -- mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} -e "DROP DATABASE IF EXISTS customdb;";
  kubectl exec -it -c mysql ${_IMPORT_TABLE_MYSQL_SHELL_CONTAINER_SUB} -- mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} -e "DROP DATABASE IF EXISTS medic_dms;";
  kubectl exec -it -c mysql ${_IMPORT_TABLE_MYSQL_SHELL_CONTAINER_SUB} -- mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} -e "DROP DATABASE IF EXISTS medic_om;";
  kubectl exec -it -c mysql ${_IMPORT_TABLE_MYSQL_SHELL_CONTAINER_SUB} -- mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} -e "DROP DATABASE IF EXISTS medic_log;";
  kubectl exec -it -c mysql ${_IMPORT_TABLE_MYSQL_SHELL_CONTAINER_SUB} -- mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} -e "DROP DATABASE IF EXISTS medic_um;";
  kubectl exec -it -c mysql ${_IMPORT_TABLE_MYSQL_SHELL_CONTAINER_SUB} -- mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} -e "DROP DATABASE IF EXISTS indexdb;";
  echo "$(date +"%m-%d-%Y %H:%M:%S") All databases dropped." >> ${_LOG_FILE}

  kubectl exec -it -c mysql ${_IMPORT_TABLE_MYSQL_SHELL_CONTAINER_SUB} -- mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} -e "CREATE DATABASE mpipro_${_MPIPRO_DATE} DEFAULT CHARACTER SET utf8 COLLATE utf8_german2_ci";
  kubectl exec -it -c mysql ${_IMPORT_TABLE_MYSQL_SHELL_CONTAINER_SUB} -- mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} -e "CREATE DATABASE mpipro_qlik DEFAULT CHARACTER SET utf8 COLLATE utf8_german2_ci";
  kubectl exec -it -c mysql ${_IMPORT_TABLE_MYSQL_SHELL_CONTAINER_SUB} -- mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} -e "CREATE DATABASE customdb DEFAULT CHARACTER SET utf8 COLLATE utf8_german2_ci";
  kubectl exec -it -c mysql ${_IMPORT_TABLE_MYSQL_SHELL_CONTAINER_SUB} -- mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} -e "CREATE DATABASE medic_dms DEFAULT CHARACTER SET utf8 COLLATE utf8_german2_ci";
  kubectl exec -it -c mysql ${_IMPORT_TABLE_MYSQL_SHELL_CONTAINER_SUB} -- mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} -e "CREATE DATABASE medic_om DEFAULT CHARACTER SET utf8 COLLATE utf8_german2_ci";
  kubectl exec -it -c mysql ${_IMPORT_TABLE_MYSQL_SHELL_CONTAINER_SUB} -- mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} -e "CREATE DATABASE medic_log DEFAULT CHARACTER SET utf8 COLLATE utf8_german2_ci";
  kubectl exec -it -c mysql ${_IMPORT_TABLE_MYSQL_SHELL_CONTAINER_SUB} -- mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} -e "CREATE DATABASE medic_um DEFAULT CHARACTER SET utf8 COLLATE utf8_german2_ci";
  kubectl exec -it -c mysql ${_IMPORT_TABLE_MYSQL_SHELL_CONTAINER_SUB} -- mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} -e "CREATE DATABASE indexdb DEFAULT CHARACTER SET utf8 COLLATE utf8_german2_ci";
  echo "$(date +"%m-%d-%Y %H:%M:%S") Ending CREATE DATABASES CALLS." >> ${_LOG_FILE}

fi #step -ge 9



if [[ 10 -ge ${_PROCES_AT_STEP_START} && 10 -le ${_PROCES_AT_STEP_END} ]]; then

  echo -e "${COLOR_RED}PROCESSING STEP 10 of ${_TOTAL_STEPS_IN_FILE} steps: Inflating DBs${COLOR_NC}" >> ${_LOG_FILE}

  echo "$(date +"%m-%d-%Y %H:%M:%S") Starting DATABASES Inflating process." >> ${_LOG_FILE}

  kubectl exec -it -c mysql ${_IMPORT_TABLE_MYSQL_SHELL_CONTAINER_SUB} -- mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} mpipro_qlik < DATE_MPIPRO_QLIK.sql && echo -e "$(date +"%m-%d-%Y %H:%M:%S")  Inflating DATE_MPIPRO_QLIK.sql succeeded" >> ${_LOG_FILE} || echo -e "$(date +"%m-%d-%Y %H:%M:%S") Inflating DATE_MPIPRO_QLIK.sql failed, exiting now." >> ${_LOG_FILE};
  kubectl exec -it -c mysql ${_IMPORT_TABLE_MYSQL_SHELL_CONTAINER_SUB} -- mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} mpipro_${_MPIPRO_DATE} < DATE_MPIPRO.sql && echo -e "$(date +"%m-%d-%Y %H:%M:%S")  Inflating DATE_MPIPRO.sql succeeded" >> ${_LOG_FILE} || echo -e "$(date +"%m-%d-%Y %H:%M:%S") Inflating DATE_MPIPRO.sql failed, exiting now." >> ${_LOG_FILE};
  kubectl exec -it -c mysql ${_IMPORT_TABLE_MYSQL_SHELL_CONTAINER_SUB} -- mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} customdb < DATE_mysql_medic_customdb.sql && echo -e "$(date +"%m-%d-%Y %H:%M:%S")  Inflating DATE_mysql_medic_customdb.sql succeeded" >> ${_LOG_FILE} || echo -e "$(date +"%m-%d-%Y %H:%M:%S") Inflating DATE_mysql_medic_customdb.sql failed, exiting now." >> ${_LOG_FILE};
  kubectl exec -it -c mysql ${_IMPORT_TABLE_MYSQL_SHELL_CONTAINER_SUB} -- mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} medic_dms < DATE_mysql_medic_dms.sql && echo -e "$(date +"%m-%d-%Y %H:%M:%S")  Inflating DATE_mysql_medic_dms.sql succeeded" >> ${_LOG_FILE} || echo -e "$(date +"%m-%d-%Y %H:%M:%S") Inflating DATE_mysql_medic_dms.sql@ failed, exiting now." >> ${_LOG_FILE};
  kubectl exec -it -c mysql ${_IMPORT_TABLE_MYSQL_SHELL_CONTAINER_SUB} -- mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} indexdb < DATE_mysql_medic_indexdb.sql && echo -e "$(date +"%m-%d-%Y %H:%M:%S")  Inflating DATE_mysql_medic_indexdb.sql succeeded" >> ${_LOG_FILE} || echo -e "$(date +"%m-%d-%Y %H:%M:%S") Inflating DATE_mysql_medic_indexdb.sql failed, exiting now." >> ${_LOG_FILE};
  kubectl exec -it -c mysql ${_IMPORT_TABLE_MYSQL_SHELL_CONTAINER_SUB} -- mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} medic_log < DATE_mysql_medic_log.sql && echo -e "$(date +"%m-%d-%Y %H:%M:%S")  Inflating DATE_mysql_medic_log.sql succeeded" >> ${_LOG_FILE} || echo -e "$(date +"%m-%d-%Y %H:%M:%S") Inflating DATE_mysql_medic_log.sql failed, exiting now." >> ${_LOG_FILE};
  kubectl exec -it -c mysql ${_IMPORT_TABLE_MYSQL_SHELL_CONTAINER_SUB} -- mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} medic_om < DATE_mysql_medic_om.sql && echo -e "$(date +"%m-%d-%Y %H:%M:%S")  Inflating DATE_mysql_medic_om.sql succeeded" >> ${_LOG_FILE} || echo -e "$(date +"%m-%d-%Y %H:%M:%S") Inflating DATE_mysql_medic_om.sql failed, exiting now." >> ${_LOG_FILE};
  kubectl exec -it -c mysql ${_IMPORT_TABLE_MYSQL_SHELL_CONTAINER_SUB} -- mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} medic_um < DATE_mysql_medic_um.sql && echo -e "$(date +"%m-%d-%Y %H:%M:%S")  Inflating DATE_mysql_medic_um.sql succeeded" >> ${_LOG_FILE} || echo -e "$(date +"%m-%d-%Y %H:%M:%S") Inflating DATE_mysql_medic_um.sql failed, exiting now." >> ${_LOG_FILE};

  echo "$(date +"%m-%d-%Y %H:%M:%S") Ending DATABASES Inflating process." >> ${_LOG_FILE}

fi #step -ge 10




if [[ 11 -ge ${_PROCES_AT_STEP_START} && 11 -le ${_PROCES_AT_STEP_END} ]]; then

  echo -e "${COLOR_RED}PROCESSING STEP 11 of ${_TOTAL_STEPS_IN_FILE} steps: Massaging dump file${COLOR_NC}" >> ${_LOG_FILE}


  #We have to massage the data in the files for MySQL-Shell to accept them
  #If you change the sed on one line you have to change it on the other.
  #ls . | grep -P "^ignore_insert_.*_.*.sql$" | grep -v "_indexlogger\|_sessions" | xargs grep -l "),(" | while read F; do sed -E -i -e 's/\),\(/\n/g' -e "s/','/', '/g" -e "s/^INSERT\sINTO\s\`.*\`\sVALUES\s*\((.*)\);$/\1/g" -e "s/, null/, NULL/g" -e 's/,NULL,/, NULL,/g' -e "s/NULL,'/NULL, '/g" -e "s/NULL,NULL/NULL, NULL/g" -e "s/,NULL$/, NULL/g" -e "s/',NULL/', NULL/g" -e "s/',1,'/', 1, '/g" -e "s/,1,/, 1, /g" $F; echo $F; done
  echo "$(date +"%m-%d-%Y %H:%M:%S") Start File Massaging for MySQL-Shell." >> ${_LOG_FILE}


###**********************
#-size -2M
  find ${_WORKING_DIR_INNODBCLUSTER} -type f -name "*.sql" \
  | sort -k2n \ 
  | awk -F/ '{ print $NF }' \
  | grep -P "^ignore_insert_.*_.*.sql$" \
  | grep -v "^DATE_.*.sql$" \
  | grep -v "_indexlogger\|_sessions" \
  | xargs grep -l "),(" \
  | while read F; do
    _BASH_START_TIME_F=$(date +%s)
    echo "$(date +"%m-%d-%Y %H:%M:%S") File starting sed changes: $F" >> ${_LOG_FILE}
    sed -E "s/^INSERT\sINTO\s\`.*\`\sVALUES\s*\((.*)\);$/\1/g" $F |
    sed -E 's/\),\(/\n/g' |
    sed -E "s/','/', '/g" |
    sed -E "s/, null/, NULL/g" |
    sed -E 's/,NULL,/, NULL,/g' |
    sed -E "s/NULL,'/NULL, '/g" |
    sed -E "s/NULL,NULL/NULL, NULL/g" |
    sed -E "s/,NULL$/, NULL/g" |
    sed -E "s/',NULL/', NULL/g" |
    sed -E "s/',1,'/', 1, '/g" |
    sed -E "s/',0,'/', 0, '/g" |
    sed -E "s/('.*',)([0-9])/\1 \2/g" |
    sed -E "s/^([0-9]{0,9},)('.*)$/\1 \2/g" |
    sed -E "s/^(.*',)[^\s]([0-9]{0,9}.*)$/\1 \2/g" |
    sed -E "s/,1,/, 1, /g" |
    sed -E "s/(.*[0-9]{1}),'/\1, '/g" |
    sed -E "s/([0-9]{1}),([0-9]{1})/\1, \2/g" |
    sed -E "s/NULL,([[:digit:]])/NULL, \1/g" > workfile;
    mv workfile $F;
    _BASH_END_TIME_F=$(date +%s)
    _BASH_ELAPSED_TIME_F=$(( _BASH_END_TIME_F - _BASH_START_TIME_F ))
    _BASH_ELAPSED_TIME_VAR_F=$( eval "echo $F processing elapsed time: $(date -ud "@$_BASH_ELAPSED_TIME_F" +'$((%s/3600/24)) days %H hr %M min %S sec')" )
    echo -e "$(date +"%m-%d-%Y %H:%M:%S") File $F has been worked with sed: ${COLOR_GREEN}${_BASH_ELAPSED_TIME_VAR_F}${COLOR_NC}" >> ${_LOG_FILE}
   done

###**********************


#  ls -Sr ${_WORKING_DIR} | grep -P "^ignore_insert_.*_.*.sql$" \
#  | grep -v "_indexlogger\|_sessions" \
#  | xargs grep -l "),(" \
#  | while read F; do \
#  sed -E -i \
#  -e 's/\),\(/\n/g' \
#  -e "s/','/', '/g" \
#  -e "s/^INSERT\sINTO\s\`.*\`\sVALUES\s*\((.*)\);$/\1/g" \
#  -e "s/, null/, NULL/g" \
#  -e 's/,NULL,/, NULL,/g' \
#  -e "s/NULL,'/NULL, '/g" \
#  -e "s/NULL,NULL/NULL, NULL/g" \
#  -e "s/,NULL$/, NULL/g" \
#  -e "s/',NULL/', NULL/g" \
#  -e "s/',1,'/', 1, '/g" \
#  -e "s/',0,'/', 0, '/g" \
#  -e "s/('.*',)([0-9])/\1 \2/g" \
#  -e "s/^([0-9]{0,9},)('.*)$/\1 \2/g" \
#  -e "s/^(.*',)[^\s]([0-9]{0,9}.*)$/\1 \2/g" \
#  -e "s/,1,/, 1, /g" \
#  -e "s/(.*[0-9]{1}),'/\1, '/g" \
#  -e "s/([0-9]{1}),([0-9]{1})/\1, \2/g" \
#  -e "s/NULL,([[:digit:]])/NULL, \1/g" \
#  $F; \
#  echo "File has been worked with sed: $F" >> ${_LOG_FILE}; \
#  done

  echo "$(date +"%m-%d-%Y %H:%M:%S") Ending File Massaging for MySQL-Shell." >> ${_LOG_FILE}

  #USED FOR INDEXLOGGER FILE We can break the file into smaller pieces and wortk on them maybe?
  #The problem is that it breaks down to over 8000 files so we are taking the hit of workingon one large file
  #csplit -f z_ignore_insert_om_indexlogger_split_ -s -z -n 5 ignore_insert_om_indexlogger.sql '/^INSERT INTO `indexlogger` VALUES/' '{*}'
  echo "$(date +"%m-%d-%Y %H:%M:%S") Start ignore_insert_om_indexlogger.sql File Massaging for MySQL-Shell." >> ${_LOG_FILE}
#  sed -E -i \
#  -e 's/\),\(/\n/g' \
#  -e "s/','/', '/g" \
#  -e "s/^INSERT.*INTO\s\`.*\`\sVALUES\s*\((.*)\);$/\1/g" \
#  -e "s/, null/, NULL/g" \
#  -e 's/,NULL,/, NULL,/g' \
#  -e "s/NULL,'/NULL, '/g" \
#  -e "s/NULL,NULL/NULL, NULL/g" \
#  -e "s/,NULL$/, NULL/g" \
#  -e "s/',NULL/', NULL/g" \
#  -e "s/',1,'/', 1, '/g" \
#  -e "s/',0,'/', 0, '/g" \
#  -e "s/('.*',)([0-9])/\1 \2/g" \
#  -e "s/^([0-9]{0,9},)('.*)$/\1 \2/g" \
#  -e "s/^(.*',)[^\s]([0-9]{0,9}.*)$/\1 \2/g" \
#  -e "s/,1,/, 1, /g" \
#  -e "s/(.*[0-9]{1}),'/\1, '/g" \
#  -e "s/([0-9]{1}),([0-9]{1})/\1, \2/g" \
#  -e "s/NULL,([[:digit:]])/NULL, \1/g" \
#  ignore_insert_om_indexlogger.sql
  echo "$(date +"%m-%d-%Y %H:%M:%S") Ending ignore_insert_om_indexlogger.sql File Massaging for MySQL-Shell." >> ${_LOG_FILE}

fi #step -ge 11



if [[ 12 -ge ${_PROCES_AT_STEP_START} && 12 -le ${_PROCES_AT_STEP_END} ]]; then

  echo -e "${COLOR_RED}PROCESSING STEP 12 of ${_TOTAL_STEPS_IN_FILE} steps: MySQL-Shell calls${COLOR_NC}" >> ${_LOG_FILE}

  #Calls for MySQL-sh
  echo "$(date +"%m-%d-%Y %H:%M:%S") Start MySQL-Shell calls to file." >> ${_LOG_FILE}

  truncate -s 0 /tmp/ignoreinserts.sh

  #The while loop is executed in a subshell. So any changes you do to the variable will not be available once the subshell exits.
  #Instead you can use a here string to re-write the while loop to be in the main shell process; only echo -e $lines will run in a subshell:
  while read line
  do

    _DB=$( echo $line | perl -ne 'if (/.*ignore_insert+_([a-z]+)_([0-9a-z]*)/) { print $1 . "\n" }' )
    _DB_TABLE=$( echo $line | perl -ne 'if (/.*ignore_insert+_([a-z]+)_([a-zA-Z0-9_]*)(\.sql)/) { print $2 . "\n" }' )
    _DB_FILE_NAME=$( echo $line | perl -ne 'if (/(.*)(ignore_insert+_[a-zA-Z0-9_.]*\.sql)/) { print $2 . "\n" }' )

    case ${_DB} in

      om)
        _DB_FINAL="medic_om"
        ;;
      um)
         _DB_FINAL="medic_um"
        ;;
      dms)
         _DB_FINAL="medic_dms"
        ;;
      mpipro)
         _DB_FINAL="mpipro_${_MPIPRO_DATE}"
        ;;
      indexdb)
        _DB_FINAL="indexdb"
        ;;
      log)
         _DB_FINAL="medic_log"
        ;;
      *)
        echo -n "_DB cannot be parsed (${_DB})."
        _DB_FINAL="FAKE_DB"
        #exit 1
        ;;
    esac

    _IMPORT_TABLE_MYSQL_SHELL_FILENAME="${_WORKING_DIR}/${_DB_FILE_NAME}"
    _IMPORT_TABLE_MYSQL_SHELL_SCHEMA="${_DB_FINAL}"
    _IMPORT_TABLE_MYSQL_SHELL_SCHEMA_TABLE="${_DB_TABLE}"

    echo "WILL RUN MySQL-Shell against: ${_IMPORT_TABLE_MYSQL_SHELL_FILENAME} :: ${_IMPORT_TABLE_MYSQL_SHELL_SCHEMA} :: ${_IMPORT_TABLE_MYSQL_SHELL_SCHEMA_TABLE}"

    echo "kubectl exec -it -c ${_IMPORT_TABLE_MYSQL_SHELL_CONTAINER_SUB} ${_IMPORT_TABLE_MYSQL_SHELL_CONTAINER} -- \\" >> /tmp/ignoreinserts.sh
    echo "      mysqlsh ${_IMPORT_TABLE_MYSQL_SHELL_CONNECTION_STRING} \\" >> /tmp/ignoreinserts.sh
    echo "      --ssl-mode=DISABLED -- util import-table \\" >> /tmp/ignoreinserts.sh
    echo "      ${_IMPORT_TABLE_MYSQL_SHELL_FILENAME} \\" >> /tmp/ignoreinserts.sh
    echo "      --schema=\"${_IMPORT_TABLE_MYSQL_SHELL_SCHEMA}\" \\" >> /tmp/ignoreinserts.sh
    echo "      --table=\"${_IMPORT_TABLE_MYSQL_SHELL_SCHEMA_TABLE}\" \\" >> /tmp/ignoreinserts.sh
    echo "      --bytesPerChunk=\"${_IMPORT_TABLE_MYSQL_SHELL_BYTES_PER_CHUNK}\" \\" >> /tmp/ignoreinserts.sh
    echo "      --fieldsTerminatedBy=\"${_IMPORT_TABLE_MYSQL_SHELL_FIELDS_TERMINATED_BY}\" \\" >> /tmp/ignoreinserts.sh
    echo "      --threads=${_IMPORT_TABLE_MYSQL_SHELL_THREADS} \\" >> /tmp/ignoreinserts.sh
    echo "      --replaceDuplicates=${_IMPORT_TABLE_MYSQL_SHELL_REPLACE_DUPLICATES} \\" >> /tmp/ignoreinserts.sh
    echo "      --showProgress=${_IMPORT_TABLE_MYSQL_SHELL_SHOW_PROGRESS} \\" >> /tmp/ignoreinserts.sh
    echo "      --dialect=\"${_IMPORT_TABLE_MYSQL_SHELL_DIALECT}\" \\" >> /tmp/ignoreinserts.sh
    echo "      --fieldsEnclosedBy=\"${_IMPORT_TABLE_MYSQL_SHELL_FIELDS_ENCLOSED_BY}\" \\" >> /tmp/ignoreinserts.sh
    echo "      --skipRows=${_IMPORT_TABLE_MYSQL_SHELL_SKIP_ROWS}" >> /tmp/ignoreinserts.sh
    echo ""  >>  /tmp/ignoreinserts.sh

  done <<< "$( ls -Sr ${_WORKING_DIR} | grep -P "^ignore_insert_.*_.*.sql$" | grep -v "_indexlogger\|_sessions" )"

  #how to call the file to run all the inserts.
  echo "$(date +"%m-%d-%Y %H:%M:%S") " >> ${_LOG_FILE}
  echo "$(date +"%m-%d-%Y %H:%M:%S") Call: bash /tmp/ignoreinserts.sh" >> ${_LOG_FILE}
  echo "$(date +"%m-%d-%Y %H:%M:%S") " >> ${_LOG_FILE}
  echo "$(date +"%m-%d-%Y %H:%M:%S") Ending MySQL-Shell calls to file." >> ${_LOG_FILE}

fi #step -ge 12




if [[ "${_CLEANUP}" == "true" ]]; then

  echo "$(date +"%m-%d-%Y %H:%M:%S") Starting CLEANUP process." >> ${_LOG_FILE}
  ls ${_WORKING_DIR}/ | grep -P "^ignore_insert_.*_.*.sql$" | xargs -r -d"\n" rm
  ls ${_WORKING_DIR}/ | grep -P "^DATE_MPIPRO.*.sql$" | xargs -r -d"\n" rm
  ls ${_WORKING_DIR}/ | grep -P "^MPIPRO_[0-9]{8}.sql$" | xargs -r -d"\n" rm
  ls ${_WORKING_DIR}/ | grep -P "^DATE_mysql_medic_.*.sql$" | xargs -r -d"\n" rm
  ls ${_WORKING_DIR}/ | grep -P "^[0-9]{8}_.*_mysql_medic.*.sql$" | xargs -r -d"\n" rm
  echo "$(date +"%m-%d-%Y %H:%M:%S") Finished CLEANUP process." >> ${_LOG_FILE}

fi





_BASH_END_TIME=$(date +%s)
_BASH_ELAPSED_TIME=$(( _BASH_END_TIME - _BASH_START_TIME ))
_BASH_ELAPSED_TIME_VAR=$( eval "echo BASH elapsed time: $(date -ud "@$_BASH_ELAPSED_TIME" +'$((%s/3600/24)) days %H hr %M min %S sec')" )
echo -e "" >> ${_LOG_FILE}
echo -e "${COLOR_GREEN}${_BASH_ELAPSED_TIME_VAR}${COLOR_NC}" >> ${_LOG_FILE}
echo -e "" >> ${_LOG_FILE}


echo -e "$(date +"%m-%d-%Y %H:%M:%S") ${COLOR_YELLOW}All processes finished; exiting with code 0.${COLOR_NC}" >> ${_LOG_FILE}
exit 0















########### EVERYTHING BELOW THIS LINE IS JUST FOR REFERENCE

#Make a copy of the files before they are massaged for testing purposes
#  ls ${_WORKING_DIR} | grep -P "^ignore_insert_.*_.*.sql$" \
#  | while read F; do \
#  cp $F /tmp/ignoreinserts; \
#  echo $F; \
#  done

