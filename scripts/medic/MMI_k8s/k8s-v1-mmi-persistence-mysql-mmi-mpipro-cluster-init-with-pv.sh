#!/bin/bash

################################## SHELL SCRIPT ###################################
# CALL THIS FILE ONLY ON .29 or any powerful server
#
#   bash ${K8S_YAML_PATH}/k8s-v1-mmi-persistence-mysql-mmi-mpipro-cluster-init-with-pv.sh --mpiprodate=20231001
#
#
#
# CERTAIN FILES MUST ALREADY EXIST!
# /mnt/nas_backup/scripts/kubernetes/keep/MysqlInsertIgnore-1.0_full.jar
# /mnt/nas_backup/scripts/kubernetes/mysql/stash/DATE_mysql_medic_indexdb.sql
# /mnt/nas_backup/scripts/kubernetes/mysql/stash/DATE_mysql_medic_customdb.sql
# /mnt/nas_backup/scripts/kubernetes/yamls/k8s-v1-mmi-persistence-mysql-init-with-pv-in-container.sh
# /mnt/nas_backup/scripts/kubernetes/keep/ProductionDBS.sh
###############################################################################

COLOR_NC='\e[0m' # No Color
COLOR_BLACK='\e[0;30m'
COLOR_RED='\e[0;31m'
COLOR_GREEN='\e[0;32m'
COLOR_YELLOW='\e[1;33m'
COLOR_BLUE='\e[0;34m'
COLOR_LIGHT_BLUE='\e[1;34m'
COLOR_WHITE='\e[1;37m'

_BASH_START_TIME=$(date +%s)
_LOG_FILE="k8s-v1-mmi-TEST-persistence-mysql-mmi-mpipro-cluster-init-with-pv.log"
_WORKING_DIR="/mnt/nas_backup/scripts/kubernetes/mysql/"
_CUSTOMDB_SQL_FILE="/mnt/nas_backup/scripts/kubernetes/mysql/stash/DATE_mysql_medic_customdb.sql"
_K8S_MYSQL_CONTAINER="mysql-0"
_K8S_MYSQL_IN_CONTAINER_PATH="/usr/local/bin/MMI_k8s/"
_K8S_MYSQL_IN_CONTAINER_FILE="k8s-v1-mmi-persistence-mysql-init-with-pv-in-container.sh"
_PRODUCTIONDBS_PATHANDFILE="/mnt/nas_backup/scripts/kubernetes/keep/ProductionDBS_mpi.sh"


read -p "Are you sure you want to run this script? " -n 1 -r
echo    # (optional) move to a new line
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1 # handle exits from shell or function but don't exit interactive shell
fi


#######################################################################################
########################## PROCESS THE PARAMETERS SENT IN #############################
#######################################################################################
IFS=' ' read -r -a array <<< "$@"
for index in "${!array[@]}"
do
    if [[ ${array[index]} == *"--mpiprodate"* ]]; then
        IFS='=' read -r -a mpiprodate_array <<< "${array[index]}"
        _MPIPRO_DATE=${mpiprodate_array[1]}
    fi
done

if [[ -z "$_MPIPRO_DATE" ]]; then
  echo "ERROR ABORTING SCRIPT; mpiprodate is null"
  exit 1
fi


cd ${_WORKING_DIR}


#Reset the log file
if [[ -f ${_LOG_FILE} ]]; then
  truncate -s 0 ${_LOG_FILE}
  echo "$(date +"%m-%d-%Y %H:%M:%S") log file (${_LOG_FILE}) truncated." >> ${_LOG_FILE}
  echo "Run the following command to see results: cat ${_LOG_FILE})"
fi


echo "MPIPRO DATE TO USE: ${_MPIPRO_DATE}" >> ${_LOG_FILE}


cd ${_WORKING_DIR}

cp ${_CUSTOMDB_SQL_FILE} ${_WORKING_DIR}pv/
cp ${_INDEXDB_SQL_FILE} ${_WORKING_DIR}pv/

unzip -o MPIPRO_*.sql.zip -d ${_WORKING_DIR}pv/


cd ${_WORKING_DIR}/pv
echo "cd to ${_WORKING_DIR}/pv" >> ${_LOG_FILE}


##### Rename the files so the .sh file can work them.
mv MPIPRO_*.sql DATE_MPIPRO.sql


${_PRODUCTIONDBS_PATHANDFILE} --mpiprodate=${_MPIPRO_DATE} --pathtofiles="${_WORKING_DIR}pv/"

cd ${_WORKING_DIR}





_BASH_END_TIME=$(date +%s)
_BASH_ELAPSED_TIME=$(( _BASH_END_TIME - _BASH_START_TIME ))
_BASH_ELAPSED_TIME_VAR=$( eval "echo BASH elapsed time: $(date -ud "@$_BASH_ELAPSED_TIME" +'$((%s/3600/24)) days %H hr %M min %S sec')" )
echo -e "" >> ${_LOG_FILE}
echo -e "${COLOR_GREEN}${_BASH_ELAPSED_TIME_VAR}${COLOR_NC}" >> ${_LOG_FILE}
echo -e "" >> ${_LOG_FILE}


echo -e "$(date +"%m-%d-%Y %H:%M:%S") ${COLOR_YELLOW}All processes finished; exiting with code 0.${COLOR_NC}" >> ${_LOG_FILE}
echo -e "$(date +"%m-%d-%Y %H:%M:%S") ${COLOR_YELLOW}All LOCAL processes finished; exiting with code 0.${COLOR_NC}"
exit 0



