#!/bin/bash

COLOR_NC='\e[0m' # No Color
COLOR_BLACK='\e[0;30m'
COLOR_RED='\e[0;31m'
COLOR_GREEN='\e[0;32m'
COLOR_YELLOW='\e[1;33m'
COLOR_BLUE='\e[0;34m'
COLOR_LIGHT_BLUE='\e[1;34m'
COLOR_WHITE='\e[1;37m'

_BASH_START_TIME=$(date +%s)
_LOG_FILE="k8s-v1-mmi-persistence-mysql-init-with-pv-in-container-ignore.log"  #/mnt/nas_backup/scripts/kubernetes/mysql/pv/z_mysql-init-pv_in_container.log
_WORKING_DIR="/tmp/"
_K8S_MYSQL_CONTAINER_USERNAME="root"
_SQL_FILE_DIRECTORY="/mnt/nas_backup/scripts/kubernetes/mysql/pv"

read -p "Are you sure you want to run this script? " -n 1 -r
echo    # (optional) move to a new line
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1 # handle exits from shell or function but don't exit interactive shell
fi

cd ${_WORKING_DIR}

#Reset the log file
if [[ -f ${_LOG_FILE} ]]; then
  truncate -s 0 ${_LOG_FILE}
  echo "$(date +"%m-%d-%Y %H:%M:%S") log file (${_LOG_FILE}) truncated." >> ${_LOG_FILE}
  echo "Run the following command to see results: cat ${_LOG_FILE})"
fi

echo "$(date +"%m-%d-%Y %H:%M:%S") Starting CREATE DATABASES CALLS." >> ${_LOG_FILE}
mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} -e "DROP DATABASE IF EXISTS medic_um;";
echo "$(date +"%m-%d-%Y %H:%M:%S") All databases dropped." >> ${_LOG_FILE}

mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} -e "CREATE DATABASE medic_um DEFAULT CHARACTER SET utf8 COLLATE utf8_german2_ci";
echo "$(date +"%m-%d-%Y %H:%M:%S") Ending CREATE DATABASES CALLS." >> ${_LOG_FILE}


#You want to run these commands once the files have been copied
#yum install pv -y && echo "$(date +"%m-%d-%Y %H:%M:%S") pv insall LOG succeeded." >> ${_LOG_FILE} || echo "$(date +"%m-%d-%Y %H:%M:%S") pv install LOG failed." >> ${_LOG_FILE}

echo -e "$(date +"%m-%d-%Y %H:%M:%S") ${COLOR_GREEN}Starting MEDIC_UM import now.${COLOR_NC}" >> ${_LOG_FILE}
pv ${_SQL_FILE_DIRECTORY}/medic_um_onlydata_13102023bkp.sql  | mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} medic_um && echo "$(date +"%m-%d-%Y %H:%M:%S") pv um succeeded." >> ${_LOG_FILE} || echo "$(date +"%m-%d-%Y %H:%M:%S") pv um failed." >> ${_LOG_FILE}
echo -e "$(date +"%m-%d-%Y %H:%M:%S") ${COLOR_GREEN}Starting DMS import.${COLOR_NC}" >> ${_LOG_FILE}
pv ${_SQL_FILE_DIRECTORY}/medic_dms_onlydata_13102023bkp.sql | mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} medic_dms && echo "$(date +"%m-%d-%Y %H:%M:%S") pv dms succeeded." >> ${_LOG_FILE} || echo "$(date +"%m-%d-%Y %H:%M:%S") pv dms failed." >> ${_LOG_FILE}
echo -e "$(date +"%m-%d-%Y %H:%M:%S") ${COLOR_GREEN}Starting LOG import.${COLOR_NC}" >> ${_LOG_FILE}
pv ${_SQL_FILE_DIRECTORY}/medic_log_onlydata_13102023bkp.sql | mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} medic_log && echo "$(date +"%m-%d-%Y %H:%M:%S") pv medic_log succeeded." >> ${_LOG_FILE} || echo "$(date +"%m-%d-%Y %H:%M:%S") pv medic_log failed." >> ${_LOG_FILE}
echo -e "$(date +"%m-%d-%Y %H:%M:%S") ${COLOR_GREEN}Starting MEDIC_OM import.${COLOR_NC}" >> ${_LOG_FILE}
echo -e "$(date +"%m-%d-%Y %H:%M:%S") ${COLOR_RED}BE READY FOR A LONG PAUSE HERE IN LOGGING FOR AT LEAST 15 hours. Take a screenshot the log file may be truncated here.${COLOR_NC}" >> ${_LOG_FILE}
pv ${_SQL_FILE_DIRECTORY}/medic_om_onlydata_13102023bkp.sql | mysql -u${_K8S_MYSQL_CONTAINER_USERNAME} medic_om && echo "$(date +"%m-%d-%Y %H:%M:%S") pv om succeeded." >> ${_LOG_FILE} || echo "$(date +"%m-%d-%Y %H:%M:%S") pv om failed." >> ${_LOG_FILE}

_BASH_END_TIME=$(date +%s)
_BASH_ELAPSED_TIME=$(( _BASH_END_TIME - _BASH_START_TIME ))
_BASH_ELAPSED_TIME_VAR=$( eval "echo BASH elapsed time: $(date -ud "@$_BASH_ELAPSED_TIME" +'$((%s/3600/24)) days %H hr %M min %S sec')" )
echo -e "" >> ${_LOG_FILE}
echo -e "${COLOR_GREEN}${_BASH_ELAPSED_TIME_VAR}${COLOR_NC}" >> ${_LOG_FILE}
echo -e "" >> ${_LOG_FILE}


echo -e "$(date +"%m-%d-%Y %H:%M:%S") ${COLOR_YELLOW}All processes finished; exiting with code 0.${COLOR_NC}" >> ${_LOG_FILE}
exit 0



