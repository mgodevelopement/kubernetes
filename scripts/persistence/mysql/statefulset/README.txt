All this is based on a turtorial located here.
  https://kubernetes.io/docs/tasks/run-application/run-replicated-stateful-application/

Start with VAGRANT boxes based in  cd C:/Users/Cruz/VagrantBoxes/focal-vagrant-virtualbox-containerd

NOTE: in the script in this tutorial it is started with a Storage type of STORAGECLASS of "standard" so your PV must also be created with "storageClassName: standard" in file mysql-pv-local.yaml

NOTE: mysql-pv-nfs-124.yaml refers to an attempt to connect to an NFS server but just could not get it to work. the machines mounted the volume but when creating the stateful set it would not mount.

in order for this to work you must run the following command on each of the vargrant K8s worker boxes
This will start with 3 servers one for WRITE and 3 for READ. You will only be able to scale up one READ as there are only 4 mysql folders created.
remember that with a STATIC provisioning we are setting up the PV's with no PVC's (volumeClaimTemplates) takes care of creating the PVC's 

$ sudo rm -r /mnt/data/mysql/
$ sudo mkdir -p /mnt/data/mysql/1; sudo mkdir -p /mnt/data/mysql/2; sudo mkdir -p /mnt/data/mysql/3; sudo mkdir -p /mnt/data/mysql/4; sudo chmod 777 /mnt/data/mysql


cruz@NB-Cruz-3 /cygdrive/c/Users/Cruz/Code/kubernetes/scripts/persistence/mysql/statefulset
$ k apply -f mysql-pv-local.yaml -f mysql-configmap.yaml -f mysql-services.yaml -f mysql-statefulset.yaml

$ k get pods -l app=mysql --watch

$ k get pv,pvc -o wide --all-namespaces

$ k get all -o wide

$ k run mysql-client --image=mysql:5.7 -i --rm --restart=Never --  mysql -h mysql-0.mysql <<EOF
CREATE DATABASE test;
CREATE TABLE test.messages (message VARCHAR(250));
INSERT INTO test.messages VALUES ('hello');
EOF

$ k run mysql-client --image=mysql:5.7 -i -t --rm --restart=Never --  mysql -h mysql-read -e "SELECT * FROM test.messages"

$ k run mysql-client-loop --image=mysql:5.7 -i -t --rm --restart=Never --  bash -ic "while sleep 1; do mysql -h mysql-read -e 'SELECT @@server_id,NOW()'; done"

$ k scale statefulset mysql  --replicas=4
watch again and query again and you will see added READ server id

$ k scale statefulset mysql  --replicas=3
Remember here that the PVC and the PV will still be there so scaling back up will be much faster.

$ k run -it --leave-stdin-open=true --rm --image=mysql:5.7 --restart=Never mysql-client -- mysql -h mysql-0.mysql -uroot
WILL GIVE YOU A mysql CLI